package jo_el;

import java.util.ArrayList;
import java.util.Scanner;

public class NuestroAtaque {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int k=sc.nextInt();
		for(int i=0;i<k;i++) {
			sc.nextLine();
			//primera pieza 
			char a=sc.next().toUpperCase().charAt(0);
			//posicion primera pieza formato XY
			int posA=sc.nextInt();
			sc.nextLine();
			//segunda pieza
			char b=sc.next().toUpperCase().charAt(0);
			//posicion segunda pieza formato XY
			int posB=sc.nextInt();
			ArrayList<Integer> pie= comun(a,posA,b,posB);
			for(int j=0;j<pie.size();j++) {
				System.out.print(pie.get(j)+" ");
			}
			System.out.println();
		}
		sc.close();
	}
	public static ArrayList<Integer> comun(char a,int posA,char b,int posB){
		ArrayList<Integer> mov1=null;
		ArrayList<Integer> mov2=null;
		ArrayList<Integer> comun = new ArrayList<>();
		switch (a) {
		case 'C':
			mov1 = cavall(posA);
			break;
		case 'A':
			mov1 = alfil(posA);
			break;
		case 'T':
			mov1 = torre(posA);
			break;
		case 'D':
			mov1 = dama(posA);
			break;
		case 'P':
			mov1 = peo(posA);
			break;
		case 'R':
			mov1 = rei(posA);
			break;
		}
		switch (b) {
		case 'C':
			mov2 = cavall(posB);
			break;
		case 'A':
			mov2 = alfil(posB);
			break;
		case 'T':
			mov2 = torre(posB);
			break;
		case 'D':
			mov2 = dama(posB);
			break;
		case 'P':
			mov2 = peo(posB);
			break;
		case 'R':
			mov2 = rei(posB);
			break;
			}
		for(int i=0;i<mov1.size();i++) {
			for(int j=0;j<mov2.size();j++) {
				if(mov1.get(i)==mov2.get(j)) {
					comun.add(mov1.get(i));
				}
			}
		}
		return comun;
	}
	public static ArrayList<Integer> rei(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		for(int i=-1;i<=1;i++) {
			for(int j=-1;j<=1;j++) {
				if(x+i!=x || y+j!=y)
					if(x+i<=8 && x+i>=1 && y+j>=1 && y+j<=8)
						posi.add((x+i) * 10 + y+j);
			}
		}
		posi.sort(null);
		return posi;
	}
	public static ArrayList<Integer> peo(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		if(x==2) {
			posi.add((x+1) * 10 + y);
			posi.add((x+2) * 10 + y);
		}else {
			if(x+1<=8)
				posi.add((x+1) * 10 + y);
		}
		posi.sort(null);
		return posi;
	}
	public static ArrayList<Integer> dama(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		posi.addAll(alfil(xy));
		posi.addAll(torre(xy));
		posi.sort(null);
		return posi;
	}
	public static ArrayList<Integer> torre(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		int auxX = x;
		int auxY = y;
		for (int i = 0; i < 4; i++) {
			while (auxX >= 1 && auxY >= 1 && auxX <= 8 && auxY <= 8) {
				if (auxX != x || auxY != y) {
					posi.add(auxX * 10 + auxY);
				}
				switch (i) {
				case 0:
					auxX--;
					break;
				case 1:
					auxX++;
					break;
				case 2:
					auxY++;
					break;
				case 3:
					auxY--;
					break;
				}
			}
			auxX = x;
			auxY = y;
		}
		posi.sort(null);
		return posi;
	}
	public static ArrayList<Integer> alfil(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		int auxX = x;
		int auxY = y;
		for (int i = 0; i < 4; i++) {
			while (auxX >= 1 && auxY >= 1 && auxX <= 8 && auxY <= 8) {
				if (auxX != x && auxY != y) {
					posi.add(auxX * 10 + auxY);
				}
				switch (i) {
				case 0:
					auxX--;
					auxY--;
					break;
				case 1:
					auxX++;
					auxY--;
					break;
				case 2:
					auxX--;
					auxY++;
					break;
				case 3:
					auxX++;
					auxY++;
					break;
				}
			}
			auxX = x;
			auxY = y;
		}
		posi.sort(null);
		return posi;
	}
	public static ArrayList<Integer> cavall(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		for(int i=-2;i<=2;i+=4) {
			if(y+i>=1 && y+i<=8) {
				if(x+1>=1 && x+1<=8) {
					posi.add((x+1) * 10 + y+i);
				}
				if(x-1>=1 && x-1<=8) {
					posi.add((x-1) * 10 + y+i);
				}
			}
		}
		for(int i=-2;i<=2;i+=4) {
			if(x+i>=1 && x+i<=8) {
				if(y+1>=1 && y+1<=8) {
					posi.add((x+i) * 10 + y+1);
				}
				if(y-1>=1 && y-1<=8) {
					posi.add((x+i) * 10 + y-1);
				}
			}
		}
		posi.sort(null);
		return posi;
	}
}
