package MITM;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class MITMTest {

	@Test
	public void test() {
		assertEquals(true,MITM.mid(new ArrayList <Integer>(Arrays.asList(14,20,30,10,20,12,15))));
		assertEquals(false,MITM.mid(new ArrayList <Integer>(Arrays.asList(60,140,30,20))));
		assertEquals(true,MITM.mid(new ArrayList <Integer>(Arrays.asList(60,140,60,20,20))));
		assertEquals(false,MITM.mid(new ArrayList <Integer>(Arrays.asList(20,20,20,20,5,50))));
	}

}
