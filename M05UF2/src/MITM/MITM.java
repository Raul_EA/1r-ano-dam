package MITM;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MITM {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		int nCasos = sc.nextInt();
		int elements;
		
		for (int i=0;i<nCasos;i++) {
			elements = sc.nextInt();
			ArrayList <Integer>llista = new ArrayList<Integer>();
			for (int j=0;j<elements;j++) 
				llista.add(sc.nextInt());
			if(mid(llista)) 
				System.out.println("SI");
			else
				System.out.println("NO");
		}
		sc.close();
	}

	public static boolean mid(ArrayList <Integer>llista) {
		int esquerra=0;int dreta=0;
		Collections.sort(llista);
		int ultim = llista.get(0);
		Collections.reverse(llista);
		for(int k=0;k<llista.size()-1;k++) 
			if (esquerra<=dreta)
				esquerra+=llista.get(k);
			else
				dreta+=llista.get(k);
		
		if ((esquerra<=dreta && esquerra+ultim>=dreta) || (esquerra>=dreta && esquerra<=dreta+ultim))
			return true;
		else
			return false;		
	}

}
