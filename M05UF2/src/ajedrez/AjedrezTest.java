package ajedrez;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class AjedrezTest {

	@Test
	public void CavallTest() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(23, 32)), Ajedrez.cavall(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(24, 31, 33)), Ajedrez.cavall(12));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 25, 32, 34)), Ajedrez.cavall(13));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 26, 33, 35)), Ajedrez.cavall(14));
		assertEquals(new ArrayList<Integer>(Arrays.asList(23, 27, 34, 36)), Ajedrez.cavall(15));
		assertEquals(new ArrayList<Integer>(Arrays.asList(24, 28, 35, 37)), Ajedrez.cavall(16));
		assertEquals(new ArrayList<Integer>(Arrays.asList(25, 36, 38)), Ajedrez.cavall(17));
		assertEquals(new ArrayList<Integer>(Arrays.asList(26, 37)), Ajedrez.cavall(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 33, 42)), Ajedrez.cavall(21));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 34, 41, 43)), Ajedrez.cavall(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 15, 31, 35, 42, 44)), Ajedrez.cavall(23));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 16, 32, 36, 43, 45)), Ajedrez.cavall(24));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 17, 33, 37, 44, 46)), Ajedrez.cavall(25));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 18, 34, 38, 45, 47)), Ajedrez.cavall(26));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 35, 46, 48)), Ajedrez.cavall(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 36, 47)), Ajedrez.cavall(28));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 23, 43, 52)), Ajedrez.cavall(31));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 13, 24, 44, 51, 53)), Ajedrez.cavall(32));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 14, 21, 25, 41, 45, 52, 54)), Ajedrez.cavall(33));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 15, 22, 26, 42, 46, 53, 55)), Ajedrez.cavall(34));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 16, 23, 27, 43, 47, 54, 56)), Ajedrez.cavall(35));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 17, 24, 28, 44, 48, 55, 57)), Ajedrez.cavall(36));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 18, 25, 45, 56, 58)), Ajedrez.cavall(37));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 26, 46, 57)), Ajedrez.cavall(38));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 33, 53, 62)), Ajedrez.cavall(41));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 23, 34, 54, 61, 63)), Ajedrez.cavall(42));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 24, 31, 35, 51, 55, 62, 64)), Ajedrez.cavall(43));
		assertEquals(new ArrayList<Integer>(Arrays.asList(23, 25, 32, 36, 52, 56, 63, 65)), Ajedrez.cavall(44));
		assertEquals(new ArrayList<Integer>(Arrays.asList(24, 26, 33, 37, 53, 57, 64, 66)), Ajedrez.cavall(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(25, 27, 34, 38, 54, 58, 65, 67)), Ajedrez.cavall(46));
		assertEquals(new ArrayList<Integer>(Arrays.asList(26, 28, 35, 55, 66, 68)), Ajedrez.cavall(47));
		assertEquals(new ArrayList<Integer>(Arrays.asList(27, 36, 56, 67)), Ajedrez.cavall(48));
		assertEquals(new ArrayList<Integer>(Arrays.asList(32, 43, 63, 72)), Ajedrez.cavall(51));
		assertEquals(new ArrayList<Integer>(Arrays.asList(31, 33, 44, 64, 71, 73)), Ajedrez.cavall(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(32, 34, 41, 45, 61, 65, 72, 74)), Ajedrez.cavall(53));
		assertEquals(new ArrayList<Integer>(Arrays.asList(33, 35, 42, 46, 62, 66, 73, 75)), Ajedrez.cavall(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(34, 36, 43, 47, 63, 67, 74, 76)), Ajedrez.cavall(55));
		assertEquals(new ArrayList<Integer>(Arrays.asList(35, 37, 44, 48, 64, 68, 75, 77)), Ajedrez.cavall(56));
		assertEquals(new ArrayList<Integer>(Arrays.asList(36, 38, 45, 65, 76, 78)), Ajedrez.cavall(57));
		assertEquals(new ArrayList<Integer>(Arrays.asList(37, 46, 66, 77)), Ajedrez.cavall(58));
		assertEquals(new ArrayList<Integer>(Arrays.asList(42, 53, 73, 82)), Ajedrez.cavall(61));
		assertEquals(new ArrayList<Integer>(Arrays.asList(41, 43, 54, 74, 81, 83)), Ajedrez.cavall(62));
		assertEquals(new ArrayList<Integer>(Arrays.asList(42, 44, 51, 55, 71, 75, 82, 84)), Ajedrez.cavall(63));
		assertEquals(new ArrayList<Integer>(Arrays.asList(43, 45, 52, 56, 72, 76, 83, 85)), Ajedrez.cavall(64));
		assertEquals(new ArrayList<Integer>(Arrays.asList(44, 46, 53, 57, 73, 77, 84, 86)), Ajedrez.cavall(65));
		assertEquals(new ArrayList<Integer>(Arrays.asList(45, 47, 54, 58, 74, 78, 85, 87)), Ajedrez.cavall(66));
		assertEquals(new ArrayList<Integer>(Arrays.asList(46, 48, 55, 75, 86, 88)), Ajedrez.cavall(67));
		assertEquals(new ArrayList<Integer>(Arrays.asList(47, 56, 76, 87)), Ajedrez.cavall(68));
		assertEquals(new ArrayList<Integer>(Arrays.asList(52, 63, 83)), Ajedrez.cavall(71));
		assertEquals(new ArrayList<Integer>(Arrays.asList(51, 53, 64, 84)), Ajedrez.cavall(72));
		assertEquals(new ArrayList<Integer>(Arrays.asList(52, 54, 61, 65, 81, 85)), Ajedrez.cavall(73));
		assertEquals(new ArrayList<Integer>(Arrays.asList(53, 55, 62, 66, 82, 86)), Ajedrez.cavall(74));
		assertEquals(new ArrayList<Integer>(Arrays.asList(54, 56, 63, 67, 83, 87)), Ajedrez.cavall(75));
		assertEquals(new ArrayList<Integer>(Arrays.asList(55, 57, 64, 68, 84, 88)), Ajedrez.cavall(76));
		assertEquals(new ArrayList<Integer>(Arrays.asList(56, 58, 65, 85)), Ajedrez.cavall(77));
		assertEquals(new ArrayList<Integer>(Arrays.asList(57, 66, 86)), Ajedrez.cavall(78));
		assertEquals(new ArrayList<Integer>(Arrays.asList(62, 73)), Ajedrez.cavall(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(61, 63, 74)), Ajedrez.cavall(82));
		assertEquals(new ArrayList<Integer>(Arrays.asList(62, 64, 71, 75)), Ajedrez.cavall(83));
		assertEquals(new ArrayList<Integer>(Arrays.asList(63, 65, 72, 76)), Ajedrez.cavall(84));
		assertEquals(new ArrayList<Integer>(Arrays.asList(64, 66, 73, 77)), Ajedrez.cavall(85));
		assertEquals(new ArrayList<Integer>(Arrays.asList(65, 67, 74, 78)), Ajedrez.cavall(86));
		assertEquals(new ArrayList<Integer>(Arrays.asList(66, 68, 75)), Ajedrez.cavall(87));
		assertEquals(new ArrayList<Integer>(Arrays.asList(67, 76)), Ajedrez.cavall(88));
		
	}

	@Test
	public void AlfilTest() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 33, 44, 55, 66, 77, 88)), Ajedrez.alfil(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 23, 34, 45, 56, 67, 78)), Ajedrez.alfil(12));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 24, 31, 35, 46, 57, 68)), Ajedrez.alfil(13));
		assertEquals(new ArrayList<Integer>(Arrays.asList(23, 25, 32, 36, 41, 47, 58)), Ajedrez.alfil(14));
		assertEquals(new ArrayList<Integer>(Arrays.asList(24, 26, 33, 37, 42, 48, 51)), Ajedrez.alfil(15));
		assertEquals(new ArrayList<Integer>(Arrays.asList(25, 27, 34, 38, 43, 52, 61)), Ajedrez.alfil(16));
		assertEquals(new ArrayList<Integer>(Arrays.asList(26, 28, 35, 44, 53, 62, 71)), Ajedrez.alfil(17));
		assertEquals(new ArrayList<Integer>(Arrays.asList(27, 36, 45, 54, 63, 72, 81)), Ajedrez.alfil(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 32, 43, 54, 65, 76, 87)), Ajedrez.alfil(21));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 13, 31, 33, 44, 55, 66, 77, 88)), Ajedrez.alfil(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 14, 32, 34, 41, 45, 56, 67, 78)), Ajedrez.alfil(23));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 15, 33, 35, 42, 46, 51, 57, 68)), Ajedrez.alfil(24));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 16, 34, 36, 43, 47, 52, 58, 61)), Ajedrez.alfil(25));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 17, 35, 37, 44, 48, 53, 62, 71)), Ajedrez.alfil(26));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 18, 36, 38, 45, 54, 63, 72, 81)), Ajedrez.alfil(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 37, 46, 55, 64, 73, 82)), Ajedrez.alfil(28));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 22, 42, 53, 64, 75, 86)), Ajedrez.alfil(31));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 21, 23, 41, 43, 54, 65, 76, 87)), Ajedrez.alfil(32));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 15, 22, 24, 42, 44, 51, 55, 66, 77, 88)), Ajedrez.alfil(33));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 16, 23, 25, 43, 45, 52, 56, 61, 67, 78)), Ajedrez.alfil(34));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 17, 24, 26, 44, 46, 53, 57, 62, 68, 71)), Ajedrez.alfil(35));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 18, 25, 27, 45, 47, 54, 58, 63, 72, 81)), Ajedrez.alfil(36));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 26, 28, 46, 48, 55, 64, 73, 82)), Ajedrez.alfil(37));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 27, 47, 56, 65, 74, 83)), Ajedrez.alfil(38));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 23, 32, 52, 63, 74, 85)), Ajedrez.alfil(41));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 24, 31, 33, 51, 53, 64, 75, 86)), Ajedrez.alfil(42));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 21, 25, 32, 34, 52, 54, 61, 65, 76, 87)), Ajedrez.alfil(43));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 17, 22, 26, 33, 35, 53, 55, 62, 66, 71, 77, 88)),Ajedrez.alfil(44));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 18, 23, 27, 34, 36, 54, 56, 63, 67, 72, 78, 81)),Ajedrez.alfil(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 24, 28, 35, 37, 55, 57, 64, 68, 73, 82)),Ajedrez.alfil(46));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 25, 36, 38, 56, 58, 65, 74, 83)),Ajedrez.alfil(47));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 26, 37, 57, 66, 75, 84)),Ajedrez.alfil(48));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 24, 33, 42, 62, 73, 84)),Ajedrez.alfil(51));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 25, 34, 41, 43, 61, 63, 74, 85)),Ajedrez.alfil(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 26, 31, 35, 42, 44, 62, 64, 71, 75, 86)),Ajedrez.alfil(53));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 21, 27, 32, 36, 43, 45, 63, 65, 72, 76, 81, 87)),Ajedrez.alfil(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 22, 28, 33, 37, 44, 46, 64, 66, 73, 77, 82, 88)),Ajedrez.alfil(55));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 23, 34, 38, 45, 47, 65, 67, 74, 78, 83)),Ajedrez.alfil(56));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 24, 35, 46, 48, 66, 68, 75, 84)),Ajedrez.alfil(57));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 25, 36, 47, 67, 76, 85)),Ajedrez.alfil(58));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 25, 34, 43, 52, 72, 83)),Ajedrez.alfil(61));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 26, 35, 44, 51, 53, 71, 73, 84)),Ajedrez.alfil(62));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 27, 36, 41, 45, 52, 54, 72, 74, 81, 85)),Ajedrez.alfil(63));
		assertEquals(new ArrayList<Integer>(Arrays.asList(28, 31, 37, 42, 46, 53, 55, 73, 75, 82, 86)),Ajedrez.alfil(64));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 32, 38, 43, 47, 54, 56, 74, 76, 83, 87)),Ajedrez.alfil(65));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 22, 33, 44, 48, 55, 57, 75, 77, 84, 88)),Ajedrez.alfil(66));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 23, 34, 45, 56, 58, 76, 78, 85)),Ajedrez.alfil(67));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 24, 35, 46, 57, 77, 86)),Ajedrez.alfil(68));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 26, 35, 44, 53, 62, 82)),Ajedrez.alfil(71));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 27, 36, 45, 54, 61, 63, 81, 83)),Ajedrez.alfil(72));
		assertEquals(new ArrayList<Integer>(Arrays.asList(28, 37, 46, 51, 55, 62, 64, 82, 84)),Ajedrez.alfil(73));
		assertEquals(new ArrayList<Integer>(Arrays.asList(38, 41, 47, 52, 56, 63, 65, 83, 85)),Ajedrez.alfil(74));
		assertEquals(new ArrayList<Integer>(Arrays.asList(31, 42, 48, 53, 57, 64, 66, 84, 86)),Ajedrez.alfil(75));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 32, 43, 54, 58, 65, 67, 85, 87)),Ajedrez.alfil(76));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 22, 33, 44, 55, 66, 68, 86, 88)),Ajedrez.alfil(77));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 23, 34, 45, 56, 67, 87)),Ajedrez.alfil(78));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 27, 36, 45, 54, 63, 72)), Ajedrez.alfil(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(28, 37, 46, 55, 64, 71, 73)), Ajedrez.alfil(82));
		assertEquals(new ArrayList<Integer>(Arrays.asList(38, 47, 56, 61, 65, 72, 74)), Ajedrez.alfil(83));
		assertEquals(new ArrayList<Integer>(Arrays.asList(48, 51, 57, 62, 66, 73, 75)), Ajedrez.alfil(84));
		assertEquals(new ArrayList<Integer>(Arrays.asList(41, 52, 58, 63, 67, 74, 76)), Ajedrez.alfil(85));
		assertEquals(new ArrayList<Integer>(Arrays.asList(31, 42, 53, 64, 68, 75, 77)), Ajedrez.alfil(86));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 32, 43, 54, 65, 76, 78)), Ajedrez.alfil(87));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 22, 33, 44, 55, 66, 77)), Ajedrez.alfil(88));
	}

	@Test
	public void TorreTest() {		
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 13, 14, 15, 16, 17, 18, 21, 31, 41, 51, 61, 71, 81)),
			Ajedrez.torre(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 13, 14, 15, 16, 17, 18, 22, 32, 42, 52, 62, 72, 82)),
				Ajedrez.torre(12));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 14, 15, 16, 17, 18, 23, 33, 43, 53, 63, 73, 83)),
				Ajedrez.torre(13));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 15, 16, 17, 18, 24, 34, 44, 54, 64, 74, 84)),
				Ajedrez.torre(14));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 16, 17, 18, 25, 35, 45, 55, 65, 75, 85)),
				Ajedrez.torre(15));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 15, 17, 18, 26, 36, 46, 56, 66, 76, 86)),
				Ajedrez.torre(16));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 15, 16, 18, 27, 37, 47, 57, 67, 77, 87)),
				Ajedrez.torre(17));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 15, 16, 17, 28, 38, 48, 58, 68, 78, 88)),
				Ajedrez.torre(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 22, 23, 24, 25, 26, 27, 28, 31, 41, 51, 61, 71, 81)),
				Ajedrez.torre(21));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 21, 23, 24, 25, 26, 27, 28, 32, 42, 52, 62, 72, 82)),
				Ajedrez.torre(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 21, 22, 24, 25, 26, 27, 28, 33, 43, 53, 63, 73, 83)),
				Ajedrez.torre(23));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 21, 22, 23, 25, 26, 27, 28, 34, 44, 54, 64, 74, 84)),
				Ajedrez.torre(24));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 21, 22, 23, 24, 26, 27, 28, 35, 45, 55, 65, 75, 85)),
				Ajedrez.torre(25));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 21, 22, 23, 24, 25, 27, 28, 36, 46, 56, 66, 76, 86)),
				Ajedrez.torre(26));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 21, 22, 23, 24, 25, 26, 28, 37, 47, 57, 67, 77, 87)),
				Ajedrez.torre(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 21, 22, 23, 24, 25, 26, 27, 38, 48, 58, 68, 78, 88)),
				Ajedrez.torre(28));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 32, 33, 34, 35, 36, 37, 38, 41, 51, 61, 71, 81)),
				Ajedrez.torre(31));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 22, 31, 33, 34, 35, 36, 37, 38, 42, 52, 62, 72, 82)),
				Ajedrez.torre(32));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 31, 32, 34, 35, 36, 37, 38, 43, 53, 63, 73, 83)),
				Ajedrez.torre(33));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 31, 32, 33, 35, 36, 37, 38, 44, 54, 64, 74, 84)),
				Ajedrez.torre(34));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 25, 31, 32, 33, 34, 36, 37, 38, 45, 55, 65, 75, 85)),
				Ajedrez.torre(35));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 26, 31, 32, 33, 34, 35, 37, 38, 46, 56, 66, 76, 86)),
				Ajedrez.torre(36));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 31, 32, 33, 34, 35, 36, 38, 47, 57, 67, 77, 87)),
				Ajedrez.torre(37));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 31, 32, 33, 34, 35, 36, 37, 48, 58, 68, 78, 88)),
				Ajedrez.torre(38));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 31, 42, 43, 44, 45, 46, 47, 48, 51, 61, 71, 81)),
				Ajedrez.torre(41));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 22, 32, 41, 43, 44, 45, 46, 47, 48, 52, 62, 72, 82)),
				Ajedrez.torre(42));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 33, 41, 42, 44, 45, 46, 47, 48, 53, 63, 73, 83)),
				Ajedrez.torre(43));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 34, 41, 42, 43, 45, 46, 47, 48, 54, 64, 74, 84)),
				Ajedrez.torre(44));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 25, 35, 41, 42, 43, 44, 46, 47, 48, 55, 65, 75, 85)),
				Ajedrez.torre(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 26, 36, 41, 42, 43, 44, 45, 47, 48, 56, 66, 76, 86)),
				Ajedrez.torre(46));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 37, 41, 42, 43, 44, 45, 46, 48, 57, 67, 77, 87)),
				Ajedrez.torre(47));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 38, 41, 42, 43, 44, 45, 46, 47, 58, 68, 78, 88)),
				Ajedrez.torre(48));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 31, 41, 52, 53, 54, 55, 56, 57, 58, 61, 71, 81)),
				Ajedrez.torre(51));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 22, 32, 42, 51, 53, 54, 55, 56, 57, 58, 62, 72, 82)),
				Ajedrez.torre(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 33, 43, 51, 52, 54, 55, 56, 57, 58, 63, 73, 83)),
				Ajedrez.torre(53));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 34, 44, 51, 52, 53, 55, 56, 57, 58, 64, 74, 84)),
				Ajedrez.torre(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 25, 35, 45, 51, 52, 53, 54, 56, 57, 58, 65, 75, 85)),
				Ajedrez.torre(55));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 26, 36, 46, 51, 52, 53, 54, 55, 57, 58, 66, 76, 86)),
				Ajedrez.torre(56));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 37, 47, 51, 52, 53, 54, 55, 56, 58, 67, 77, 87)),
				Ajedrez.torre(57));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 38, 48, 51, 52, 53, 54, 55, 56, 57, 68, 78, 88)),
				Ajedrez.torre(58));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 31, 41, 51, 62, 63, 64, 65, 66, 67, 68, 71, 81)),
				Ajedrez.torre(61));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 22, 32, 42, 52, 61, 63, 64, 65, 66, 67, 68, 72, 82)),
				Ajedrez.torre(62));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 33, 43, 53, 61, 62, 64, 65, 66, 67, 68, 73, 83)),
				Ajedrez.torre(63));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 34, 44, 54, 61, 62, 63, 65, 66, 67, 68, 74, 84)),
				Ajedrez.torre(64));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 25, 35, 45, 55, 61, 62, 63, 64, 66, 67, 68, 75, 85)),
				Ajedrez.torre(65));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 26, 36, 46, 56, 61, 62, 63, 64, 65, 67, 68, 76, 86)),
				Ajedrez.torre(66));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 37, 47, 57, 61, 62, 63, 64, 65, 66, 68, 77, 87)),
				Ajedrez.torre(67));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 38, 48, 58, 61, 62, 63, 64, 65, 66, 67, 78, 88)),
				Ajedrez.torre(68));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 31, 41, 51, 61, 72, 73, 74, 75, 76, 77, 78, 81)),
				Ajedrez.torre(71));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 22, 32, 42, 52, 62, 71, 73, 74, 75, 76, 77, 78, 82)),
				Ajedrez.torre(72));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 33, 43, 53, 63, 71, 72, 74, 75, 76, 77, 78, 83)),
				Ajedrez.torre(73));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 34, 44, 54, 64, 71, 72, 73, 75, 76, 77, 78, 84)),
				Ajedrez.torre(74));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 25, 35, 45, 55, 65, 71, 72, 73, 74, 76, 77, 78, 85)),
				Ajedrez.torre(75));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 26, 36, 46, 56, 66, 71, 72, 73, 74, 75, 77, 78, 86)),
				Ajedrez.torre(76));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 37, 47, 57, 67, 71, 72, 73, 74, 75, 76, 78, 87)),
				Ajedrez.torre(77));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 38, 48, 58, 68, 71, 72, 73, 74, 75, 76, 77, 88)),
				Ajedrez.torre(78));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 31, 41, 51, 61, 71, 82, 83, 84, 85, 86, 87, 88)),
				Ajedrez.torre(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 22, 32, 42, 52, 62, 72, 81, 83, 84, 85, 86, 87, 88)),
				Ajedrez.torre(82));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 33, 43, 53, 63, 73, 81, 82, 84, 85, 86, 87, 88)),
				Ajedrez.torre(83));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 34, 44, 54, 64, 74, 81, 82, 83, 85, 86, 87, 88)),
				Ajedrez.torre(84));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 25, 35, 45, 55, 65, 75, 81, 82, 83, 84, 86, 87, 88)),
				Ajedrez.torre(85));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 26, 36, 46, 56, 66, 76, 81, 82, 83, 84, 85, 87, 88)),
				Ajedrez.torre(86));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 37, 47, 57, 67, 77, 81, 82, 83, 84, 85, 86, 88)),
				Ajedrez.torre(87));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 38, 48, 58, 68, 78, 81, 82, 83, 84, 85, 86, 87)),
				Ajedrez.torre(88));
	}

	@Test
	public void DamaTest() {
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 13, 14, 15, 16, 17, 18, 21, 22, 31, 33, 41, 44, 51, 55, 61, 66, 71, 77, 81, 88)),
				Ajedrez.dama(11));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 13, 14, 15, 16, 17, 18, 21, 22, 23, 32, 34, 42, 45, 52, 56, 62, 67, 72, 78, 82)),
				Ajedrez.dama(12));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 14, 15, 16, 17, 18, 22, 23, 24, 31, 33, 35, 43, 46, 53, 57, 63, 68, 73, 83)),
				Ajedrez.dama(13));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 13, 15, 16, 17, 18, 23, 24, 25, 32, 34, 36, 41, 44, 47, 54, 58, 64, 74, 84)),
				Ajedrez.dama(14));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 13, 14, 16, 17, 18, 24, 25, 26, 33, 35, 37, 42, 45, 48, 51, 55, 65, 75, 85)),
				Ajedrez.dama(15));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 13, 14, 15, 17, 18, 25, 26, 27, 34, 36, 38, 43, 46, 52, 56, 61, 66, 76, 86)),
				Ajedrez.dama(16));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 13, 14, 15, 16, 18, 26, 27, 28, 35, 37, 44, 47, 53, 57, 62, 67, 71, 77, 87)),
				Ajedrez.dama(17));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 13, 14, 15, 16, 17, 27, 28, 36, 38, 45, 48, 54, 58, 63, 68, 72, 78, 81, 88)),
				Ajedrez.dama(18));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 22, 23, 24, 25, 26, 27, 28, 31, 32, 41, 43, 51, 54, 61, 65, 71, 76, 81, 87)),
				Ajedrez.dama(21));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 12, 13, 21, 23, 24, 25, 26, 27, 28, 31, 32, 33, 42, 44, 52, 55, 62, 66, 72, 77, 82, 88)),
				Ajedrez.dama(22));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 13, 14, 21, 22, 24, 25, 26, 27, 28, 32, 33, 34, 41, 43, 45, 53, 56, 63, 67, 73, 78, 83)),
				Ajedrez.dama(23));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 14, 15, 21, 22, 23, 25, 26, 27, 28, 33, 34, 35, 42, 44, 46, 51, 54, 57, 64, 68, 74, 84)),
				Ajedrez.dama(24));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 15, 16, 21, 22, 23, 24, 26, 27, 28, 34, 35, 36, 43, 45, 47, 52, 55, 58, 61, 65, 75, 85)),
				Ajedrez.dama(25));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(15, 16, 17, 21, 22, 23, 24, 25, 27, 28, 35, 36, 37, 44, 46, 48, 53, 56, 62, 66, 71, 76, 86)),
				Ajedrez.dama(26));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(16, 17, 18, 21, 22, 23, 24, 25, 26, 28, 36, 37, 38, 45, 47, 54, 57, 63, 67, 72, 77, 81, 87)),
				Ajedrez.dama(27));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(17, 18, 21, 22, 23, 24, 25, 26, 27, 37, 38, 46, 48, 55, 58, 64, 68, 73, 78, 82, 88)),
				Ajedrez.dama(28));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 13, 21, 22, 32, 33, 34, 35, 36, 37, 38, 41, 42, 51, 53, 61, 64, 71, 75, 81, 86)),
				Ajedrez.dama(31));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 14, 21, 22, 23, 31, 33, 34, 35, 36, 37, 38, 41, 42, 43, 52, 54, 62, 65, 72, 76, 82, 87)),
				Ajedrez.dama(32));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 13, 15, 22, 23, 24, 31, 32, 34, 35, 36, 37, 38, 42, 43, 44, 51, 53, 55, 63, 66, 73, 77, 83, 88)),
				Ajedrez.dama(33));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 14, 16, 23, 24, 25, 31, 32, 33, 35, 36, 37, 38, 43, 44, 45, 52, 54, 56, 61, 64, 67, 74, 78, 84)),
				Ajedrez.dama(34));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 15, 17, 24, 25, 26, 31, 32, 33, 34, 36, 37, 38, 44, 45, 46, 53, 55, 57, 62, 65, 68, 71, 75, 85)),
				Ajedrez.dama(35));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 16, 18, 25, 26, 27, 31, 32, 33, 34, 35, 37, 38, 45, 46, 47, 54, 56, 58, 63, 66, 72, 76, 81, 86)),
				Ajedrez.dama(36));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(15, 17, 26, 27, 28, 31, 32, 33, 34, 35, 36, 38, 46, 47, 48, 55, 57, 64, 67, 73, 77, 82, 87)),
				Ajedrez.dama(37));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(16, 18, 27, 28, 31, 32, 33, 34, 35, 36, 37, 47, 48, 56, 58, 65, 68, 74, 78, 83, 88)),
				Ajedrez.dama(38));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 14, 21, 23, 31, 32, 42, 43, 44, 45, 46, 47, 48, 51, 52, 61, 63, 71, 74, 81, 85)),
				Ajedrez.dama(41));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 15, 22, 24, 31, 32, 33, 41, 43, 44, 45, 46, 47, 48, 51, 52, 53, 62, 64, 72, 75, 82, 86)),
				Ajedrez.dama(42));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 16, 21, 23, 25, 32, 33, 34, 41, 42, 44, 45, 46, 47, 48, 52, 53, 54, 61, 63, 65, 73, 76, 83, 87)),
				Ajedrez.dama(43));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 14, 17, 22, 24, 26, 33, 34, 35, 41, 42, 43, 45, 46, 47, 48, 53, 54, 55, 62, 64, 66, 71, 74, 77, 84, 88)),
				Ajedrez.dama(44));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 15, 18, 23, 25, 27, 34, 35, 36, 41, 42, 43, 44, 46, 47, 48, 54, 55, 56, 63, 65, 67, 72, 75, 78, 81, 85)),
				Ajedrez.dama(45));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 16, 24, 26, 28, 35, 36, 37, 41, 42, 43, 44, 45, 47, 48, 55, 56, 57, 64, 66, 68, 73, 76, 82, 86)),
				Ajedrez.dama(46));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 17, 25, 27, 36, 37, 38, 41, 42, 43, 44, 45, 46, 48, 56, 57, 58, 65, 67, 74, 77, 83, 87)),
				Ajedrez.dama(47));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(15, 18, 26, 28, 37, 38, 41, 42, 43, 44, 45, 46, 47, 57, 58, 66, 68, 75, 78, 84, 88)),
				Ajedrez.dama(48));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 15, 21, 24, 31, 33, 41, 42, 52, 53, 54, 55, 56, 57, 58, 61, 62, 71, 73, 81, 84)),
				Ajedrez.dama(51));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 16, 22, 25, 32, 34, 41, 42, 43, 51, 53, 54, 55, 56, 57, 58, 61, 62, 63, 72, 74, 82, 85)),
				Ajedrez.dama(52));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 17, 23, 26, 31, 33, 35, 42, 43, 44, 51, 52, 54, 55, 56, 57, 58, 62, 63, 64, 71, 73, 75, 83, 86)),
				Ajedrez.dama(53));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 18, 21, 24, 27, 32, 34, 36, 43, 44, 45, 51, 52, 53, 55, 56, 57, 58, 63, 64, 65, 72, 74, 76, 81, 84, 87)),
				Ajedrez.dama(54));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 15, 22, 25, 28, 33, 35, 37, 44, 45, 46, 51, 52, 53, 54, 56, 57, 58, 64, 65, 66, 73, 75, 77, 82, 85, 88)),
				Ajedrez.dama(55));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 16, 23, 26, 34, 36, 38, 45, 46, 47, 51, 52, 53, 54, 55, 57, 58, 65, 66, 67, 74, 76, 78, 83, 86)),
				Ajedrez.dama(56));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 17, 24, 27, 35, 37, 46, 47, 48, 51, 52, 53, 54, 55, 56, 58, 66, 67, 68, 75, 77, 84, 87)),
				Ajedrez.dama(57));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 18, 25, 28, 36, 38, 47, 48, 51, 52, 53, 54, 55, 56, 57, 67, 68, 76, 78, 85, 88)),
				Ajedrez.dama(58));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 16, 21, 25, 31, 34, 41, 43, 51, 52, 62, 63, 64, 65, 66, 67, 68, 71, 72, 81, 83)),
				Ajedrez.dama(61));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 17, 22, 26, 32, 35, 42, 44, 51, 52, 53, 61, 63, 64, 65, 66, 67, 68, 71, 72, 73, 82, 84)),
				Ajedrez.dama(62));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 18, 23, 27, 33, 36, 41, 43, 45, 52, 53, 54, 61, 62, 64, 65, 66, 67, 68, 72, 73, 74, 81, 83, 85)),
				Ajedrez.dama(63));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 24, 28, 31, 34, 37, 42, 44, 46, 53, 54, 55, 61, 62, 63, 65, 66, 67, 68, 73, 74, 75, 82, 84, 86)),
				Ajedrez.dama(64));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 17, 22, 27, 33, 37, 44, 47, 55, 57, 66, 67, 68, 71, 72, 73, 74, 75, 76, 78, 86, 87, 88)),
				Ajedrez.dama(77));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 18, 23, 28, 34, 38, 45, 48, 56, 58, 67, 68, 71, 72, 73, 74, 75, 76, 77, 87, 88)),
				Ajedrez.dama(78));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 18, 21, 27, 31, 36, 41, 45, 51, 54, 61, 63, 71, 72, 82, 83, 84, 85, 86, 87, 88)),
				Ajedrez.dama(81));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(12, 22, 28, 32, 37, 42, 46, 52, 55, 62, 64, 71, 72, 73, 81, 83, 84, 85, 86, 87, 88)),
				Ajedrez.dama(82));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(13, 23, 33, 38, 43, 47, 53, 56, 61, 63, 65, 72, 73, 74, 81, 82, 84, 85, 86, 87, 88)),
				Ajedrez.dama(83));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(14, 24, 34, 44, 48, 51, 54, 57, 62, 64, 66, 73, 74, 75, 81, 82, 83, 85, 86, 87, 88)),
				Ajedrez.dama(84));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(15, 25, 35, 41, 45, 52, 55, 58, 63, 65, 67, 74, 75, 76, 81, 82, 83, 84, 86, 87, 88)),
				Ajedrez.dama(85));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(16, 26, 31, 36, 42, 46, 53, 56, 64, 66, 68, 75, 76, 77, 81, 82, 83, 84, 85, 87, 88)),
				Ajedrez.dama(86));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(17, 21, 27, 32, 37, 43, 47, 54, 57, 65, 67, 76, 77, 78, 81, 82, 83, 84, 85, 86, 88)),
				Ajedrez.dama(87));
		assertEquals(new ArrayList<Integer>(
				Arrays.asList(11, 18, 22, 28, 33, 38, 44, 48, 55, 58, 66, 68, 77, 78, 81, 82, 83, 84, 85, 86, 87)),
				Ajedrez.dama(88));
	}

	@Test
	public void PeonTest() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(21)), Ajedrez.peo(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22)), Ajedrez.peo(12));
		assertEquals(new ArrayList<Integer>(Arrays.asList(23)), Ajedrez.peo(13));
		assertEquals(new ArrayList<Integer>(Arrays.asList(24)), Ajedrez.peo(14));
		assertEquals(new ArrayList<Integer>(Arrays.asList(25)), Ajedrez.peo(15));
		assertEquals(new ArrayList<Integer>(Arrays.asList(26)), Ajedrez.peo(16));
		assertEquals(new ArrayList<Integer>(Arrays.asList(27)), Ajedrez.peo(17));
		assertEquals(new ArrayList<Integer>(Arrays.asList(28)), Ajedrez.peo(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(31,41)), Ajedrez.peo(21));
		assertEquals(new ArrayList<Integer>(Arrays.asList(32, 42)), Ajedrez.peo(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(33, 43)), Ajedrez.peo(23));
		assertEquals(new ArrayList<Integer>(Arrays.asList(34, 44)), Ajedrez.peo(24));
		assertEquals(new ArrayList<Integer>(Arrays.asList(35, 45)), Ajedrez.peo(25));
		assertEquals(new ArrayList<Integer>(Arrays.asList(36, 46)), Ajedrez.peo(26));
		assertEquals(new ArrayList<Integer>(Arrays.asList(37, 47)), Ajedrez.peo(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(38, 48)), Ajedrez.peo(28));
		assertEquals(new ArrayList<Integer>(Arrays.asList(41)), Ajedrez.peo(31));
		assertEquals(new ArrayList<Integer>(Arrays.asList(42)), Ajedrez.peo(32));
		assertEquals(new ArrayList<Integer>(Arrays.asList(43)), Ajedrez.peo(33));
		assertEquals(new ArrayList<Integer>(Arrays.asList(44)), Ajedrez.peo(34));
		assertEquals(new ArrayList<Integer>(Arrays.asList(45)), Ajedrez.peo(35));
		assertEquals(new ArrayList<Integer>(Arrays.asList(46)), Ajedrez.peo(36));
		assertEquals(new ArrayList<Integer>(Arrays.asList(47)), Ajedrez.peo(37));
		assertEquals(new ArrayList<Integer>(Arrays.asList(48)), Ajedrez.peo(38));
		assertEquals(new ArrayList<Integer>(Arrays.asList(51)), Ajedrez.peo(41));
		assertEquals(new ArrayList<Integer>(Arrays.asList(52)), Ajedrez.peo(42));
		assertEquals(new ArrayList<Integer>(Arrays.asList(53)), Ajedrez.peo(43));
		assertEquals(new ArrayList<Integer>(Arrays.asList(54)), Ajedrez.peo(44));
		assertEquals(new ArrayList<Integer>(Arrays.asList(55)), Ajedrez.peo(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(56)), Ajedrez.peo(46));
		assertEquals(new ArrayList<Integer>(Arrays.asList(57)), Ajedrez.peo(47));
		assertEquals(new ArrayList<Integer>(Arrays.asList(58)), Ajedrez.peo(48));
		assertEquals(new ArrayList<Integer>(Arrays.asList(61)), Ajedrez.peo(51));
		assertEquals(new ArrayList<Integer>(Arrays.asList(62)), Ajedrez.peo(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(63)), Ajedrez.peo(53));
		assertEquals(new ArrayList<Integer>(Arrays.asList(64)), Ajedrez.peo(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(65)), Ajedrez.peo(55));
		assertEquals(new ArrayList<Integer>(Arrays.asList(66)), Ajedrez.peo(56));
		assertEquals(new ArrayList<Integer>(Arrays.asList(67)), Ajedrez.peo(57));
		assertEquals(new ArrayList<Integer>(Arrays.asList(68)), Ajedrez.peo(58));
		assertEquals(new ArrayList<Integer>(Arrays.asList(71)), Ajedrez.peo(61));
		assertEquals(new ArrayList<Integer>(Arrays.asList(72)), Ajedrez.peo(62));
		assertEquals(new ArrayList<Integer>(Arrays.asList(73)), Ajedrez.peo(63));
		assertEquals(new ArrayList<Integer>(Arrays.asList(74)), Ajedrez.peo(64));
		assertEquals(new ArrayList<Integer>(Arrays.asList(75)), Ajedrez.peo(65));
		assertEquals(new ArrayList<Integer>(Arrays.asList(76)), Ajedrez.peo(66));
		assertEquals(new ArrayList<Integer>(Arrays.asList(77)), Ajedrez.peo(67));
		assertEquals(new ArrayList<Integer>(Arrays.asList(78)), Ajedrez.peo(68));
		assertEquals(new ArrayList<Integer>(Arrays.asList(81)), Ajedrez.peo(71));
		assertEquals(new ArrayList<Integer>(Arrays.asList(82)), Ajedrez.peo(72));
		assertEquals(new ArrayList<Integer>(Arrays.asList(83)), Ajedrez.peo(73));
		assertEquals(new ArrayList<Integer>(Arrays.asList(84)), Ajedrez.peo(74));
		assertEquals(new ArrayList<Integer>(Arrays.asList(85)), Ajedrez.peo(75));
		assertEquals(new ArrayList<Integer>(Arrays.asList(86)), Ajedrez.peo(76));
		assertEquals(new ArrayList<Integer>(Arrays.asList(87)), Ajedrez.peo(77));
		assertEquals(new ArrayList<Integer>(Arrays.asList(88)), Ajedrez.peo(78));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(82));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(83));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(84));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(85));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(86));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(87));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Ajedrez.peo(88));
	}

	@Test
	public void ReyTest() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 21, 22)), Ajedrez.rei(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 13, 21, 22, 23)), Ajedrez.rei(12));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 14, 22, 23, 24)), Ajedrez.rei(13));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 15, 23, 24, 25)), Ajedrez.rei(14));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 16, 24, 25, 26)), Ajedrez.rei(15));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 17, 25, 26, 27)), Ajedrez.rei(16));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 18, 26, 27, 28)), Ajedrez.rei(17));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 28)), Ajedrez.rei(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 22, 31, 32)), Ajedrez.rei(21));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 21, 23, 31, 32, 33)), Ajedrez.rei(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 13, 14, 22, 24, 32, 33, 34)), Ajedrez.rei(23));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 14, 15, 23, 25, 33, 34, 35)), Ajedrez.rei(24));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 15, 16, 24, 26, 34, 35, 36)), Ajedrez.rei(25));
		assertEquals(new ArrayList<Integer>(Arrays.asList(15, 16, 17, 25, 27, 35, 36, 37)), Ajedrez.rei(26));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 17, 18, 26, 28, 36, 37, 38)), Ajedrez.rei(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 18, 27, 37, 38)), Ajedrez.rei(28));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 22, 32, 41, 42)), Ajedrez.rei(31));
		assertEquals(new ArrayList<Integer>(Arrays.asList(21, 22, 23, 31, 33, 41, 42, 43)), Ajedrez.rei(32));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 23, 24, 32, 34, 42, 43, 44)), Ajedrez.rei(33));
		assertEquals(new ArrayList<Integer>(Arrays.asList(23, 24, 25, 33, 35, 43, 44, 45)), Ajedrez.rei(34));
		assertEquals(new ArrayList<Integer>(Arrays.asList(24, 25, 26, 34, 36, 44, 45, 46)), Ajedrez.rei(35));
		assertEquals(new ArrayList<Integer>(Arrays.asList(25, 26, 27, 35, 37, 45, 46, 47)), Ajedrez.rei(36));
		assertEquals(new ArrayList<Integer>(Arrays.asList(26, 27, 28, 36, 38, 46, 47, 48)), Ajedrez.rei(37));
		assertEquals(new ArrayList<Integer>(Arrays.asList(27, 28, 37, 47, 48)), Ajedrez.rei(38));
		assertEquals(new ArrayList<Integer>(Arrays.asList(31, 32, 42, 51, 52)), Ajedrez.rei(41));
		assertEquals(new ArrayList<Integer>(Arrays.asList(31, 32, 33, 41, 43, 51, 52, 53)), Ajedrez.rei(42));
		assertEquals(new ArrayList<Integer>(Arrays.asList(32, 33, 34, 42, 44, 52, 53, 54)), Ajedrez.rei(43));
		assertEquals(new ArrayList<Integer>(Arrays.asList(33, 34, 35, 43, 45, 53, 54, 55)), Ajedrez.rei(44));
		assertEquals(new ArrayList<Integer>(Arrays.asList(34, 35, 36, 44, 46, 54, 55, 56)), Ajedrez.rei(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(35, 36, 37, 45, 47, 55, 56, 57)), Ajedrez.rei(46));
		assertEquals(new ArrayList<Integer>(Arrays.asList(36, 37, 38, 46, 48, 56, 57, 58)), Ajedrez.rei(47));
		assertEquals(new ArrayList<Integer>(Arrays.asList(37, 38, 47, 57, 58)), Ajedrez.rei(48));
		assertEquals(new ArrayList<Integer>(Arrays.asList(41, 42, 52, 61, 62)), Ajedrez.rei(51));
		assertEquals(new ArrayList<Integer>(Arrays.asList(41, 42, 43, 51, 53, 61, 62, 63)), Ajedrez.rei(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(42, 43, 44, 52, 54, 62, 63, 64)), Ajedrez.rei(53));
		assertEquals(new ArrayList<Integer>(Arrays.asList(43, 44, 45, 53, 55, 63, 64, 65)), Ajedrez.rei(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(44, 45, 46, 54, 56, 64, 65, 66)), Ajedrez.rei(55));
		assertEquals(new ArrayList<Integer>(Arrays.asList(45, 46, 47, 55, 57, 65, 66, 67)), Ajedrez.rei(56));
		assertEquals(new ArrayList<Integer>(Arrays.asList(46, 47, 48, 56, 58, 66, 67, 68)), Ajedrez.rei(57));
		assertEquals(new ArrayList<Integer>(Arrays.asList(47, 48, 57, 67, 68)), Ajedrez.rei(58));
		assertEquals(new ArrayList<Integer>(Arrays.asList(51, 52, 62, 71, 72)), Ajedrez.rei(61));
		assertEquals(new ArrayList<Integer>(Arrays.asList(51, 52, 53, 61, 63, 71, 72, 73)), Ajedrez.rei(62));
		assertEquals(new ArrayList<Integer>(Arrays.asList(52, 53, 54, 62, 64, 72, 73, 74)), Ajedrez.rei(63));
		assertEquals(new ArrayList<Integer>(Arrays.asList(53, 54, 55, 63, 65, 73, 74, 75)), Ajedrez.rei(64));
		assertEquals(new ArrayList<Integer>(Arrays.asList(54, 55, 56, 64, 66, 74, 75, 76)), Ajedrez.rei(65));
		assertEquals(new ArrayList<Integer>(Arrays.asList(55, 56, 57, 65, 67, 75, 76, 77)), Ajedrez.rei(66));
		assertEquals(new ArrayList<Integer>(Arrays.asList(56, 57, 58, 66, 68, 76, 77, 78)), Ajedrez.rei(67));
		assertEquals(new ArrayList<Integer>(Arrays.asList(57, 58, 67, 77, 78)), Ajedrez.rei(68));
		assertEquals(new ArrayList<Integer>(Arrays.asList(61, 62, 72, 81, 82)), Ajedrez.rei(71));
		assertEquals(new ArrayList<Integer>(Arrays.asList(61, 62, 63, 71, 73, 81, 82, 83)), Ajedrez.rei(72));
		assertEquals(new ArrayList<Integer>(Arrays.asList(62, 63, 64, 72, 74, 82, 83, 84)), Ajedrez.rei(73));
		assertEquals(new ArrayList<Integer>(Arrays.asList(63, 64, 65, 73, 75, 83, 84, 85)), Ajedrez.rei(74));
		assertEquals(new ArrayList<Integer>(Arrays.asList(64, 65, 66, 74, 76, 84, 85, 86)), Ajedrez.rei(75));
		assertEquals(new ArrayList<Integer>(Arrays.asList(65, 66, 67, 75, 77, 85, 86, 87)), Ajedrez.rei(76));
		assertEquals(new ArrayList<Integer>(Arrays.asList(66, 67, 68, 76, 78, 86, 87, 88)), Ajedrez.rei(77));
		assertEquals(new ArrayList<Integer>(Arrays.asList(67, 68, 77, 87, 88)), Ajedrez.rei(78));
		assertEquals(new ArrayList<Integer>(Arrays.asList(71, 72, 82)), Ajedrez.rei(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(71, 72, 73, 81, 83)), Ajedrez.rei(82));
		assertEquals(new ArrayList<Integer>(Arrays.asList(72, 73, 74, 82, 84)), Ajedrez.rei(83));
		assertEquals(new ArrayList<Integer>(Arrays.asList(73, 74, 75, 83, 85)), Ajedrez.rei(84));
		assertEquals(new ArrayList<Integer>(Arrays.asList(74, 75, 76, 84, 86)), Ajedrez.rei(85));
		assertEquals(new ArrayList<Integer>(Arrays.asList(75, 76, 77, 85, 87)), Ajedrez.rei(86));
		assertEquals(new ArrayList<Integer>(Arrays.asList(76, 77, 78, 86, 88)), Ajedrez.rei(87));
		assertEquals(new ArrayList<Integer>(Arrays.asList(77, 78, 87)), Ajedrez.rei(88));
	}
}
