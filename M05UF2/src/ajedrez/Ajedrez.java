package ajedrez;

import java.util.ArrayList;
import java.util.Scanner;
/**
 * <h1 style="text-align:center">Programa piezas de ajedrez</h1>
 * <b>Descripcion basica del programa:</b><br>
 * 
 * Programa que calcula los posibles moviminetos de una pieza de ajedrez determinada en una posicion del tablero tambien determinada por el usuario.

 * @author Raul Edo Andres

 * @version 01-03-2023

 * @see <a href = "https://gitlab.com/Raul_EA/1r-ano-dam" /> Link de mi Gitlab donde tengo mas programas interesantes</a>

 */
public class Ajedrez {

	static Scanner sc = new Scanner(System.in);
	/**

	 * Es la funcion main del programa en la cual se llama diferentes funciones para pedir la pieza y posicion y llama a otra para que te devuelva las posiciones posibles.

	 */
	public static void main(String[] args) {
		char a=menu();
		int xy=0;
		while(a!='S') {
			xy = pos();
			System.out.println(moviments(xy,a));
			a = menu();
		} 
	}
	/**

	 * Superfuncion que determina los movimientos de una pieza en la casilla indicada

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY. 

	 * @param a variable de un caracter que representa que tipo de pieza es.

	 * @return mov - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY.  

	 */
	public static ArrayList<Integer> moviments (int xy,char a){
		ArrayList<Integer> mov=null;
		switch (a) {
		case 'C':
			mov = cavall(xy);
			break;
		case 'A':
			mov = alfil(xy);
			break;
		case 'T':
			mov = torre(xy);
			break;
		case 'D':
			mov = dama(xy);
			break;
		case 'P':
			mov = peo(xy);
			break;
		case 'R':
			mov = rei(xy);
			break;
		}
		return mov;
	}
	/**

	 * Funcion encargada de devolver las posiciones en caso de que la pieza sea el rey.

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY.
	 
	 * @return posi - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY, que estan ordenadas numericamente.

	 */
	public static ArrayList<Integer> rei(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		for(int i=-1;i<=1;i++) {
			for(int j=-1;j<=1;j++) {
				if(x+i!=x || y+j!=y)
					if(x+i<=8 && x+i>=1 && y+j>=1 && y+j<=8)
						posi.add((x+i) * 10 + y+j);
			}
		}
		posi.sort(null);
		return posi;
	}
	/**

	 * Funcion encargada de devolver las posiciones en caso de que la pieza sea el peon.

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY.
	 
	 * @return posi - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY, que estan ordenadas numericamente.

	 */
	public static ArrayList<Integer> peo(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		if(x==2) {
			posi.add((x+1) * 10 + y);
			posi.add((x+2) * 10 + y);
		}else {
			if(x+1<=8)
				posi.add((x+1) * 10 + y);
		}
		posi.sort(null);
		return posi;
	}
	/**

	 * Funcion encargada de devolver las posiciones en caso de que la pieza sea el dama.

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY.
	 
	 * @return posi - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY, que estan ordenadas numericamente.

	 */
	public static ArrayList<Integer> dama(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		posi.addAll(alfil(xy));
		posi.addAll(torre(xy));
		posi.sort(null);
		return posi;
	}
	/**

	 * Funcion encargada de devolver las posiciones en caso de que la pieza sea el torre.

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY.
	 
	 * @return posi - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY, que estan ordenadas numericamente.

	 */
	public static ArrayList<Integer> torre(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		int auxX = x;
		int auxY = y;
		for (int i = 0; i < 4; i++) {
			while (auxX >= 1 && auxY >= 1 && auxX <= 8 && auxY <= 8) {
				if (auxX != x || auxY != y) {
					posi.add(auxX * 10 + auxY);
				}
				switch (i) {
				case 0:
					auxX--;
					break;
				case 1:
					auxX++;
					break;
				case 2:
					auxY++;
					break;
				case 3:
					auxY--;
					break;
				}
			}
			auxX = x;
			auxY = y;
		}
		posi.sort(null);
		return posi;
	}
	/**

	 * Funcion encargada de devolver las posiciones en caso de que la pieza sea el alfil.

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY.
	 
	 * @return posi - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY, que estan ordenadas numericamente.

	 */
	public static ArrayList<Integer> alfil(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		int auxX = x;
		int auxY = y;
		for (int i = 0; i < 4; i++) {
			while (auxX >= 1 && auxY >= 1 && auxX <= 8 && auxY <= 8) {
				if (auxX != x && auxY != y) {
					posi.add(auxX * 10 + auxY);
				}
				switch (i) {
				case 0:
					auxX--;
					auxY--;
					break;
				case 1:
					auxX++;
					auxY--;
					break;
				case 2:
					auxX--;
					auxY++;
					break;
				case 3:
					auxX++;
					auxY++;
					break;
				}
			}
			auxX = x;
			auxY = y;
		}
		posi.sort(null);
		return posi;
	}
	/**

	 * Funcion encargada de devolver las posiciones en caso de que la pieza sea el caballo.

	 * @param xy variable de numero entero que representa la posicion "x" y "y" de la pieza en formato XY.
	 
	 * @return posi - ArrayList que contiene las posiciones del tablero en las que se puede mover la pieza, tambien en formato XY, que estan ordenadas numericamente.

	 */
	public static ArrayList<Integer> cavall(int xy) {
		ArrayList<Integer> posi = new ArrayList<>();
		int x = xy / 10;
		int y = xy % 10;
		for(int i=-2;i<=2;i+=4) {
			if(y+i>=1 && y+i<=8) {
				if(x+1>=1 && x+1<=8) {
					posi.add((x+1) * 10 + y+i);
				}
				if(x-1>=1 && x-1<=8) {
					posi.add((x-1) * 10 + y+i);
				}
			}
		}
		for(int i=-2;i<=2;i+=4) {
			if(x+i>=1 && x+i<=8) {
				if(y+1>=1 && y+1<=8) {
					posi.add((x+i) * 10 + y+1);
				}
				if(y-1>=1 && y-1<=8) {
					posi.add((x+i) * 10 + y-1);
				}
			}
		}
		posi.sort(null);
		return posi;
	}
	/**

	 * Funcion encargada de mostrar las opciones de las diferentes piezas, pedir que se introduzca por consola y verificar que es valido.
	 
	 * @return a - variable de un caracter que contiene la primera letra de la pieza en la que se va calcular sus moviminetos.

	 */
	public static char menu() {
		System.out.println("Seleccione el tipo de pieza:");
		System.out.println("C: caballo");
		System.out.println("A: alfil");
		System.out.println("T: torre");
		System.out.println("D: dama");
		System.out.println("P: peon");
		System.out.println("R: rey");
		System.out.println("S: salir");
		char a = sc.next().toUpperCase().charAt(0);
		while (a!='S' && a != 'C' && a != 'A' && a != 'T' && a != 'D' && a != 'P' && a != 'R') {
			System.out.println("Valor introducido incorrecto, porfavor vuelva a introducirlo");
			System.out.println("Seleccione el tipo de pieza:");
			System.out.println("C: caballo");
			System.out.println("A: alfil");
			System.out.println("T: torre");
			System.out.println("D: dama");
			System.out.println("P: peon");
			System.out.println("R: rey");
			System.out.println("S: salir");
			a = sc.next().toUpperCase().charAt(0);
		}
		return a;
	}
	/**

	 * Funcion encargada de pedir que se introduzca por consola la posicion de la pieza y verificar que es una posicion valida.
	 
	 * @return a - variable numerico entero que contiene la posicion de la pieza en formato XY.

	 */
	public static int pos() {
		System.out.println("Porfavor introduzca la posicion de la pieza en el formato FC (Fila Columna):");
		int a = sc.nextInt();
		while (a < 11 || a > 88 || a%10==0 || a%10==9) {
			System.out.println("Valor introducido incorrecto, porfavor vuelva a introducirlo");
			System.out.println("Porfavor introduzca la posicion de la pieza en el formato FC (Fila Columna):");
			a = sc.nextInt();
		}
		return a;
	}
}