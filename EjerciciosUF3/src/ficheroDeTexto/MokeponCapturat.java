package ficheroDeTexto;

public class MokeponCapturat extends Mokepon{
	
	private static int NombreMokeponsCapturats=0;
	
	private String nomPosat;
	private String nomEntrenador;
	private int felicitat;
	private Objecte objecte;
	private Equipament objecteEquipat;
	
	public MokeponCapturat(String nom,Tipus tipus) {
		super(nom,tipus);
		this.nomPosat=nom;
		this.nomEntrenador="Marc";
		this.felicitat=50;
		NombreMokeponsCapturats++;
	}
	
	public MokeponCapturat(String nom,int nivell,int hp_max,int atk,int def,int vel) {
		super(nom,nivell,hp_max,atk,def,vel);
		this.nomPosat=nom;
		this.nomEntrenador="Marc";
		this.felicitat=50;
		NombreMokeponsCapturats++;
	}
	public MokeponCapturat(String nom,int nivell) {
		super(nom,nivell);
		this.nomPosat=nom;
		this.nomEntrenador="Marc";
		this.felicitat=50;
		NombreMokeponsCapturats++;
	}
	public MokeponCapturat() {
		super();
		this.nomPosat=this.getNom();
		this.nomEntrenador="Marc";
		this.felicitat=50;
		NombreMokeponsCapturats++;
	}
	public MokeponCapturat(String nom) {
		super(nom);
		this.nomPosat=nom;
		this.nomEntrenador="Marc";
		this.felicitat=50;
		NombreMokeponsCapturats++;
	}
	
	public MokeponCapturat(Mokepon mok,String nomPosat,String nomEntrenador) {
		super(mok);
		this.nomPosat=nomPosat;
		this.nomEntrenador=nomEntrenador;
		this.felicitat=50;
		NombreMokeponsCapturats++;
	}
	
	public void acariciar() {
		if(this.felicitat<100)
			this.felicitat+=10;
		if(this.felicitat>100) {
			this.felicitat=100;
		}
	}
	

	@Override
	public String toString() {
		String a=super.toString();
		a+="\nNomPosat: "+this.nomPosat+"\nNomEntrenador: "+this.nomEntrenador+"\nFelicitat: "+this.felicitat;
		return a;
	}

	@Override
	public void atacar(Mokepon atacat, int num_atac) {
		if(this.isDebilitat()==false) {
			double damage = ((((2*this.getNivell())/2)*this.getAtacs().get(num_atac).poder*(this.getAtk()/atacat.getDef())/50)+2) * super.efectivitat(this.getAtacs().get(num_atac).tipus,atacat.getTipus());
			if(this.felicitat>=50) {
				damage=damage*(1.2);
			}else {
				damage=damage*(0.8);
			}
			atacat.setHp_actual(atacat.getHp_actual()-(int)damage);
			if(atacat.getHp_actual()<=0) {
				atacat.debilitarse();
			}
		}
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj)
			return true;
		if(obj==null)
			return false;
		if(getClass()!=obj.getClass())
			return false;
		
		MokeponCapturat other = (MokeponCapturat)obj;
		if(this.getAtk()==other.getAtk()&&
			this.getDef()==other.getDef()&&
			this.getHp_max()==other.getHp_max()&&this.getNivell()==other.getNivell()&&
			this.getNom()==other.getNom()&&this.getSexe()==other.getSexe()&&
			this.getTipus()==other.getTipus()&&this.getVel()==other.getVel()&&
			this.nomEntrenador==other.nomEntrenador && this.nomPosat==other.nomPosat)
			return true;
		else
			return false;
 	}
	public void utilizarObjecte() {
		objecte.utilizar(this);
	}
	
	public Objecte getObjecte() {
		return objecte;
	}

	public void setObjecte(Objecte objecte) {
		this.objecte = objecte;
	}

	public String getNomPosat() {
		return nomPosat;
	}

	public void setNomPosat(String nomPosat) {
		this.nomPosat = nomPosat;
	}

	public String getNomEntrenador() {
		return nomEntrenador;
	}

	public void setNomEntrenador(String nomEntrenador) {
		this.nomEntrenador = nomEntrenador;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public static int getNombreMokeponsCapturats() {
		return NombreMokeponsCapturats;
	}

	public Equipament getObjecteEquipat() {
		return objecteEquipat;
	}

	public void setObjecteEquipat(Equipament objecteEquipat) {
		this.objecteEquipat = objecteEquipat;
	}	
}

