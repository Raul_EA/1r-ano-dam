package ficheroDeTexto;

import java.util.ArrayList;
import java.util.Random;

public class Mokepon implements Comparable<Object>{
	
	public static Random rd = new Random();
	
	private String nom;
	private int nivell;
	private int atk;
	private int def;
	private int vel;
	private int exp;
	private int hp_max;
	private int hp_actual;
	private Tipus tipus;
	private ArrayList<Atac> atacs= new ArrayList<>(2);
	private boolean debilitat;
	private Sexe sexe;
	
	public Mokepon() {
		this.nom="Sense definir";
		this.nivell=1;
		this.atk=1;
		this.def=1;
		this.vel=1;
		this.hp_max=10;
		this.hp_actual=this.hp_max;
		this.sexe=rd.nextBoolean()?Sexe.MASCLE:Sexe.FEMELLA;
		this.debilitat=false;
	}
	
	public Mokepon(String nom) {
		this();
		this.nom=nom;
		
	}
	
	public Mokepon(String nom,int nivell) {
		this(nom);
		for(int i=1;i<nivell;i++) {
			this.pujarNivell();
		}
	}
	
	public Mokepon(String nom,Tipus tipus) {
		this();
		this.nom=nom;
		this.tipus=tipus;
	}
	
	public Mokepon(String nom,int nivell,int hp_max,int atk,int def,int vel) {
		this();
		this.nom=nom;
		this.nivell=nivell;
		this.atk=atk;
		this.def=def;
		this.vel=vel;
		this.hp_max=hp_max;
		this.hp_actual=this.hp_max;
	}
	
	public Mokepon(String nom,int nivell,int hp_max,int atk,int def,int vel,Tipus tipus) {
		this();
		this.nom=nom;
		this.nivell=nivell;
		this.atk=atk;
		this.def=def;
		this.vel=vel;
		this.hp_max=hp_max;
		this.hp_actual=this.hp_max;
		this.tipus=tipus;
	}
	
	public Mokepon(Mokepon mok) {
		this.nom=mok.getNom();
		this.nivell=mok.getNivell();
		this.atk=mok.getAtk();
		this.def=mok.getDef();
		this.vel=mok.getVel();
		this.hp_max=mok.getHp_max();
		this.hp_actual=this.hp_max;
		this.exp=mok.getExp();
		this.tipus=mok.getTipus();
		this.atacs=mok.getAtacs();
		this.debilitat=mok.isDebilitat();
		this.sexe=mok.getSexe();
	}
	
	public void diguesNom() {
		System.out.println(this.nom);
	}
	
	public void atorgarExperiencia(int exp_atorgada) {
		this.exp+=exp_atorgada;
		if(this.exp>=100) {
			this.exp-=100;
			pujarNivell();
		}
	}
	
	public void pujarNivell() {
		this.nivell++;
		this.hp_max+=rd.nextInt(6);
		this.atk+=rd.nextInt(3);
		this.def+=rd.nextInt(3);
		this.vel+=rd.nextInt(3);
	}
	
	public void afegirAtac(Atac at) {
		if(this.atacs.size()<2) {
			this.atacs.add(at);
		}
	}
	
	public double efectivitat(Tipus atac,Tipus defensa) {
		if(atac==Tipus.FOC && defensa==Tipus.AIGUA || atac == Tipus.AIGUA && defensa==Tipus.PLANTA || atac==Tipus.PLANTA && defensa== Tipus.FOC) {
			return 0.5;
		}else if(atac == Tipus.AIGUA && defensa==Tipus.FOC || atac == Tipus.FOC && defensa == Tipus.PLANTA || atac == Tipus.PLANTA && defensa == Tipus.AIGUA){
			return 2;
		}else {
			return 1;
		}
	}
	
	public void atacar(Mokepon atacat, int num_atac) {
		if(this.debilitat==false) {
			double damage = ((((2*this.nivell)/2)*this.atacs.get(num_atac).poder*(this.atk/atacat.def)/50)+2) * efectivitat(this.atacs.get(num_atac).tipus,atacat.tipus);
			atacat.hp_actual-=(int)damage;
			if(atacat.hp_actual<=0) {
				atacat.debilitarse();
			}
		}
	}
	
	public void debilitarse() {
		this.debilitat=true;
	}
	public void curar() {
		if(this.debilitat==true)
			this.debilitat=false;
		this.hp_actual=this.hp_max;
	}
	
	public MokeponCapturat capturar(String nomEntrenador, String nomDonat) throws Exception{
		if(!(this instanceof MokeponCapturat)) {
			MokeponCapturat mc = new MokeponCapturat(this,nomEntrenador,nomDonat);
			return mc;
		}else {
			throw new MokeponJaCapturatException("Error, ja esta caapturat");
		}
	}
	
	public Ou reproduccio(Mokepon a) throws Exception {
		if(a.getTipus()!=this.getTipus())
			throw new TipusDiferentException("Error, son de tipus diferents");
		if(a.getSexe()==this.getSexe())
			throw new SexeDiferentException("Error, son del mateix sexe");
		if(a.isDebilitat() || this.isDebilitat())
			throw new DebilitatException("Error, algun mokepon esta debilitat");
		return new Ou(rd.isDeprecated()?a.getNom():this.getNom(),a.getTipus());
	}
	@Override
	public String toString() {
		return "MokeponCapturat [nom=" + nom + ", nivell=" + nivell + ", atk=" + atk + ", def=" + def + ", vel=" + vel + ", exp="
				+ exp + ", hp_max=" + hp_max + ", hp_actual=" + hp_actual + ", tipus=" + tipus + ", atacs=" + atacs
				+ ", debilitat=" + debilitat + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj)
			return true;
		if(obj==null)
			return false;
		if(getClass()!=obj.getClass())
			return false;
		
		Mokepon other = (Mokepon)obj;
		if(this.atacs==other.atacs&&this.atk==other.atk&&
			this.def==other.def&&this.exp==other.exp&&
			this.hp_max==other.hp_max&&this.nivell==other.nivell&&
			this.nom==other.nom&&this.sexe==other.sexe&&
			this.tipus==other.tipus&&this.vel==other.vel)
			return true;
		else
			return false;
 	}
	@Override
	public int compareTo (Object arg0) {
		Mokepon altre = (Mokepon)arg0;
		if(altre.tipus.ordinal()>this.tipus.ordinal())
			return -1;
		else if (altre.tipus.ordinal()<this.tipus.ordinal())
			return 1;
		else if(this.nom.compareTo(altre.nom)!=0)
			return this.nom.compareTo(altre.nom);
		else if(altre.nivell>this.nivell)
			return -1; 
		else if(altre.nivell<this.nivell)
			return 1;
		else
			return 0;
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNivell() {
		return nivell;
	}

	public void setNivell(int nivell) {
		this.nivell = nivell;
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getVel() {
		return vel;
	}

	public void setVel(int vel) {
		this.vel = vel;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public int getHp_max() {
		return hp_max;
	}

	public void setHp_max(int hp_max) {
		this.hp_max = hp_max;
	}

	public int getHp_actual() {
		return hp_actual;
	}

	public void setHp_actual(int hp_actual) {
		this.hp_actual = hp_actual;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public ArrayList<Atac> getAtacs() {
		return atacs;
	}

	public void setAtacs(ArrayList<Atac> atacs) {
		this.atacs = atacs;
	}

	public boolean isDebilitat() {
		return debilitat;
	}

	public void setDebilitat(boolean debilitat) {
		this.debilitat = debilitat;
	}

	public Sexe getSexe() {
		return sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}
	
}
