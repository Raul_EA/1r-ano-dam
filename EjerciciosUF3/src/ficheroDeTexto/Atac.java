package ficheroDeTexto;

public class Atac implements Comparable<Object>{
	String nom;
	double poder;
	Tipus tipus;
	int moviments_maxims;
	int moviments_actuals;
	
	public Atac(String nom,double poder,Tipus tipus,int moviments_maxims) {
		this.moviments_actuals=moviments_maxims;
		this.moviments_maxims=moviments_maxims;
		this.nom=nom;
		this.tipus=tipus;
		if(poder>100) {
			this.poder=100;
		}else if(poder<10) {
			this.poder=10;
		}else {
			this.poder=poder;
		}
	}
	public Atac(String nom,Tipus tipus) {
		this.nom=nom;
		this.tipus=tipus;
		this.poder=10;
		this.moviments_maxims=10;
	}
	@Override
	public boolean equals(Object obj) {
		if (this==obj)
			return true;
		if(obj==null)
			return false;
		if(getClass()!=obj.getClass())
			return false;
		
		Atac other = (Atac)obj;
		if(tipus==other.tipus && nom.equals(other.nom)&& poder==other.poder&&moviments_actuals==other.moviments_actuals&&moviments_maxims==other.moviments_maxims)
			return true;
		else
			return false;
	}
	@Override 
	public int compareTo(Object arg0) {
		Atac altre = (Atac) arg0;
		if(altre.poder>this.poder) 
			return -1;
		else if(this.poder>altre.poder)
			return 1;
		else 
			return this.nom.compareTo(altre.nom);
	}
}
