package ficheroDeTexto;

public abstract class Objecte {
	
	private String nom;
	private int quantitat;
	
	Objecte (String nom){
		this.nom=nom;
		this.quantitat=1;
	}
	
	public abstract void utilizar(Mokepon a);
	
	public void obtenir(int numObjectes) {
		quantitat+=numObjectes;
	}
	
	public void donar(MokeponCapturat a) {
		a.setObjecte(this);
	}
	
	public String getNom() {
		return nom;
	}
	
	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
	
}
