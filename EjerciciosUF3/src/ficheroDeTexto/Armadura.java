package ficheroDeTexto;

public class Armadura extends Objecte implements Equipament {

	private int defExtra;

	Armadura(String nom, int defExtra) {
		super(nom);
		this.defExtra = defExtra;
	}
	
	public void utilizar(Mokepon a) {
		this.equipar((MokeponCapturat) a);
	};
	public void equipar(MokeponCapturat a) {
		a.setObjecteEquipat(this);
		a.setDef(a.getDef() + defExtra);
	}

	public void desequipar(MokeponCapturat a) {
		a.setObjecteEquipat(null);
		a.setDef(a.getDef() - defExtra);
	}

	public int getDefExtra() {
		return defExtra;
	}

	public void setDefExtra(int defExtra) {
		this.defExtra = defExtra;
	}
	
}