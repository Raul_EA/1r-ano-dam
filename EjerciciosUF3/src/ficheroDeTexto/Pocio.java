package ficheroDeTexto;

public class Pocio extends Objecte {
	
	private int hp_curada;
	
	Pocio(String nom, int curar) {
		super(nom);	
		this.hp_curada=curar;
	}
	
	public void utilizar(Mokepon a) {
		if(!a.isDebilitat() && this.getQuantitat()>0) {
			a.setHp_actual(a.getHp_actual()+hp_curada);
			if(a.getHp_actual()>a.getHp_max()) {
				a.setHp_actual(a.getHp_max());
			}
			this.setQuantitat(getQuantitat()-1);
		}
	}
}
