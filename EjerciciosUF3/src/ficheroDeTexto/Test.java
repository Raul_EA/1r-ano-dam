package ficheroDeTexto;

import java.util.Scanner;

public class Test {
	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) throws Exception {
		try {
			Gimnasios.afegirGimnas("De piedra gimnas","Sant Mugat","Gregorin");
			Gimnasios.afegirGimnas("De fuego gimnas","Merdanyola","Isabelita");
			Gimnasios.afegirGimnas("De agua gimnas","Mabadell","Elenita");
			Gimnasios.afegirGimnas("De planta gimnas","Merrassa","Marcos");
			Gimnasios.afegirGimnas("De piedra gimnas","Sant Mugat","Gregorin");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		Gimnasios.copiaSeguretat("text/gimnasios.txt", "text/copia_seguridad.txt");

		Gimnasios.mostrarGimnasos();
		
		Gimnasios.cercaLider("De piedra gimnas");
		
		Gimnasios.invictes(1);
		
		Gimnasios.canviLider("De piedra gimnas", "Josemiguel");
	
		Gimnasios.afegeixEntrenador("De piedra gimnas", "Alberto");
		
		Gimnasios.esborrarGimnas("De fuego gimnas");

		Gimnasios.afegeixEntrenador("De piedra gimnas", "Alberto");
		
		Gimnasios.afegeixEntrenador("De piedra gimnas", "Baltasar");
		
		Gimnasios.afegeixEntrenador("De piedra gimnas", "Rodolfo");
		
		Gimnasios.consultaEntrenadors("De piedra gimnas");
		
	}
}
