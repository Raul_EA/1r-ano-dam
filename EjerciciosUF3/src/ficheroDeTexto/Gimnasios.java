package ficheroDeTexto;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;

public class Gimnasios {

	public static Random rd = new Random();

	public static void afegirGimnas(String nomGimnas, String ciutat, String liderGimnas) throws Exception {
		try {
			copiaSeguretat("text/gimnasios.txt", "text/copia_seguridad.txt");

			File f = new File("text/gimnasios.txt");
			FileWriter fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);

			File f2 = new File("text/copia_seguridad.txt");
			FileReader fr = new FileReader(f2);
			BufferedReader br = new BufferedReader(fr);
			
			if (!br.ready()) {
				bw.append(nomGimnas + ";" + ciutat + ";" + liderGimnas + ";" + 0 + "\n");
				System.out.println("Writer Carregat Correctament");
			} else {
				while (br.ready()) {
					String b = br.readLine();
					if (b.contains(nomGimnas)) {
						br.close();
						bw.close();
						throw new Exception("Error, ya existe el gimnasio");
					}
				}
				bw.append(nomGimnas + ";" + ciutat + ";" + liderGimnas + ";" + 0 + "\n");
				System.out.println("Writer Carregat Correctament");
			}
			
			br.close();
			bw.close();
			copiaSeguretat("text/gimnasios.txt", "text/copia_seguridad.txt");
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void mostrarGimnasos() {
		try {
			File f = new File("text/gimnasios.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			while (br.ready()) {
				System.out.println(br.readLine());
			}

			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void cercaLider(String nomGimnas) {
		try {
			File f = new File("text/gimnasios.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			while (br.ready()) {
				String b = br.readLine();
				if (b.contains(nomGimnas)) {
					String[] a = b.split(";");
					System.out.println(a[2]);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void invictes(int z) {
		try {
			File f = new File("text/gimnasios.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			while (br.ready()) {
				String b = br.readLine();
				String[] c = b.split(";");
				int a = Integer.parseInt(c[3]);
				if (z > a)
					System.out.println(b);
			}

			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void copiaSeguretat(String path1, String path2) {
		try {
			File f = new File(path1);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			File f2 = new File(path2);
			FileWriter fw = new FileWriter(f2, false);
			BufferedWriter bw = new BufferedWriter(fw);

			while (br.ready()) {
				bw.append(br.readLine() + "\n");
			}

			bw.close();
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void canviLider(String nomGimnas, String nouLider) {
		try {
			File f = new File("text/copia_seguridad.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			File f2 = new File("text/gimnasios.txt");
			FileWriter fw = new FileWriter(f2, false);
			BufferedWriter bw = new BufferedWriter(fw);

			while (br.ready()) {
				String b = br.readLine();
				if (b.contains(nomGimnas)) {
					String[] a = b.split(";");
					bw.append(a[0] + ";" + a[1] + ";" + nouLider + ";" + a[3] + "\n");
				} else {
					bw.append(b + "\n");
				}
			}
			bw.close();
			br.close();
			copiaSeguretat("text/gimnasios.txt", "text/copia_seguridad.txt");
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void afegeixEntrenador(String nomGimnas, String nomEntrenador) {
		try {
			File f = new File("text/copia_seguridad.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			File f2 = new File("text/gimnasios.txt");
			FileWriter fw = new FileWriter(f2, false);
			BufferedWriter bw = new BufferedWriter(fw);

			while (br.ready()) {
				String b = br.readLine();
				if (b.contains(nomGimnas)) {
					String[] c = b.split(";");
					int a = Integer.parseInt(c[3]);
					a++;
					String cadena = c[0] + ";" + c[1] + ";" + c[2] + ";" + a;
					for (int i = 0; i < a - 1; i++) {
						cadena += ";" + c[4 + i];
					}
					cadena += ";" + nomEntrenador;
					bw.append(cadena + "\n");
				} else {
					bw.append(b + "\n");
				}
			}
			bw.close();
			br.close();
			copiaSeguretat("text/gimnasios.txt", "text/copia_seguridad.txt");
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void esborrarGimnas(String nomGimnas) {
		try {
			File f = new File("text/copia_seguridad.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			File f2 = new File("text/gimnasios.txt");
			FileWriter fw = new FileWriter(f2, false);
			BufferedWriter bw = new BufferedWriter(fw);

			while (br.ready()) {
				String b = br.readLine();
				if (!b.contains(nomGimnas)) {
					bw.append(b + "\n");
				}
			}
			bw.close();
			br.close();
			copiaSeguretat("text/gimnasios.txt", "text/copia_seguridad.txt");
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void consultaEntrenadors(String nomGimnas) {
		try {
			File f = new File("text/gimnasios.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			while (br.ready()) {
				String b = br.readLine();
				if (b.contains(nomGimnas)) {
					String cadena = "";
					String[] c = b.split(";");
					int a = Integer.parseInt(c[3]);
					for (int i = 0; i < a; i++) {
						cadena += c[4 + i] + "\n";
					}
					System.out.println(cadena);
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}
	public static void borrarArchivo(String path) {
		File f = new File(path);
		f.delete();
	}
	public static void renameArchivo(String path,String path2) {
		File f = new File(path);
		File f2 = new File(path2);
		f.renameTo(f2);
	}
}
