package serialización;

import java.util.Scanner;

public class Test {
	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		/*
		Objecte b = new Reviure("Max revivir");
		Objecte a = new Pocio("SuperPocio",50);
		Objecte c = new Arma("De diamante",50);
		Objecte d = new Armadura("De oro",100);
		
		GuardadoObjetos.afegirObjecte(a);
		GuardadoObjetos.afegirObjecte(b);
		GuardadoObjetos.afegirObjecte(c);
		GuardadoObjetos.afegirObjecte(d);
		
		System.out.println(GuardadoObjetos.recuperarObjecte());
		System.out.println(GuardadoObjetos.recuperarObjectes());
		*/
		MokeponCapturat e = new MokeponCapturat("Gregorin",5);
		MokeponCapturat z = new MokeponCapturat("Jose",6);
		MokeponCapturat x = new MokeponCapturat("Isabelita",7);
		
		GuardadoObjetos.afegeixMokepon(e);
		GuardadoObjetos.afegeixMokepon(z);
		GuardadoObjetos.afegeixMokepon(x);
		
		GuardadoObjetos.teamMocketAtacDeNou();
		
		System.out.println(GuardadoObjetos.recuperarMokepon("Jose", 6, "Team Mocket", "Jose"));
	}
}
