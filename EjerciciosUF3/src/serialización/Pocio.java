package serialización;

public class Pocio extends Objecte {
	
	private int hp_curada;
	
	Pocio(String nom, int curar) {
		super(nom);	
		this.hp_curada=curar;
	}
	
	public void utilizar(Mokepon a) {
		if(!a.isDebilitat() && this.getQuantitat()>0) {
			a.setHp_actual(a.getHp_actual()+hp_curada);
			if(a.getHp_actual()>a.getHp_max()) {
				a.setHp_actual(a.getHp_max());
			}
			this.setQuantitat(getQuantitat()-1);
		}
	}

	@Override
	public String toString() {
		return super.toString()+" Pocio [hp_curada=" + hp_curada + "]";
	}

	public int getHp_curada() {
		return hp_curada;
	}

	public void setHp_curada(int hp_curada) {
		this.hp_curada = hp_curada;
	}
	
	
}
