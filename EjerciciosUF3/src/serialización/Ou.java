package serialización;

import java.util.Random;

public class Ou {
	
	static Random rd = new Random();
	
	private String especie;
	private Tipus tipus;
	private int passesRestants;
	
	Ou(String especie, Tipus tipus){
		this.especie=especie;
		this.tipus=tipus;
		this.passesRestants=rd.nextInt(5,10);
	}
	
	public void caminar() {
		this.passesRestants-=1;
		if(this.passesRestants==0) {
			this.eclosionar();
		}
	}
	public Mokepon eclosionar() {
		return new Mokepon(especie,tipus);
	}
}
