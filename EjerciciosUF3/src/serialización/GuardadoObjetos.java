package serialización;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.ArrayList;

public class GuardadoObjetos {

	public static void afegirObjecte(Objecte obj) {
		try {
			File f = new File("serialización/objectes.dat");
			FileOutputStream fos = new FileOutputStream(f, true);
			AppendableObjectOutputStream aoos = new AppendableObjectOutputStream(fos, true);
			aoos.writeObject(obj);
			aoos.flush();
			aoos.close();
			fos.close();
			System.out.println("Todo gucci");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Object recuperarObjecte() {
		try {
			File f = new File("serialización/objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);

			Object a = ois.readObject();
			ois.close();
			if (a instanceof Pocio)
				return ((Pocio) a);
			else if (a instanceof Reviure)
				return ((Reviure) a);
			else if (a instanceof Arma)
				return ((Arma) a);
			else if (a instanceof Armadura)
				return ((Armadura) a);
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		return null;
	}

	public static List<Object> recuperarObjectes() {
		List<Object> b = new ArrayList<Object>();
		try {
			File f = new File("serialización/objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object a = ois.readObject();
				if (a instanceof Pocio)
					b.add((Pocio) a);
				else if (a instanceof Reviure)
					b.add((Reviure) a);
				else if (a instanceof Arma)
					b.add((Arma) a);
				else if (a instanceof Armadura)
					b.add((Armadura) a);
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		return b;
	}

	public static Pocio recuperaPocioConcreta(int n) {
		try {
			File f = new File("serialización/objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object a = ois.readObject();
				if (a instanceof Pocio)
					if (((Pocio) a).getHp_curada() == n)
						return (Pocio) a;
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			return null;
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		return null;
	}

	public static void afegeixMokepon(MokeponCapturat mok) {
		try {
			if (!estaMokepon(mok)) {
				File r = new File("serialización/objectes.dat");
				FileOutputStream fos = new FileOutputStream(r, true);
				AppendableObjectOutputStream aoos = new AppendableObjectOutputStream(fos, true);
				aoos.writeObject(mok);
				aoos.close();
				fos.close();
			} else {
				System.out.println("Ya existe el mokepon");
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("error");
			e.printStackTrace();
		}
	}

	public static boolean estaMokepon(MokeponCapturat mok) {
		try {
			File r = new File("serialización/objectes.dat");
			FileInputStream fis = new FileInputStream(r);
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object a = ois.readObject();
				if (a instanceof MokeponCapturat)
					if (mok.equals((MokeponCapturat) a)) {
						ois.close();
						return true;
					}
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		return false;
	}

	public static void copiar(File r, File w) {
		FileInputStream fis;
		ObjectInputStream ois = null;

		FileOutputStream fos;
		AppendableObjectOutputStream aoos = null;

		try {
			fis = new FileInputStream(r);
			ois = new ObjectInputStream(fis);

			fos = new FileOutputStream(w, false);
			aoos = new AppendableObjectOutputStream(fos, true);

			while (true) {
				Object a = ois.readObject();
				aoos.writeObject(a);
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		} finally {
			try {
				aoos.close();
				ois.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			w.delete();
		}
	}

	public static MokeponCapturat recuperarMokepon(String nom, int nivell, String entrenador, String nomDonat) {
		FileInputStream fis;
		ObjectInputStream ois = null;

		FileOutputStream fos;
		AppendableObjectOutputStream aoos = null;
		try {
			File r = new File("serialización/objectes.dat");
			fis = new FileInputStream(r);
			ois = new ObjectInputStream(fis);

			File w = new File("serialización/kk.dat");
			fos = new FileOutputStream(w);
			aoos = new AppendableObjectOutputStream(fos, true);

			boolean flag = true;
			while (true) {
				Object a = ois.readObject();
				if (a instanceof MokeponCapturat) {
					MokeponCapturat b = ((MokeponCapturat) a);
					if (!(b.getNom().equals(nom) && b.getNivell() == nivell && b.getNomEntrenador().equals(entrenador)
							&& b.getNomPosat().equals(nomDonat))) {
						aoos.writeObject(b);
					} else {
						if (flag) {
							flag = false;
						} else {
							aoos.writeObject(b);
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			try {
				aoos.close();
				File r = new File("serialización/objectes.dat");
				fis = new FileInputStream(r);
				ois = new ObjectInputStream(fis);

				File w = new File("serialización/kk.dat");
				while (true) {
					Object a = ois.readObject();
					if (a instanceof MokeponCapturat) {
						MokeponCapturat b = ((MokeponCapturat) a);
						if ((b.getNom().equals(nom) && b.getNivell() == nivell
								&& b.getNomEntrenador().equals(entrenador) && b.getNomPosat().equals(nomDonat))) {
							ois.close();
							fis.close();
							copiar(w, r);
							w.delete();
							return b;
						}
					}
				}
			} catch (FileNotFoundException x) {
				System.out.println("no existeix el fitxer");
				e.printStackTrace();
			} catch (IOException x) {
			} catch (ClassNotFoundException x) {
				System.out.println("no s'ha trobat la classe demanada");
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		return null;
	}
	public static void teamMocketAtacDeNou() {
		FileInputStream fis=null;
		ObjectInputStream ois = null;

		FileOutputStream fos=null;
		AppendableObjectOutputStream aoos = null;
		try {
			File r = new File("serialización/objectes.dat");
			fis = new FileInputStream(r);
			ois = new ObjectInputStream(fis);

			File w = new File("serialización/kk.dat");
			fos = new FileOutputStream(w);
			aoos = new AppendableObjectOutputStream(fos, true);
			
			while (true) {
				Object a = ois.readObject();
				if(a instanceof MokeponCapturat) {
					MokeponCapturat b = (MokeponCapturat)a;
					b.setNomEntrenador("Team Mocket");
					aoos.writeObject(b);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			try {
				aoos.close();
				File r = new File("serialización/objectes.dat");
				File w = new File("serialización/kk.dat");
				ois.close();
				fis.close();
				copiar(w, r);
				w.delete();
			} catch (FileNotFoundException x) {
				System.out.println("no existeix el fitxer");
				e.printStackTrace();
			} catch (IOException x) {
			}
		}catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
	}
}
