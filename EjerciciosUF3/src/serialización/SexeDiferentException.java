package serialización;

public class SexeDiferentException extends Exception{
	
	public SexeDiferentException(String message) {
		super(message);
	}

}
