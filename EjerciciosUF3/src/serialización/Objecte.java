package serialización;

import java.io.Serializable;

public abstract class Objecte implements Serializable{
	
	private String nom;
	private int quantitat;
	
	Objecte (String nom){
		this.nom=nom;
		this.quantitat=1;
	}
	
	public abstract void utilizar(Mokepon a);
	
	public void obtenir(int numObjectes) {
		quantitat+=numObjectes;
	}
	
	public void donar(MokeponCapturat a) {
		a.setObjecte(this);
	}
	
	@Override
	public String toString() {
		return "Objecte [nom=" + nom + ", quantitat=" + quantitat + "]";
	}

	public String getNom() {
		return nom;
	}
	
	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
	
}
