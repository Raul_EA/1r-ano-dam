package serialización;

public class TipusDiferentException extends Exception{
	
	public TipusDiferentException(String message) {
		super(message);
	}

}
