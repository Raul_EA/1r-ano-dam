package hp_Inicial;

import java.io.*;
import java.util.*;

import serialización.AppendableObjectOutputStream;

public class Granja implements Serializable{
	private static final long serialVersionUID = 1L;
	private Animal [] animals;
	private int numAnimals;
	private int topAnimals = 100;
	
	public Granja() {
		animals = new Animal[topAnimals];
		numAnimals = 0;
	}
	
	public Granja (int top) {
		if (top >= 1 && top <= 100) {
			topAnimals = top;
		}
		animals = new Animal[topAnimals];
		numAnimals = 0;
	}

	public Animal[] getanimals() {
		return animals;
	}
	
	public int getNumAnimalos() {
		return numAnimals;
	}

	
	public int getTopAnimals() {
		return topAnimals;
	}

	public int afegir(Animal a) {
		if (numAnimals == topAnimals) 
			return -1;
		else {
			animals[numAnimals] = a;
			return numAnimals++;
		}
	}

	@Override
	public String toString() {
		String res;
		
		res = "\nanimals de la granja\n\n";
		for (int i = 0; i < numAnimals; i++)
			res = res + "\n" +   animals[i].toString();
		return res;
	}
	
	public void visualitzar() {
		System.out.println(this.toString());
	}
	
	public void visualitzarVius() {
		for (int i = 0; i < numAnimals; i++) {
			if (animals[i].getEstat() == Estat.VIU) {
				animals[i].visualitzar();
			}
		}
	}
	
	
	public Animal obtenirAnimal(int pos) {
		if (pos >= this.getTopAnimals()) return null;
		else
			return this.animals[pos];
	}
	
	public int toFile(String cadena)
	{
		int num  = 0;
		
		File w = new File("hp files/"+cadena+".dat");
		FileOutputStream fos =null;
		AppendableObjectOutputStream aoos=null;
		try {
			fos = new FileOutputStream(w, false);
			aoos = new AppendableObjectOutputStream(fos, true);
			
			for(int i=0;i<this.topAnimals;i++) {
				if(this.getanimals()[i].estat==Estat.VIU) {
					aoos.writeObject(this.getanimals()[i]);
					num++;
				}
			}
			aoos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		}
		return num;
	}
	
	public int fromFile(String cadena)
	{
		
		File r = new File("hp files/"+cadena+".dat");
		FileInputStream fis =null;
		ObjectInputStream ois=null;
		
		List<Animal> animals = new ArrayList<Animal>();
		
		try {
			fis=new FileInputStream(r);
			ois=new ObjectInputStream(fis);
			
			while(true) {
				Object a = ois.readObject();
				if(a instanceof Animal)
					if(((Animal)a).estat==Estat.VIU)
						animals.add((Animal) a);
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			this.animals=null;
			this.topAnimals=animals.size();
			this.animals= new Animal[topAnimals];
			for(int i=0;i<topAnimals;i++) {
				this.animals[i]=animals.get(i);
			}
			this.numAnimals=animals.size();
		} catch (ClassNotFoundException e) {
			System.out.println();
		}
		return animals.size();
	}
	
	public Jardi toJardi() {
	 	Jardi j = new Jardi();
		Animal[][] a = new Animal[10][10];
		for (int i = 0; i < 10; i++) {
			for (int h = 0; h < 10; h++) {
				a[i][h]=null;
			}
		}
		for(int i=0;i<animals.length;i++){
			if(animals[i].estat==Estat.VIU)
				a[animals[i].coordX][animals[i].coordY] = animals[i];
		}
		j.setTauler(a);
		return j;
	}
}
