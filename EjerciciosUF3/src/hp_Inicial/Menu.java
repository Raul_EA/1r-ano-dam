package hp_Inicial;

import java.io.*;
import java.util.*;

public class Menu {
	static final int TG = 20;
	
	private int opt;
	
	public static ScannerArreglat sc= new ScannerArreglat();
	public static Random rd= new Random();
	
	public void menu() {
		String cad="Menu\n1-Generar granja\n2-Visualizar granja\n3-Gravar granja\n4-Leer granje\n5-Granja a jardin\n6-Ver jardin\n7-Aplicar movimientos\n8-Ver fichero log\n9-Jardin a Granja\n0-Salir";
		System.out.println(cad);
	}
	
	public void fun(){
		boolean flag=true;
		Granja g = null;
		Jardi j = null;
		while(flag) {
			menu();
			opt = sc.nextInt();
			switch(opt) {
			case 1:
				g = new Granja(TG);
				for(int i=0;i<TG;i++) {
					if(i<=TG/2) {
						g.afegir(new Gos("g"+i));
					}else {
						g.afegir(new Ganso("s"+i));
					}
				}
				System.out.println("GRANJA CREADA");
				break;
			case 2:
				if(g!=null)
					System.out.println(g.toString());
				else
					System.err.println("GRANJA VACIA");
				break;
			case 3:
				if(g!=null) {
					System.out.println("INTRODUZCA EL NOMBRE DEL ARCHIVO");
					g.toFile(sc.nextLine());
				}else {
					System.err.println("Granja no instanciada");
				}
				break;
			case 4:
				g = new Granja();
				System.out.println("INTRODUZCA EL NOMBRE DEL ARCHIVO");
				g.fromFile(sc.nextLine());
				break;
			case 5:
				if(g!=null) {
					j=g.toJardi();
					System.out.println("TODOS AL JARDIN");
				}else
					System.err.println("GRANJA VACIA");
				break;
			case 6:
				if(j!=null) {
					j.visualitzar();
				}else
					System.err.println("JARDIN VACIO");
				break;
			case 7:
				if(j!=null) {
					System.out.println("INTRODUCE EL NOMBRE DEL ARCHIVO");
					j.movimientos(sc.nextLine());
				}else
					System.err.println("JARDIN VACIO");
				break;
			case 8:
				System.out.println("LOG:");
				log();
				break;
			case 9:
				if(j!=null) {
					g=j.toGranja();
					System.out.println("TODOS A LA GRANJA");
				}else
					System.err.println("JARDIN VACIO");
				break;
			default:
				System.out.println("SALISTEEEEEE");
				flag=false;
			}
		}
	}
	public void log() {
		try {
			File f1 = new File("hp files/log.csv");
			FileReader fr = null;
			BufferedReader br= null;
			fr = new FileReader(f1);
			br = new BufferedReader(fr);
			
			while(br.ready()) {
				System.out.println(br.readLine());
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}
}
