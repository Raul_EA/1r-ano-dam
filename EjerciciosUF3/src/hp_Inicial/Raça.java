package hp_Inicial;

import java.io.Serializable;

public class Raça implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nomRaça;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Raça (String nom, GosMida gt, int t, boolean domina) {
		nomRaça = nom;
		mida = gt;
		tempsVida = t;
		dominant = domina;
	}
	
	public Raça (String nom, GosMida gm) {
		this (nom, gm, 10, false);
	}

	public Raça() {
		// TODO Auto-generated constructor stub
	}

	public String getNomRaça() {
		return nomRaça;
	}

	public void setNomRaça(String nomRaça) {
		this.nomRaça = nomRaça;
	}

	public GosMida getMida() {
		return mida;
	}

	public void setMida(GosMida mida) {
		this.mida = mida;
	}

	public int getTempsVida() {
		return tempsVida;
	}

	public void setTempsVida(int tempsVida) {
		if (tempsVida >= 0 && tempsVida <= 10)
			this.tempsVida = tempsVida;
	}

	public boolean isDominant() {
		return dominant;
	}

	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}

	public String toString() {
		return "Raça [nomRaça=" + nomRaça + ", mida=" + mida + ", tempsVida=" + tempsVida + ", dominant=" + dominant
				+ "]";
	}
	
	
}
