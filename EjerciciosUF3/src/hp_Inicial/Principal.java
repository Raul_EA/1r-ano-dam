package hp_Inicial;

import java.util.Scanner;

/**
 * M03UF3. Pr�ctica Fitxers
 **/

public class Principal {
	static Scanner input;

	public static void main(String[] args) {
	//	int valor = 10;
		input = new Scanner(System.in);

		int opcio;

		do {
			opcio = menu();
			switch (opcio) {
			case 1: // crear Granja
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			case 0:
				break;
			}
		} while (opcio != 0);
		System.out.println("A reveure !!!");
	}

	static int menu() {
		int valor = -1;

		do {
			System.out.println("M03UF3.Pr�ctica Fitxers. HP. Episodi IV");
			System.out.println("---------------------------------------\n");
			System.out.println(" 1.- Generar Granja");
			System.out.println(" 2.- Mostrar Granja");
			System.out.println(" 3.- Gravar Granja");
			System.out.println(" 4.- Llegir Graja");
			System.out.println(" 5.- Granja a Jard�");
			System.out.println(" 6.- Mostrar Jard�");
			System.out.println(" 7.- Passar Moviments");
			System.out.println(" 8.- Veure Fitxer Log");
			System.out.println(" 9.- Jard� a Granja");
			System.out.println("10.- Nop");
			System.out.println(" 0.- Sortir");

			System.out.print("\n\nTria la teva opci�: ");

			try {
				valor = Integer.parseInt(input.nextLine());
			} catch (Exception e) {
				System.out.println("Si us plau, valors num�rics");
			}
		} while (valor == -1);
		return (valor);
	}

	static void omplirGranja(Granja laMevaGranja, int ocupacio) {
		int limit = laMevaGranja.getTopAnimals();
		int qAnimal;
		int qRaça;
		Raça RaçaGos;
		int qedat;
		int resposta;
		int qSexe;

		Raça r1 = new Raça("pastorAlemany", GosMida.GRAN, 12, true);
		Raça r2 = new Raça("yorshie", GosMida.PETIT, 16, false);
		Raça r3 = new Raça("corgi", GosMida.PETIT, 15, false);
		Raça r4 = new Raça("boxer", GosMida.MITJA, 13, true);

		// omplir granja de gos aleatoris
		for (int i = 0; i < ocupacio; i++) {
			Gos g = new Gos();
			// assignaci� aleat�ria de Raça
			qRaça = (int) (Math.random() * 5);
			switch (qRaça) {
			case 1:
				RaçaGos = r1;
				break;
			case 2:
				RaçaGos = r2;
				break;
			case 3:
				RaçaGos = r3;
				break;
			case 4:
				RaçaGos = r4;
				break;
			default:
				RaçaGos = null;
			}
			g.setRaça(RaçaGos);
			g.setNom("G" + i);
			resposta = laMevaGranja.afegir(g);

			// assignaci� aleat�ria d'edat
			qedat = (int) (Math.random() * 10);
			laMevaGranja.obtenirAnimal(resposta).setEdat(qedat);

			// assignaci� del sexe
			qSexe = (int) (Math.random() * 2);
			if (qSexe == 0)
				laMevaGranja.obtenirAnimal(resposta).setSexe(Sexe.M);
			else
				laMevaGranja.obtenirAnimal(resposta).setSexe(Sexe.F);

			Ganso s = new Ganso();
			// assignaci� aleat�ria de tipus
			resposta = (int) (Math.random() * 3);
			switch (resposta) {
			case 0:
				s.setTipus(GansoTipus.AGRESSIU);
				break;
			case 1:
				s.setTipus(GansoTipus.DOMESTIC);
				break;
			case 2:
				s.setTipus(GansoTipus.DESCONEGUT);
				break;
			}
			s.setNom("S" + i);
			resposta = laMevaGranja.afegir(s);

			// assignaci� aleat�ria d'edat
			qedat = (int) (Math.random() * 10);
			laMevaGranja.obtenirAnimal(resposta).setEdat(qedat);

			// assignaci� aleat�ria d'edat
			qedat = (int) (Math.random() * 10);
			laMevaGranja.obtenirAnimal(resposta).setEdat(qedat);

			// assignaci� del sexe
			qSexe = (int) (Math.random() * 2);
			if (qSexe == 0)
				laMevaGranja.obtenirAnimal(resposta).setSexe(Sexe.M);
			else
				laMevaGranja.obtenirAnimal(resposta).setSexe(Sexe.F);
		}
	}	
}
