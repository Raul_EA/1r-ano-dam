package hp_Inicial;

import java.util.*;
import java.io.*;

public class Jardi {
	private Animal tauler[][];
	int topX;
	int topY;
	
	Jardi(int cX,int cY){
		if (cX<=0) cX = 10;
		if (cY<=0) cY = 10;
		topX = cX;
		topY = cY;
		tauler = new Animal[cX][cY];
		
		for (int i=0;i<cX;i++)
			for (int j=0;j<cY;j++)
				tauler[i][j]=null;
	}
	
	Jardi(){
		this(10,10);
	}
	
	public int getTopX()
	{
		return topX;
	}
	
	public int getTopY()
	{
		return topY;
	}
	
	public void setTauler(Animal[][] tauler) {
		this.tauler = tauler;
	}
	
	public Granja toGranja() {
		Granja g = new Granja();
		List<Animal> a = new ArrayList<Animal>();
		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[0].length; j++) {
				if(tauler[i][j]!=null)
					a.add(tauler[i][j]);
			}
		}
		for(Animal x : a) {
			g.afegir(x);
		}
		return g;
	}
	
	public void visualitzar() {
		/*
		Gregorio, como pienso que es irrelevante 
		de que forma haga el visualizar del jardin
		lo voy ha hacer como me parezca
		*/
		String cadena = "";
		for(int i =0;i<tauler.length;i++) {
			for(int j =0;j<tauler[0].length;j++) {
				if(tauler[i][j]!=null)
					cadena+=tauler[i][j].nom+" | ";
				else 
					cadena+="null | ";
			}
			cadena+="\n";
		}
		System.out.println(cadena);
	}
	
	public void movimientos(String cadena) {
		File f = new File("hp files/"+cadena+".csv");
		FileReader fr = null;
		BufferedReader br = null;
		File f1 = new File("hp files/log.csv");
		FileWriter fw = null;
		BufferedWriter bw= null;
		boolean flag=true;
		int num=0;
		try{
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			
			fw = new FileWriter(f1,true);
			bw = new BufferedWriter(fw);
			while(br.ready()) {
				if(num>=10)
					visualitzar();
				num++;
				String l = br.readLine();
				String p[] = l.split(";");
				if(p.length==2) {
					for (int i = 0; i < tauler.length; i++) {
						for (int j = 0; j < tauler[0].length; j++) {
							if(tauler[i][j]!=null && tauler[i][j].nom.equals(p[0]) && flag) {
								flag=false;
								switch (p[1]) {
								case "D":
									if(tauler[i][j].coordY+1>=0 && tauler[i][j].coordY+1<=9) {
										tauler[i][j].coordY++;
										if(tauler[i][j+1]==null) {
											Animal a = tauler[i][j];
											a.movement();
											if(a.estat!=Estat.MORT) { 
												tauler[i][j+1]=a;
												bw.append("MOVIMENT NET\n");
											}
											tauler[i][j]=null;
										}else {
											tauler[i][j].trobades(tauler[i][j+1]);
											if(tauler[i][j+1].estat==Estat.MORT) {
												Animal a = tauler[i][j];
												a.movement();
												if(a.estat!=Estat.MORT) 
													tauler[i][j+1]=a;
												else
													tauler[i][j+1]=null;
												tauler[i][j]=null;
											}else {
												tauler[i][j]=null;
											}
												
										}
									}else
										bw.append("MOVIMENT INCORRECTE\n");
									break;
								case "E":
									if(tauler[i][j].coordY-1>=0 && tauler[i][j].coordY-1<=9) {
										tauler[i][j].coordY--;
										if(tauler[i][j-1]==null) {
											Animal a = tauler[i][j];
											bw.append("ENERGIA A "+a.energia+"\n");
											a.energia+=10;
											a.movement();
											if(a.estat!=Estat.MORT) {
												tauler[i][j-1]=a;
												bw.append("MOVIMENT NET\n");
											}
											tauler[i][j]=null;
										}else {
											tauler[i][j].trobades(tauler[i][j-1]);
											if(tauler[i][j-1].estat==Estat.MORT) {
												Animal a = tauler[i][j];
												a.energia+=10;
												a.movement();
												if(a.estat!=Estat.MORT) 
													tauler[i][j-1]=a;
												else
													tauler[i][j-1]=null;
												tauler[i][j]=null;
											}else {
												tauler[i][j]=null;
											}
												
										}
									}else
										bw.append("MOVIMENT INCORRECTE\n");
									break;
								case "A":
									if(tauler[i][j].coordX-1>=0 && tauler[i][j].coordX-1<=9) {
										tauler[i][j].coordX--;
										if(tauler[i-1][j]==null) {
											Animal a = tauler[i][j];
											a.movement();
											if(a.estat!=Estat.MORT){
												tauler[i-1][j]=a;
												bw.append("MOVIMENT NET\n");
											}
											tauler[i][j]=null;
										}else {
											tauler[i][j].trobades(tauler[i-1][j]);
											if(tauler[i-1][j].estat==Estat.MORT) {
												Animal a = tauler[i][j];
												a.movement();
												if(a.estat!=Estat.MORT) 
													tauler[i-1][j]=a;
												else
													tauler[i-1][j]=null;
												tauler[i][j]=null;
											}else {
												tauler[i][j]=null;
											}
												
										}
									}else
										bw.append("MOVIMENT INCORRECTE\n");
									break;
								case "B":
									if(tauler[i][j].coordX+1>=0 && tauler[i][j].coordX+1<=9) {
										tauler[i][j].coordX++;
										if(tauler[i+1][j]==null) {
											Animal a = tauler[i][j];
											a.movement();
											if(a.estat!=Estat.MORT) { 
												bw.append("MOVIMENT NET\n");
												tauler[i+1][j]=a;
											}
											tauler[i][j]=null;
										}else {
											tauler[i][j].trobades(tauler[i+1][j]);
											if(tauler[i+1][j].estat==Estat.MORT) {
												Animal a = tauler[i][j];
												a.movement();
												if(a.estat!=Estat.MORT) 
													tauler[i+1][j]=a;
												else
													tauler[i+1][j]=null;
												tauler[i][j]=null;
											}else {
												tauler[i][j]=null;
											}
												
										}
									}else
										bw.append("MOVIMENT INCORRECTE\n");
									break;
								case "X":
									tauler[i][j].energia+=10;
									break;
								default:
									System.err.println("INDICACION NO ESPECIFICADA");
								}
							}
						}
					}
					if(flag)
						bw.append("ANIMAL NO TROBAT\n");
					flag=true;
				}else
					bw.append("FORMAT INCORRECTE\n");
			}
			br.close();
			bw.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
		
	}
}

