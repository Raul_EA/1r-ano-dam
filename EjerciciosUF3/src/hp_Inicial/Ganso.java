package hp_Inicial;

import java.io.Serializable;

public class Ganso extends Animal implements Serializable {

	private static final long serialVersionUID = 1L;

	private static int edatTope = 6;
	
	private GansoTipus tipus;
	
	public Ganso() {
		super();
		edatTope = 6;
		tipus = GansoTipus.DESCONEGUT;
	}

	public Ganso(String nom, int edat, GansoTipus tipus, Sexe sexe) {
		super(nom, edat, sexe);
		this.tipus = tipus;
	}

	public Ganso(String n) {
		super(n);
		tipus = GansoTipus.DESCONEGUT;
	}

	public Ganso(Ganso g) {
		this();
		clonar(g);
	}

	public void setTipus(GansoTipus tipus) {
		this.tipus = tipus;
	}

	public GansoTipus getTipus() {
		return tipus;
	}

	public void clonar(Ganso g) {
		super.clonar(g);
	}

	public int getEdatTope() {
		return Ganso.edatTope;
	}
	
	@Override
	public void visualitzar() {
		System.out.println(this.toString());
	}

	public String toString() {
		String cadena = "GANSO ";
		
		cadena += super.toString();
		cadena += " " + this.getTipus();

		return (cadena);
	}


	@Override
	public void incEdat() {
		if (getEstat() == Estat.VIU) {
			edat++;
			actualitzarEstat();
		}
	}

	private void actualitzarEstat() {

		switch (this.getTipus()) {
			case AGRESSIU:
				if (edat >= edatTope - 1)
					estat = Estat.MORT;
				break;
				
			case DOMESTIC:
				if (edat >= edatTope + 1)
					estat = Estat.MORT;
				break;
				
			case DESCONEGUT:
				if (edat >= edatTope)
					estat = Estat.MORT;
				break;
		}
	}

}
