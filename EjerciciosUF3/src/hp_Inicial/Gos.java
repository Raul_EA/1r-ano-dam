package hp_Inicial;

import java.io.Serializable;

public class Gos extends Animal implements Serializable{
	private static final long serialVersionUID = 1L;
	private Raça Raça;

	public Gos() {
		super();
		Raça = null;
	}

	public Gos(String nom, int edat, Raça Raça, Sexe sexe) {
		super(nom, edat, sexe);
		this.Raça = Raça;
	}

	public Gos(String n) {
		super(n);
		Raça = null;
	}

	public Gos(Gos g) {
		this();
		clonar(g);
	}

	public void setRaça(Raça Raça) {
		this.Raça = Raça;
	}

	public Raça getRaça() {
		return Raça;
	}

	public void clonar(Gos g) {
		// TODO Auto-generated method stub
		super.clonar(g);
		Raça = g.getRaça();
	}

	@Override
	public void visualitzar() {
		// TODO Auto-generated method stub
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String cadena="";

		cadena = "GOS " + super.toString();

		if (Raça != null)
			cadena += " Raça: " + Raça.getNomRaça();
		else
			cadena += " Raça desconeguda";
		return (cadena);
	}
	
	@Override
	public void incEdat() {
		if (getEstat()==Estat.VIU) {
			edat++;
			actualitzarEstat();
		}
	}
	
	private void actualitzarEstat() {
		if (getRaça()!=null) {
			if (edat >= getRaça().getTempsVida())
				estat = Estat.MORT;
		}
		else //si no te Raça ens el "carreguem" quan passa de 10 anys
			if (edat >10)
				estat = Estat.MORT;
	}
}
