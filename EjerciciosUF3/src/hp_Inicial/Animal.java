package hp_Inicial;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

public abstract class Animal implements Serializable {

	private static final long serialVersionUID = 1L;
	protected int edat;
	protected String nom;
	protected int fills;
	protected Sexe sexe;
	protected Estat estat;

	//nous atributs per M03UF3
	protected int coordX;
	protected int coordY;
	protected int energia;
	protected int forzaAtac;
	protected int forzaDefensa;
	
	private static int comptador = 0;
	private static int comptadorX = 0;
	private static int comptadorY = 0;
	
	//Constructors
	Animal () {
		nom = "Sense nom";
		edat = 0;
		comptador++;
		estat = Estat.VIU;
		fills = 0;
		// afegiu el corresponent als nous atributs
		energia = 10;
		forzaAtac = (int)(Math.random()*11);
		forzaDefensa = (int)(Math.random()*11);
		coordX = comptadorX;
		coordY = comptadorY;
		comptadorY++;
		if(comptadorY == 10)
		{
			comptadorY = 0;
			comptadorX++;
		}
	}

	//adapteu als nous atributs !!!
	Animal(String nom, int edat, Sexe sexe) {
		this();
		this.nom = nom;
		this.edat = edat;
		this.sexe = sexe;
	}

	Animal(String n) {
		this();
		nom = n;
	}

	Animal(Animal a) {
		this();
		clonar(a);
	}

	
	//M�tedes setters i getters
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		if (edat >= 0)
			this.edat = edat;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getFills() {
		return fills;
	}
	public void setFills(int fills) {
		if (fills >= 0)
			this.fills = fills;
	}
	public Sexe getSexe() {
		return sexe;
	}
	public void setSexe(Sexe sexe) {
		if (sexe == Sexe.F || sexe == Sexe.M)
			this.sexe = sexe;
	}
	public Estat getEstat() {
		return estat;
	}
	public void setEstat(Estat estat) {
		if (estat == Estat.MORT || estat == Estat.VIU)
			this.estat = estat;
	}
		
	public int getCoordX()
	{
		return coordX;
	}
	public int getCoordY()
	{
		return coordY;
	}
	public int getEnergia()
	{
		return energia;
	}
	public int getForzaAtac()
	{
		return forzaAtac;
	}
	public int getForzaDefensa()
	{
		return forzaDefensa;
	}
	
	public void setCoordX(int n)
	{
		coordX = n;
	}
	public void setCoordY(int n)
	{
		coordY = n;
	}
	public void setEnergia(int n)
	{
		energia = n;
	}
	public void setForzaAtac(int n)
	{
		forzaAtac = n;
	}
	public void setForzaDefensa(int n)
	{
		forzaDefensa = n;
	}
	
	public void incFills() {
		fills ++;
	}

	public void incEdat() {
		if (getEstat() == Estat.VIU) 
			edat++;
	}

	//adapteu als nous atributs
	public void clonar(Animal a) {
		nom = a.getNom();
		edat = a.getEdat();
		sexe = a.getSexe();
		fills = a.getFills();
		estat = a.getEstat();
	}

	public void visualitzar() {
		System.out.println(this.toString());
	}

	//adapteu als nous atributs
	public String toString() {
		String cadena="";
		
		if (getEstat() == Estat.MORT)
			cadena = "+ ";
		
		cadena = cadena + "Nom: " + nom + " Edat: " + edat + " Sexe: " + sexe + " Energia: " + energia + " CoordX: " + coordX + " CoordY: " + coordY;
		return (cadena);
	}

	public static int quantsAnimals() {
		return comptador;
	}

	public void movement() {
		this.energia--;
		if(this.energia<=0) {
			this.estat=Estat.MORT;
			try {
				File f1 = new File("hp files/log.csv");
				FileWriter fw = null;
				BufferedWriter bw= null;
				fw = new FileWriter(f1,true);
				bw = new BufferedWriter(fw);
				
				bw.append("SENSE ENERGIA\n");
				bw.close();
			} catch (FileNotFoundException e) {
				System.out.println("El fitxer no existeix");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Excepció general d'escriptura");
				e.printStackTrace();
			}
		}
	}
	public void trobades(Animal a){
		try {
			File f1 = new File("hp files/log.csv");
			FileWriter fw = null;
			BufferedWriter bw= null;
			fw = new FileWriter(f1,true);
			bw = new BufferedWriter(fw);
			
			if(this.getClass()!=a.getClass()) {
				if(this.forzaAtac+this.energia>=a.forzaDefensa) {
					a.estat=Estat.MORT;
					bw.append("MOR ANIMAL "+a.nom+"\n");
				}else {
					this.estat=Estat.MORT;
					bw.append("MOR ANIMAL "+this.nom+"\n");
				}
			}else {
				a.estat=Estat.MORT;
				bw.append("MOR ANIMAL "+a.nom+"\n");
				this.energia=this.energia+a.energia;
				this.forzaDefensa=this.forzaDefensa+this.forzaDefensa;
			}
			
			bw.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}	
	}

}
