package provaM03UF3;

import java.io.*;
import java.util.*;

public class Examen {
	
	public static void main(String[] args) {
		
		boolean trobat = buscaGimnas(2, "gymP1.csv");
		if (trobat) System.out.println("Hi ha gimnasos amb menys entrenadors");
		else System.out.println("Tots els gimnasos tenen m�s entrenadors");
		
		ordenarFicheros("gymP1.csv");		
		ordenarFicheros("gymP2.csv");
		gimnasosIguals("gymP1.csv","gymP2.csv");
		gimnasosDiferents("gymP1.csv","gymP2.csv");
		mostrarObjectes("objectes.dat");
		ArrayList<mokepon.Objecte> objectes = obtenirObjectes("objectes.dat", "Armadura");
		mostrarArrayList(objectes);  //rep un ArrayList d'objectes i mostra els objectes per pantalla. JA EST� FET
		actualizaFicheroObjectes("objectes.dat", "objectes.csv");	
		}
	
	private static void mostrarArrayList(ArrayList<mokepon.Objecte> objectes) {
		
		System.out.println("******  Llistat d'objectes del mateix tipus  ********");
		for (int i = 0; i < objectes.size(); i++)
			System.out.println(objectes.get(i));
		System.out.println("***** FI DEL LLISTAT *****");
	}
	
	public static boolean buscaGimnas(int num,String nomF) {
		File f = new File(nomF);
		FileReader fr = null;
		BufferedReader br= null;
		
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			
			while(br.ready()) {
				String b = br.readLine();
				String[] a = b.split(";");
				int c = Integer.parseInt(a[3]);
				if(c<num)
					return true;
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
		
		return false;
	}
	public static void ordenarFicheros(String nomF) {
		File f = new File(nomF);
		FileReader fr = null;
		BufferedReader br= null;
		List<String> a = new ArrayList<String>();
		File f1 = new File("O"+nomF);
		FileWriter fw = null;
		BufferedWriter bw= null;
		
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			
			fw = new FileWriter(f1,false);
			bw = new BufferedWriter(fw);
			
			while(br.ready()) {
				a.add(br.readLine());
			}
			br.close();
			
			a.sort(null);
			
			for(String s : a) {
				bw.append(s+"\n");
			}
			bw.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}
	public static void gimnasosIguals(String nomF1,String nomF2) {
		ordenarFicheros(nomF1);		
		ordenarFicheros(nomF2);
		
		File f = new File("O"+nomF1);
		FileReader fr = null;
		BufferedReader br= null;
		
		File f2 = new File("O"+nomF2);
		FileReader fr2 = null;
		BufferedReader br2= null;
		
		File f1 = new File("iguals.csv");
		FileWriter fw = null;
		BufferedWriter bw= null;
		
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			
			fw = new FileWriter(f1,false);
			bw = new BufferedWriter(fw);
			
			while(br.ready()) {
				String a = br.readLine();
				fr2 = new FileReader(f2);
				br2 = new BufferedReader(fr2);
				while(br2.ready()) {
					String b = br2.readLine();
					if(a.equals(b) && a!=null && b!=null)
						bw.append(a+"\n");
				}
			}
			
			br.close();
			br2.close();
			bw.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}
	public static void gimnasosDiferents(String nomF1,String nomF2) {
		ordenarFicheros(nomF1);		
		ordenarFicheros(nomF2);
		
		File f = new File("O"+nomF1);
		FileReader fr = null;
		BufferedReader br= null;
		
		File f2 = new File("O"+nomF2);
		FileReader fr2 = null;
		BufferedReader br2= null;
		
		File f1 = new File("diferents.csv");
		FileWriter fw = null;
		BufferedWriter bw= null;
		
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			
			fw = new FileWriter(f1,false);
			bw = new BufferedWriter(fw);
			
			boolean flag = true;
			
			while(br.ready()) {
				flag=true;
				String a = br.readLine();
				fr2 = new FileReader(f2);
				br2 = new BufferedReader(fr2);
				while(br2.ready()) {
					String b = br2.readLine();
					if(a.equals(b) && a!=null && b!=null)
						flag=false;
				}
				if(flag)
					bw.append(a+"\n");
			}
			
			br.close();
			br2.close();
			bw.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}
	public static void mostrarObjectes(String nomF) {
		File f = new File(nomF);
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		String cadena="";
		try {
			fis = new FileInputStream(f);
			ois = new ObjectInputStream(fis);
			while(true) {
				Object a = ois.readObject();
				if (a instanceof mokepon.Pocio)
					cadena+=("Objecte Pocio "+(mokepon.Pocio) a);
				else if (a instanceof mokepon.Reviure)
					cadena+=("Objecte Reviure "+(mokepon.Reviure) a);
				else if (a instanceof mokepon.Arma)
					cadena+=("Objecte Arma "+(mokepon.Arma) a);
				else if (a instanceof mokepon.Armadura)
					cadena+=("Objecte Armadura "+(mokepon.Armadura) a);
				cadena+="\n";
			}
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {			
			System.out.println(cadena);
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
	}
	public static ArrayList<mokepon.Objecte> obtenirObjectes(String nomF,String tipus) {
		File f = new File(nomF);
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		ArrayList<mokepon.Objecte> a = new ArrayList<mokepon.Objecte>();
		
		try {
			fis = new FileInputStream(f);
			ois = new ObjectInputStream(fis);
			switch (tipus.toUpperCase()) {
			case "POCIO":
				while(true) {
					Object b = ois.readObject();
					if (b instanceof mokepon.Pocio)
						a.add((mokepon.Pocio)b);
				}
			case "ARMA":
				while(true) {
					Object b = ois.readObject();
					if (b instanceof mokepon.Arma)
						a.add((mokepon.Arma)b);
				}
			case "ARMADURA":
				while(true) {
					Object b = ois.readObject();
					if (b instanceof mokepon.Armadura)
						a.add((mokepon.Armadura)b);
				}
			case "REVIURE":
				while(true) {
					Object b = ois.readObject();
					if (b instanceof mokepon.Reviure)
						a.add((mokepon.Reviure)b);
				}
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		return a;
	}
	public static void actualizaFicheroObjectes(String nomFO,String nomFT) {
		File f = new File(nomFO);
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		
		ArrayList<mokepon.Objecte> a = new ArrayList<mokepon.Objecte>();
		ArrayList<mokepon.Objecte> g = new ArrayList<mokepon.Objecte>();
		int num=0;
		
		try {
			fis = new FileInputStream(f);
			ois = new ObjectInputStream(fis);
			while(true) {
				Object b = ois.readObject();
				if(b instanceof mokepon.Objecte)
				a.add((mokepon.Objecte)b);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			try {
				File f1 = new File(nomFO);
				FileOutputStream fos = new FileOutputStream(f1,false);
				AppendableObjectOutputStream aoos = new AppendableObjectOutputStream(fos,true);
				
				boolean flag = true;
				for(mokepon.Objecte z : a) {
					File f2 = new File(nomFT);
					FileReader fr = new FileReader(f2);
					BufferedReader br= new BufferedReader(fr);
					while(br.ready()) {
						String s = br.readLine();
						String[] ss = s.split(";");
 						if((z.getNom().equals(ss[1])))
 							flag=false;
					}
					if(!flag) {
						num++;
					}else {
						g.add(z);
					}
					flag=true;
					br.close();
					fr.close();
				}
				for(mokepon.Objecte z : g) {
					aoos.writeObject(z);
				}
				aoos.close();
				fos.close();
				System.out.println("Numero de objetos borrados "+num);
				mostrarObjectes(nomFO);
			}catch (FileNotFoundException ex) {
				System.out.println("no existeix el fitxer");
				e.printStackTrace();
			} catch (IOException ex) {
			}		
		} catch (ClassNotFoundException ex) {
			System.out.println("no s'ha trobat la classe demanada");
			ex.printStackTrace();
		}
	}
	
}