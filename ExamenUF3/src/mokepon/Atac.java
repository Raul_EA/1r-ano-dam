package mokepon;
/*
 *  Crea una classe Atac amb els seg�ents atributs:
String nom
double poder  (un double entre 10 i 100)
Tipus tipus
int moviments_maxims
int moviments_actuals

Crea un parell de constructors. 
Al primer li passes nom, poder, tipus, i moviments_maxims (els actuals els posa igual als maxims) 
El constructor verificar� que poder sigui m�s gran o igual que 10 (si no ho �s, ho posar� autom�ticament a 10) i 
m�s petit o igual que 100 (si no ho �s, ho posar� autom�ticament a 100).
L�altre li passes nom i tipus, i posa poder a 10 i moviments_maxims a 10. 

 */

public class Atac implements java.io.Serializable {


	//Atributs
	String nom;
	double poder;	//  (un double entre 10 i 100)
	Tipus tipus;
	int moviments_maxims;
	int moviments_actuals;
	
	
	//Constructors
	public Atac(String nom, double poder, Tipus tipus, int moviments_maxims) {
		this.nom = nom;
		if (poder < 10) poder = 10;
		else if (poder > 100) poder = 100;
		this.poder = poder;
		this.tipus = tipus;
		this.moviments_maxims = moviments_maxims;
		this.moviments_actuals = moviments_maxims;
	}
	
	public Atac(String nom, Tipus tipus) {
		this.nom = nom;
		this.tipus = tipus;
		this.poder = 10;
		this.moviments_maxims = 10;
		this.moviments_actuals = this.moviments_maxims;
	}
	
	@Override
	//el override significa que sobreescriu el m�tode est�ndard de toString
    public String toString() {
        return "Atac [nom=" + nom + ", poder=" + poder + ", tipus=" + tipus + ", moviments_maxims=" + moviments_maxims
                + ", moviments_actuals=" + moviments_actuals + "]";
    }

	

}
