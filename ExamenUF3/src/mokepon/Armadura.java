package mokepon;

public class Armadura extends Objecte implements Equipament, java.io.Serializable {

	int defExtra;
	
	public Armadura(String nom, int def) {
		super(nom);
		this.defExtra = def;
		// TODO Auto-generated constructor stub
	}
	
	public Armadura(String nom, int q, int def) {
		super(nom);
		this.defExtra = def;
		// TODO Auto-generated constructor stub
	}
	
	// Equipar posar� l�armadura (per tant, a tu mateix) com a l�objecte equipat del Mokepon, 
	// i sumar� a la defensa del Mokepon la defExtra de l�armadura.
	@Override
	public void equipar(Mokepon mk) {
		// TODO Auto-generated method stub
		mk.objecteEquipat = this;
		mk.def = mk.def + this.defExtra;
	}

	//Desequipar posar� null com a objecte Equipat i restar� de la defensa
	//del Mokepon la defExtra de l'armadura.
	@Override
	public void desequipar(Mokepon mk) {
		// TODO Auto-generated method stub
		mk.objecteEquipat = null;
		mk.def = mk.def - this.defExtra;
	}

	@Override
	public void utilitzar(Mokepon mok) {
		// TODO Auto-generated method stub
		this.equipar(mok);
	}

	@Override
	public String toString() {
		return super.toString() + " [defExtra=" + defExtra + "]";
	}
	
	
}
