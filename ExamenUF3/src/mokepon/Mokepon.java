package mokepon;

import java.util.ArrayList;
import java.util.Random;

/*
 * 	Afegeix un nou atribut enter �exp�, i un altre enter �hp_max�, i un �hp_actual�
 *	Fes un nou m�tode a Mokepon atorgarExperiencia(int exp_atorgada) que sumi la experiencia atorgada a la exp actual. Assegura�t que �s publica.
 *	Si exp passa de 100, fes que es restin 100 d�experi�ncia i la funci� cridi a un nou m�tode Mokepon.pujarNivell() 
 *	a on augmenta el nivell en 1, hp_max un valor entre 0 i 5, i l�atac, defensa i velocitat un valor entre 0 i 2 amb un random.
 *	Prova aquests m�todes en el main
 *	Caracte�ristiques Mokepon: https://www.wikidex.net/wiki/Caracter%C3%ADsticas
 */

/*
 * 	Fes que Mokepon tingui una variable atacs que sigui una llista de 2 Atac. Crea un nou m�tode a Mokepon 
 *  que es digui �public void afegirAtac(Atac at)� que afegeixi un nou atac a la llista d�atacs (sempre que en 
 *  tingui menys de 2)
	
	Lluita!
	
	Ja estas llest per fer una lluita de Mokepon. 
	La f�rmula per lluitar �s la seg�ent
	
	(((2 * nivell / 5) + 2) * poder_atac * atac_atacant * defensa_atacat) / 50 + 2) * modificador_de_tipus
	
	a on Level es el nivell del Mokepon atacant, Power el poder de l�atac, A l�atac del Mokepon atacant, 
	D la defensa del Mokepon que rep l�atac, i 
	Modifier el modificador de tipus, a on 
	- �s 2 si �s efectiu, 
	- 1 si es normal, i 
	- 0.5 si �s poc efectiu
	Foc �s efectiu contra planta i poc efectiu contra aigua
	Planta �s efectiva contra aigua i poc efectiva contra foc
	Aigua �s efectiva contra foc i poc efectiva contra planta.
 */
public class Mokepon implements java.io.Serializable {
    
	static Random rnd = new Random();
	//atributs del pokemon
	protected String nom;			//Nom del Mokepon
	protected int nivell;			//nivell
	protected int atk;			//Poder d'atac
	protected int def;			//Poder de defensa
	protected int vel;			//Velocitat
	protected int exp;			//Experi�ncia
	protected int hp_max;			//Valor m�xim de la condici� f�sica
	protected int hp_actual;		//Condici� f�sica del Mokemon
	protected Tipus tipus;		//Tipus de Mokepon
	protected ArrayList<Atac> atacs = new ArrayList<Atac>();
	protected boolean debilitat = false;
    
	 static String estatic;

	 //Crea un nou atribut objecteEquipat a Mokepon, de classe Equipament.
	 Equipament objecteEquipat;
	 
    //Constructors
    //fixa't que el constuctor no t� retorn, ja que el que retorna es la propia classe
    public Mokepon() {
        this.nom = "Sense definir";
        this.nivell = 1;
        this.atk = 1;
        this.def = 1;
        this.vel = 1;
        this.hp_max = 10;
    }
    
    
    //podem crear m�ltiples constructors sempre que tinguin par�metres diferents
    public Mokepon(String nom) {
        //this.nom fa refer�ncia a la variable global i nom a la local que hem passat per par�metre
        this.nom = nom;
        this.nivell = 1;
        this.atk = 1;
        this.def = 1;
        this.vel = 1;
        this.hp_max = 10;
    }
    
  //podem crear m�ltiples constructors sempre que tinguin par�metres diferents
    public Mokepon(String nom, Tipus tipus) {
        //this.nom fa refer�ncia a la variable global i nom a la local que hem passat per par�metre
        this.nom = nom;
        this.nivell = 1;
        this.atk = 1;
        this.def = 1;
        this.vel = 1;
        this.tipus = tipus;
    }

    
    public Mokepon(String nom, int nivell) {
        //aquest constructor crida al constructor de nom primer, per aix� el this(nom), amb el que far� tot el que el constructor de nom digui
        this(nom);
        //podem fer fors dintre d'un constructor com si fos una funci� normal
        for (int i = 1; i < nivell; i++) {
            //podem cridar a funcions de la nostra propia classe amb el this
            this.pujarNivell();
        }
    }

    /*
     * 	Amplia els constructors fent el seg�ent
		Modifica els constructors per a afegir tamb� el camp hp_max que has afegit. El valor per defecte d�un nivell 1 ser� 10
		Afegeix un nou constructor a on li passis nom, nivell, hp_max, atk, def, vel, i et crei un pokemon amb aquests valors
     */
    public Mokepon(String nom, int nivell, int atk, int def, int vel, int hp_max, int hp_actual) {
		this.nom = nom;
		this.nivell = nivell;
		this.atk = atk;
		this.def = def;
		this.vel = vel;
		this.hp_max = hp_max;
		this.hp_actual = hp_actual;
	}
    
    //getters i setters
    /*public int getHp_actual() {
		return hp_actual;
	}
     */
    /*
     * Modifica el setter de setHp per a que, si posem un valor negatiu, en comptes d�aix� posi 0 a l�atribut, i si poses un valor m�s gran de max_hp, posi max_hp a l�atribut.
     */
	public void setHp_actual(int hp) {
		if (hp <0) hp = 0;
		if (hp > hp_max) hp = hp_max;
		this.hp_actual = hp;
	}
    
    //Comportament de la classe
    public void diguesNom() {
        //per a accedir a un atribut fem this.nomDeLAtribut
        //this es una paraula especial que fa referencia a nosaltres mateixos.
        System.out.println(this.nom);
    }
    
	public void atorgarExperiencia(int exp_atorgada) {
    	this.exp = exp + exp_atorgada;
    	if (this.exp > 100) {
    		this.exp = this.exp - 100;
    		this.pujarNivell();
    	}
    }

	private void pujarNivell() {
		// TODO Auto-generated method stub
		this.nivell++;
		this.hp_max = rnd.nextInt(5);
		this.atk = rnd.nextInt(3);
		this.def = rnd.nextInt(3);
		this.vel = rnd.nextInt(3);
	}
	
	public void afegirAtac(Atac at) {
		if (this.atacs.size() < 2) 
			this.atacs.add(at);
	}
	//M�tode estatic perqu� no es propi d'un objecte Mokepon concret, sino de la classe
	protected static double efectivitat(Tipus atac, Tipus defensa) {
        if(atac == Tipus.FOC && defensa == Tipus.AIGUA ||atac == Tipus.AIGUA && defensa == Tipus.PLANTA ||atac == Tipus.PLANTA && defensa == Tipus.FOC ) {
            return 0.5;
        }else if (atac == Tipus.AIGUA && defensa == Tipus.FOC ||atac == Tipus.FOC && defensa == Tipus.PLANTA ||atac == Tipus.PLANTA && defensa == Tipus.AIGUA ) {
            return 2;
        }else {
            return 1;
        }        
    }
	
	/*
	 * 	Fes un m�tode a Mokepon que sigui public void atacar(Mokepon atacat, int num_atac) a on agafar� 
	 * l�atac en aquella posici� a la llista, calculara el dany segons la f�rmula esmentada, i restar� 
	 * la vida al Mokepon atacat. 
	*/
	public void atacar(Mokepon atacat, int num_atac) {
		if (!this.debilitat) {
			Atac at = this.atacs.get(num_atac);
			//La f�rmula per lluitar �s la seg�ent : (((2 * nivell / 5) + 2) * poder_atac * atac_atacant * defensa_atacat) / 50 + 2) * modificador_de_tipus
			
			double dany = (((( (2 * this.nivell) / 5) + 2) * at.poder * this.atk * atacat.def) / 50 + 2) * efectivitat(at.tipus, atacat.tipus);
			atacat.hp_actual = atacat.hp_actual - (int) dany;
			if(atacat.hp_actual<0)
	              atacat.debilitar();
		}
	}
	
	//Crea un atribut boolean debilitat i fes tamb� un m�tode debilitarse() que activi l�atribut debilitat. 
	//Modifica atacar de forma que un Mokepon debilitat no pugui atacar. 
	public void debilitar() {
		this.debilitat = true;
	}
	
	//Fes per �ltim un metode curar() que desactiva debilitat (si estava activat) i posi la vida a hp_max. 
	//Assegura�t que tots els m�todes son publics
	public void curar() {
		if (this.debilitat) {
			this.debilitat = false;
			this.hp_actual = this.hp_max;
		}
	}
	 
	public MokeponCapturat capturar(String nomEntrenador, String nomDonat) {
		 
		//si nosaltres mateixos 
        if(this instanceof MokeponCapturat) {
        	System.out.println("No pots capturar un Mokepon que ja esta capturat");
        	return null;
            //capturar el Mokepon
        }else //podem castejar perque estem segurs de que es un MokeponCapturat gracies al instanceof
            
        	return new MokeponCapturat(this, nomDonat, nomEntrenador);		
	}
	
	/*
	 * �Fes un equals a Mokepon i a MokeponCapturat.
	 * dos Mokepon seran iguals si tenen les mateixes estad�stiques 
	 * (excepte hp_actual, i debilitat, ja que seria el mateix Mokepon, per� ferit). 
	 * �s a dir, haurien de tenir iguals nom, sexe, nivell, tipus, atk, def, vel, hp_max. 
	 * Dos MokeponCapturats seran iguals si, apart dels par�metres de Mokepon, tenen igual entrenador i nomDonat.�
	 */
	public boolean equals(Object obj) {
		if (!(obj instanceof MokeponCapturat)) return false;
	
		if (this.nom.equals(((Mokepon)obj).nom) && this.tipus == ((Mokepon)obj).tipus && 
				this.nivell == ((Mokepon)obj).nivell && this.atk == ((Mokepon)obj).atk && 
				this.def == ((Mokepon)obj).def && this.exp == ((Mokepon)obj).exp && 
				this.vel == ((Mokepon)obj).vel && this.hp_max == ((Mokepon)obj).hp_max)
					return true;
		return false;
	}


	@Override
	public String toString() {
		return "Mokepon [nom=" + nom + ", nivell=" + nivell + ", atk=" + atk + ", def=" + def + ", vel=" + vel
				+ ", exp=" + exp + ", hp_max=" + hp_max + ", hp_actual=" + hp_actual + ", tipus=" + tipus + ", atacs="
				+ atacs + ", debilitat=" + debilitat + ", objecteEquipat=" + objecteEquipat + "]";
	}
	
	
	
}
