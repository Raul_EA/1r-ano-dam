package mokepon;

//extends Mokepon implica que �s filla de Mokepon, i hereda tots els m�todes i atributs de Mokepon
public class MokeponCapturat extends Mokepon implements java.io.Serializable {
  
  //posem nom�s els atributs nous, no els de Mokepon. �s a dir, posem els atributs que tindr� MokeponCapturat, pero no Mokepon
  private String nomPosat;
  private String nomEntrenador;
  private int felicitat;
  private Objecte objecte;//Afegeix a MokeponCapturat un atribut de tipus Objecte
  
  static int nombreMokeponsCapturats = 0;
  
  public MokeponCapturat(String nom, Tipus tipus) {
      //cridem al constructor id�ntic del pare
      super(nom, tipus);
      //la resta de variables les posem nosaltres
      this.nomPosat = nom;
      this.nomEntrenador = "Marc";
      this.felicitat = 50;
      nombreMokeponsCapturats++;
  }
  
  /*
   * Fes la resta de constructors de Mokepon fent servir el super.
   * Fes a m�s, un nou constructor a on li passes (Mokepon mok, String nomPosat, String nomEntrenador) i cre� un nou MokeponCapturat en base a les dades del Mokepon proporcionat (posa la felicitat a 50)
   */
  public MokeponCapturat() {
	  super();
	  this.nomPosat = "Sense nom";
      this.nomEntrenador = "Proves";
      this.felicitat = 50;
      nombreMokeponsCapturats++;
  }
  
  public MokeponCapturat(String nom) {
      //cridem al constructor id�ntic del pare
      super(nom);
      //la resta de variables les posem nosaltres
      this.nomPosat = nom;
      this.nomEntrenador = "Proves";
      this.felicitat = 50;
      nombreMokeponsCapturats++;
  }
  
  public MokeponCapturat(String nom, int nivell) {
	  super(nom, nivell);
	  this.nomPosat = nom;
      this.nomEntrenador = "Proves";
      this.felicitat = 50;
      nombreMokeponsCapturats++;
  }
  
  public MokeponCapturat(String nom, int nivell, int atk, int def, int vel, int hp_max, int hp_actual) {
	  super(nom, nivell, atk, def, vel, hp_max, hp_actual);
	  this.nomPosat = nom;
      this.nomEntrenador = "Proves";
      this.felicitat = 50;
      nombreMokeponsCapturats++;
  }
  
  public MokeponCapturat(Mokepon mok, String nomPosat, String nomEntrenador) {
	  super(mok.nom, mok.nivell, mok.atk, mok.def, mok.vel, mok.hp_max, mok.hp_actual);
	  this.nomPosat = nomPosat;
      this.nomEntrenador = nomEntrenador;
      this.felicitat = 50;
      nombreMokeponsCapturats++;
  }
  
  //getters i setters
  public String getNomPosat() {
		return nomPosat;
  }

  public void setNomPosat(String nomPosat) {
		this.nomPosat = nomPosat;
  }

  public String getNomEntrenador() {
		return nomEntrenador;
  }

  public void setNomEntrenador(String nomEntrenador) {
		this.nomEntrenador = nomEntrenador;
  }
  
  public void setObjecte(Objecte obj) {
	  this.objecte = obj;
  }
  
  public Objecte getObjecte() {
	  return this.objecte;
  }

  //Comportament
  public void acariciar() {
	  if(this.felicitat < 100) this.felicitat = this.felicitat + 10;
  }

	/*
	 *  Implementa a MokeponCapturat un m�tode atacar que sobreescrigui el de Mokepon amb les seg�ents caracter�stiques
	si felicitat est� a 50 o m�s, es multiplica el dany per 1.2. En cas contrari es multiplica per 0.8.
	Implementa-ho escrivint el m�tode sencer o fent servir super.atacar()
	
	 */
	@Override
	public void atacar(Mokepon atacat, int num_atac) {
		// TODO Auto-generated method stub
		//super.atacar(atacat, num_atac);
		if (!this.debilitat) {
			Atac at = this.atacs.get(num_atac);
			//La f�rmula per lluitar �s la seg�ent : (((2 * nivell / 5) + 2) * poder_atac * atac_atacant * defensa_atacat) / 50 + 2) * modificador_de_tipus
			
			double dany = (((( (2 * this.nivell) / 5) + 2) * at.poder * this.atk * atacat.def) / 50 + 2) * efectivitat(at.tipus, atacat.tipus);
			if (this.felicitat >= 50) dany = dany * 1.2;
			else dany = dany * 0.8;
			atacat.hp_actual = atacat.hp_actual - (int) dany;
			if(atacat.hp_actual<0)
	              atacat.debilitar();
		}
	}
  
	public void utilitzarObjecte() {
		this.objecte.utilitzar(this);
	}

	/*
	 * �Fes un equals a Mokepon i a MokeponCapturat.
	 * dos Mokepon seran iguals si tenen les mateixes estad�stiques 
	 * (excepte hp_actual, i debilitat, ja que seria el mateix Mokepon, per� ferit). 
	 * �s a dir, haurien de tenir iguals nom, sexe, nivell, tipus, atk, def, vel, hp_max. 
	 * Dos MokeponCapturats seran iguals si, apart dels par�metres de Mokepon, tenen igual entrenador i nomDonat.�
	 */
	public boolean equals(Object obj) {
		if (!(obj instanceof MokeponCapturat)) return false;
		
		if (!super.equals(obj)) return false;
	
		return this.nomEntrenador.equals(((MokeponCapturat)obj)) && this.nomPosat.equals((MokeponCapturat)obj);
	}

	@Override
	public String toString() {
		return super.toString() + " -- " + "MokeponCapturat [nomPosat=" + nomPosat + ", nomEntrenador=" + nomEntrenador + ", felicitat=" + felicitat
				+ ", objecte=" + objecte + "]";
	}

	public int getNivell() {
		// TODO Auto-generated method stub
		return nivell;
	}

	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}
	
	
	
	
  
}
