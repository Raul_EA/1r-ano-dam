package mokepon;

//L�equipament �s com un objecte, per� a m�s 
//incorpora dues noves accions, equipar i desequipar. 

public interface Equipament {

	public abstract void equipar(Mokepon mk); 
	public abstract void desequipar(Mokepon mk);
	
	// et retorna si se li pot posar un equipament. 
	//A un Mokepon se li pot posar un equipament si no t� un equip 
	//ja posat i no est� debilitat.

	public default boolean potEquipar(MokeponCapturat mok) {
		return !mok.debilitat && mok.objecteEquipat != null;
	}
	
	/*
	 * Tamb� podria ser que haguessis donat l�equipament en comptes
	 *  d�haver-ho equipat. Crea un altre m�tode a Equipament 
	 *  a on li passes un Mokepon i et diu si l�objecte que t� a la 
	 *  casella objecte (no a la casella equip) �s un Equipament. 
	 *  Fes servir instanceof per a aix�

	 */
	
	default boolean equipMalPosat(MokeponCapturat mok) {
		return mok.getObjecte() instanceof Equipament;
	}
}
