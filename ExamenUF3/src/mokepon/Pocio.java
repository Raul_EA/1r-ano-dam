package mokepon;


public class Pocio extends Objecte implements java.io.Serializable {

	int hp_curada = 10;
	
	public Pocio(String nom) {
		super(nom);
		// TODO Auto-generated constructor stub
	}
	
	public Pocio(String nom, int hp) {
		super(nom);
		this.hp_curada = hp;
		// TODO Auto-generated constructor stub
	}

	public Pocio(String nom, int q, int hp) {
		super(nom, q);
		this.hp_curada = hp;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void utilitzar(Mokepon mok) {
		// TODO Auto-generated method stub
		//hp_curada de l'objecte cura a la hp del mokepon, pero mai pot ser m�s que la seva hp_max. 
		//Si el Mokepon est� debilitat, no cura res.
		//disminueix en un la quantitat. Si la quantitat �s menor a 1, no fa res
		if (this.getQuantitat() > 1) {
			if (!mok.debilitat) {
				if (mok.hp_max >= this.hp_curada) mok.hp_actual = hp_curada;
			}
			this.quantitat--;
		}
	}

	@Override
	public String toString() {
		return super.toString() + " [hp_curada=" + hp_curada + "]";
	}
	
	

}
