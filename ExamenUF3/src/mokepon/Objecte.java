package mokepon;

//Una classe abstracta no pot instanci�rse (�s a dir, no pots fer un new). Nom�s les classes abstractes poden tenir m�todes abstractes
public abstract class Objecte implements java.io.Serializable {
	
	/*
	 * Fes una classe Objecte. Fes-li els atributs String nom i int Quantitat. 
	 * Posa�ls privats els dos. Posa�ls getters pero no setters
	 */
	protected String nom;
	protected int quantitat;
	
	public String getNom() {
		return nom;
	}

	public int getQuantitat() {
		return quantitat;
	}


	public Objecte(String nom) {
		this.nom = nom;
		this.quantitat = 1;
	}
	
	public Objecte(String nom, int quantitat) {
		super();
		this.nom = nom;
		this.quantitat = quantitat;
	}

	public void obtenir(int numObjectes) {
		this.quantitat = this.quantitat + numObjectes;
	}

	//fes un m�tode �donar� que li passes un MokeponCapturat, i posa el propi objecte (this) com a l�atribut objecte de Mokepon
	public void donar(MokeponCapturat mc) {
		mc.setObjecte(this);
	}
	
	//Un m�tode abstracte significa que �s un m�tode que no existeix per� els seus fills estan obligats a implementar-lo. Fixa�t que es tanca amb punt i coma sense obrir i definir el m�tode    
	public abstract void utilitzar(Mokepon mok);

	@Override
	public String toString() {
		return "nom=" + nom + ", quantitat=" + quantitat;
	}
	
	
}

