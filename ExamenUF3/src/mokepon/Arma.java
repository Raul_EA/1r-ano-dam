package mokepon;

public class Arma extends Objecte implements Equipament, java.io.Serializable {

	int atacExtra;
	
	public Arma(String nom, int atk) {
		super(nom);
		this.atacExtra = atk;
		// TODO Auto-generated constructor stub
	}

	public Arma(String nom, int q, int atk) {
		super(nom, q);
		this.atacExtra = atk;
		// TODO Auto-generated constructor stub
	}
	// Equipar posar� l�arma (per tant, a tu mateix) com a l�objecte equipat del Mokepon, 
	// i sumar� a l�atac del Mokepon l�atacExtra de l�arma.
	@Override
	public void equipar(Mokepon mk) {
		// TODO Auto-generated method stub
		mk.objecteEquipat = this;
		mk.atk = mk.atk + this.atacExtra;
	}

	//Desequipar posar� null com a objecte Equipat i restar� de l�atac 
	//del Mokepon l�atacExtra d�arma.
	@Override
	public void desequipar(Mokepon mk) {
		// TODO Auto-generated method stub
		mk.objecteEquipat = null;
		mk.atk = mk.atk - this.atacExtra;
	}

	@Override
	public void utilitzar(Mokepon mok) {
		// TODO Auto-generated method stub
		this.equipar(mok);
	}

	@Override
	public String toString() {
		return super.toString() + " [atacExtra=" + atacExtra + "]";
	}
	
	

}
