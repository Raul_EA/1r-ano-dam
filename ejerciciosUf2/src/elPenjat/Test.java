package elPenjat;

public class Test {
/**
 * Juego del AHORCADO
 * @author Raul Edo Andres
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int op;
		Jugador j = new Jugador();
		Verificador v = new Verificador();
		do {
			op=Funciones.menu();
			switch (op){
			case 1:
				Jugador.definirJug(j,v);
				break;
			case 2:
				if(v.defJug==true) {
					Funciones.jugar(j);
				} else {
					System.err.println("Defina primero el jugador");
				}
				break;
			case 3:
				if(v.defJug==true) {
					Jugador.verJug(j);
				}else {
					System.err.println("Defina primero el jugador");
				}
				break;
			case 0:
				System.out.println("Saliendo...");
				break;
			}
		}while(op!=0);
	}

}
