package elPenjat;

import java.util.ArrayList;

public class Verificador {
	
	boolean defJug;
	ArrayList<Character> letras = new ArrayList<Character>();
	public static boolean verCharNor(char let) {
		if((let>=97 && let<=122) || (let>=65 && let<=90)) {
			return true;
		}else {
			return false;
		}
	}
	public static boolean verGan(char[]pal,char[]ocu) {
		boolean igual = true;
		for(int i=0;i<pal.length;i++) {
			if(pal[i]!=ocu[i]) {
				igual = false;
			}
		}
		if(igual) {
			return true;
		}else {
			return false;
		}
	}
	public static boolean verCharNoUsed(char let,ArrayList<Character> letras) {
		if(!letras.contains(let)) {
			return true;
		}else {
			return false;
		}
	}
}
