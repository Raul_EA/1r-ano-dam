package elPenjat;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Funciones {
	public static Scanner sc = new Scanner(System.in);
	public static  Random rd = new Random();
	
	public static int menu() {	
		System.out.println("EL PENJAT");
		System.out.println("1-Definir jugador");
		System.out.println("2-Jugar partida");
		System.out.println("3-Ver juagdor");
		System.out.println("0-Salir");
		int op=sc.nextInt();
		return op;
	}

	public static void jugar(Jugador j) {
		/*jugar
		 * modo de juego 
		 * 
		 * NORMAL --> basico
		 * DIFICIL --> no se valen vocales
		 * 
		 * 1-escoger palabra
		 * 2-mostrar caracteres
		 * 3-elegir introducir letra 
		 * 4.1-introducir letra
		 * 4.1.1-sí esta muestra los espacios rellenados de la letra
		 * 4.1.2-sí no esta muestra muestra de nuevo los espacios y cuent 1 error
		 * 4.1.3-Repetir hasta numero maximo de intentos --> PIERDES
		 * 4.1.4-Se hayan rellenado todas las letras --> GANAS
		 * 4.2-introducir palabra
		 * 4.2.1-sea correcta, sumar victoria --> GANAS
		 * 4.2.2-sea incorrecto, finalizar partida --> PIERDES 
		 */
		sc.nextLine();
		char[] pal;
		pal=escogerPal();
		char[] ocu=new char[pal.length];
		palabraOcu(ocu);
		j.ganadas+=turnos(pal,ocu);
	}
	public static int turnos(char[] pal,char[]ocu) {
		int intent=6;
		boolean gan=false;
		char let;
		Verificador v = new Verificador();
		while(intent>0 && gan!=true) {
			mostrar(ocu,intent);
			let=introducirPal(v.letras);
			intent-=cambiarOcu(pal,ocu,let);
			gan=Verificador.verGan(pal,ocu);
		}
		if(gan==true) {
			System.out.println("\nGANASTE :)");
			return 1;
		}else {
			System.out.println("\nPERDISTE :(");
			return 0;
		}
	}
	public static int cambiarOcu(char[] pal,char[] ocu, char let) {
		boolean trob=false;
		char let2 = Character.toLowerCase(let);
		for(int i=0;i<pal.length;i++) {
			if(pal[i]==let2) {
				ocu[i]=let2;
				trob = true;
			}
		}
		if(!trob) {
			System.out.println("Fallaste!!!");
			return 1;
		}else {
			System.out.println("Acertaste!!!");
			return 0;
		}
	}
	private static char introducirPal(ArrayList<Character> letras) {
		System.out.println("\nIntroduzca la letra deseada:");
		char let = sc.nextLine().charAt(0);
		while((!Verificador.verCharNor(let)) && (!Verificador.verCharNoUsed(let,letras))) {
			System.err.println("Valor incorrecto");
			System.out.println("Introduzca la letra deseada:");
			let = sc.nextLine().charAt(0);
		}
		letras.add(let);
		return let;
	}

	public static void mostrar (char[] ocu,int intent) {
		System.out.println("\nNumero de intentos restantes: "+intent);
		for(int i=0;i<ocu.length;i++) {
			System.out.print(ocu[i]+" ");
		}
		System.out.println();
	}
	public static void palabraOcu(char[] ocu) {
		for(int i=0;i<ocu.length;i++) {
			ocu[i]='_';
		}
	}
	public static char[] escogerPal() {
		String[] palabras = {"a", "aaronita", "aarónico", "aba", "ababa", "ababillarse", "ababol", "abacal", "abacalero", "abacero", "abacería", "abacial", "abacora", "abacorar", "abacá", "abad", "abada", "abadejo", "abadengo", "abadernar", "abadesa", "abadiado", "abadiato", "abadí", "abadía", "abajadero", "abajamiento", "abajar", "abajera", "abajeño", "abajo", "abalada", "abalanzar", "abalar", "abalaustrado", "abaldonadamente", "abaldonamiento", "abaldonar", "abaleador", "abaleadura", "abalear", "abaleo", "abalizamiento", "abalizar", "aballar", "aballestar", "abalorio", "abaluartar", "abanador", "abanar", "abancalar", "abanderado", "abanderamiento", "abanderar", "abanderizador", "abanderizar", "abandonado", "abandonamiento", "abandonar", "abandonismo", "abandonista", "abandono", "abanear", "abanicar", "abanicazo", "abanico", "abanillo", "abanino", "abaniqueo", "abaniquero", "abaniquería", "abano", "abanto", "abarajar", "abaratamiento", "abaratar", "abarañar", "abarbechar", "abarbetar", "abarca", "abarcable", "abarcado", "abarcador", "abarcadura", "abarcamiento", "abarcar", "abarcuzar", "abarcón", "abareque", "abaritonado", "abarloar", "abarquero", "abarquillado", "abarquillamiento", "abarquillar", "abarracar", "abarrado", "abarraganamiento", "abarraganarse", "abarrajado", "abarrajar", "abarramiento", "abarrancadero", "abarrancamiento", "abarrancar", "abarrar", "abarraz", "abarredera", "abarrenar", "abarrer", "abarrisco", "abarrotar", "abarrote", "abarrotero", "abarrotería", "abarse", "abastadamente", "abastamiento", "abastante", "abastanza", "abastar", "abastardar", "abastecedor", "abastecer", "abastecimiento", "abastero", "abastimiento", "abastionar", "abasto", "abasí", "abatanado", "abatanar", "abatatamiento", "abatatar", "abate", "abatible", "abatidero", "abatido", "abatidura", "abatimiento", "abatir", "abatismo", "abatojar", "abatí", "abayado", "abazón", "abañador", "abañadura", "abañar", "abderitano", "abdicación", "abdicar", "abdicativamente", "abdicativo", "abdomen", "abdominal", "abducción", "abductor", "abecedario", "abecé", "abedul", "abeja", "abejar", "abejarrón", "abejaruco", "abejear", "abejero", "abejonear", "abejoneo", "abejorrear", "abejorreo", "abejorro", "abejuno", "abejón", "abeldar", "abellacado", "abellacar", "abellota", "abellotado", "abelmosco", "abemoladamente", "abemolar", "abencerraje", "abenuz", "aberenjenado", "aberración", "aberrante", "aberrar", "abertal", "abertura", "aberzale", "abesana", "abestiado", "abestializado", "abesón", "abetal", "abetar", "abete", "abetinote", "abeto", "abetuna", "abetunado", "abetunar", "abey", "abia", "abiar", "abibollo", "abicharse", "abierta", "abiertamente", "abierto", "abiete", "abietino", "abietáceo", "abietíneo", "abigarrado", "abigarramiento", "abigarrar", "abigeato", "abigeo", "abigotado", "abinar", "abintestato", "abiogénesis", "abipón", "abisagrar", "abisal", "abiselar", "abisinio", "abismado", "abismal", "abismar", "abismo", "abismático", "abiso", "abita", "abitadura", "abitaque", "abitar", "abitón", "abizcochado", "abiótico", "abjuración", "abjurar", "ablación", "ablandabrevas", "ablandador", "ablandahígos", "ablandamiento", "ablandar", "ablandativo", "ablande", "ablandecer", "ablanedo", "ablano", "ablativo", "ablegado", "ablentador", "ablentar", "ablución", "ablusado", "abnegación", "abnegado", "abnegar", "abobado", "abobamiento", "abobar", "abobra", "abocadear", "abocado", "abocamiento", "abocanar", "abocar", "abocardado", "abocardar", "abocardo", "abocelado", "abocetado", "abocetamiento", "abocetar", "abochornado", "abochornar", "abocinado", "abocinamiento", "abocinar", "abofado", "abofarse", "abofeteador", "abofetear", "abogacía", "abogaderas", "abogadesco", "abogadil", "abogadismo", "abogado", "abogador", "abogamiento", "abogar", "abohetado", "abolaga", "abolengo", "abolicionismo", "abolicionista", "abolición", "abolir", "abollado", "abolladura", "abollar", "abollonar", "abollón", "abolorio", "abolsado", "abolsarse", "abomaso", "abombado", "abombamiento", "abombar", "abominable", "abominación", "abominar", "abonable", "abonado", "abonador", "abonanza", "abonanzar", "abonar", "abonaré", "abondado", "abondamiento", "abondar", "abondo", "abondoso", "abonero", "abono", "aboquillado", "aboquillar", "aboral", "abordable", "abordador", "abordaje", "abordar", "abordo", "abordonar", "aborigen", "aborlonado", "aborrachado", "aborrajarse", "aborrascarse", "aborrecedor", "aborrecer", "aborrecible", "aborrecimiento", "aborregado", "aborregarse", "aborrible", "aborrir", "aborronar", "aborso", "abortadura", "abortamiento", "abortar", "abortista", "abortivo", "aborto", "abortón", "aborujar", "abotagamiento", "abotagarse", "abotargarse", "abotinado", "abotonador", "abotonadura", "abotonar", "abovedado", "abovedar", "aboyado", "aboyar", "abozalar", "abozo", "abra", "abracadabra", "abracadabrante", "abracar", "abracijarse", "abracijo", "abrasadamente", "abrasador", "abrasamiento", "abrasar", "abrasilado", "abrasivo", "abrasión", "abravar", "abravecer", "abraxas", "abrazada", "abrazadera", "abrazador", "abrazamiento", "abrazar", "abrazo", "abreboca", "abrebotellas", "abrecartas", "abrecoches", "abregancias", "abrelatas", "abrenunciar", "abrenuncio", "abreojos", "abrepuño", "abretonar", "abrevadero", "abrevador", "abrevar", "abreviación", "abreviadamente", "abreviado", "abreviador", "abreviaduría", "abreviamiento", "abreviar", "abreviatura", "abreviaturía", "abrezar", "abriboca", "abribonado", "abribonarse", "abridero", "abridor", "abrigada", "abrigadero", "abrigado", "abrigador", "abrigadura", "abrigamiento", "abrigar", "abrigaño", "abrigo", "abril", "abrileño", "abrillantador", "abrillantar", "abrimiento", "abriolar", "abrir", "abrochador", "abrochadura", "abrochamiento", "abrochar", "abrogación", "abrogar", "abrojal", "abrojillo", "abrojo", "abrojín", "abroma", "abromado", "abromar", "abroncar", "abroquelado", "abroquelar", "abrotoñar", "abrumador", "abrumar", "abrumarse", "abrunal", "abruno", "abrupción", "abrupto", "abrutado", "abruzarse", "abruzo", "abruñal", "abruño", "abrótano", "absceso", "abscisa", "abscisión", "absconder", "absencia", "absenta", "absentarse", "absente", "absentismo", "absentista", "absidal", "absidiolo", "absintio", "absolución", "absoluta", "absolutamente", "absolutidad", "absolutismo", "absolutista", "absoluto", "absolutorio", "absolvederas", "absolvedor", "absolver", "absorbencia", "absorbente", "absorber", "absorbible", "absorbimiento", "absorciómetro", "absorción", "absortar", "absorto", "abstemio", "abstencionismo", "abstencionista", "abstención", "abstener", "abstergente", "absterger", "abstersivo", "abstersión", "abstinencia", "abstinente", "abstracción", "abstractivo", "abstracto", "abstraer", "abstraído", "abstruso", "absuelto", "absurdidad", "absurdo", "abubilla", "abubo", "abuchear", "abucheo", "abuela", "abuelastro", "abuelo", "abuhado", "abuhamiento", "abuhardillado", "abujardar", "abuje", "abulaga", "abulagar", "abulense", "abulia", "abullonar", "abulonar", "abultado", "abultamiento", "abultar", "abulón", "abundado", "abundamiento", "abundancia", "abundancial", "abundante", "abundantemente", "abundar", "abundo", "abundoso", "abur", "aburar", "aburelado", "aburguesamiento", "aburguesarse", "aburrado", "aburrarse", "aburrición", "aburridamente", "aburrido", "aburridor", "aburrimiento", "aburrir", "aburujar", "abusado", "abusador", "abusar", "abusionero", "abusivo", "abusión", "abuso", "abusón", "abuzarse", "abuñolado", "abuñolar", "abuñuelado", "abuñuelar", "abyección", "abyecto", "abés", "abéstola", "abéñola", "abéñula", "abúlico", "aca", "acabable", "acabada", "acabadamente", "acabado", "acabador", "acabalar", "acaballadero", "acaballado", "acaballar", "acaballerado", "acaballerar", "acaballonar", "acabamiento", "acabar", "acabañar", "acabe", "acabellado", "acabestrillar", "acabijo", "acabildar", "acabo", "acabose", "acabronado", "acacharse", "acachetar", "acachetear", "acachorrar", "acacia", "acaciano", "academia", "academicismo", "academicista", "academio", "academista", "academizar", "acadio", "académicamente", "académico", "acaecedero", "acaecer", "acaecimiento", "acafresna", "acaguasarse", "acahual", "acairelar", "acal", "acalabrotar", "acalambrar", "acaldar", "acalefo", "acalenturarse", "acalia", "acallador", "acallantar", "acallar", "acalmar", "acaloradamente", "acalorado", "acaloramiento", "acalorar", "acaloro", "acaloñar", "acalugar", "acalumniador", "acalumniar", "acalórico", "acamado", "acamar", "acamaya", "acambrayado", "acamellado", "acamellonar", "acampada", "acampamento", "acampanado", "acampanar", "acampar", "acampo", "acamuzado", "acanalado", "acanalador", "acanaladura", "acanalar", "acanallado", "acanallar", "acandilado", "acanelado", "acanelonar", "acanillado", "acanilladura", "acantalear", "acantarar", "acantear", "acantilado", "acantilar", "acantinflado", "acantio", "acanto", "acantocéfalo", "acantonamiento", "acantonar", "acantopterigio", "acantáceo", "acaparador", "acaparamiento", "acaparar", "acaparrarse", "acaparrosado", "acapillar", "acapizarse", "acaponado", "acapullarse", "acapulqueño", "acaracolado", "acarambanado", "acaramelar", "acarar", "acardenalar", "acareamiento", "acarear", "acariciador", "acariciar", "acarminado", "acarnerado", "acaronar", "acarralado", "acarraladura", "acarralar", "acarrarse", "acarrascado", "acarrazarse", "acarreadizo", "acarreador", "acarreamiento", "acarrear", "acarreo", "acarretar", "acarreto", "acartonarse", "acasamatado", "acasarado", "acaserarse", "acaso", "acastañado", "acastillado", "acastorado", "acatable", "acatadamente", "acatadura", "acatalecto", "acataléctico", "acatamiento", "acatanca", "acatar", "acatarrar", "acates", "acato", "acatólico", "acaudalado", "acaudalador", "acaudalar", "acaudillador", "acaudillamiento", "acaudillar", "acaule", "acautelarse", "acañaverear", "acañonear", "acañutado", "acceder", "accesibilidad", "accesible", "accesional", "accesión", "acceso", "accesoria", "accesoriamente", "accesorio", "accidentabilidad", "accidentadamente", "accidentado", "accidental", "accidentalidad", "accidentalmente", "accidentar", "accidentariamente", "accidentario", "accidente", "accionado", "accionamiento", "accionar", "accionariado", "accionarial", "accionario", "accionista", "accitano", "acción", "accésit", "acebadamiento", "acebadar", "acebal", "acebeda", "acebedo", "acebibe", "acebo", "acebollado", "acebolladura", "acebrado", "acebuchal", "acebuche", "acebucheno", "acebuchina", "acechadera", "acechadero", "acechador", "acechamiento", "acechanza", "acechar", "aceche", "acecho", "acechón", "acecido", "acecinar", "acedamente", "acedar", "acedera", "acederaque", "acederilla"};
		String palabra = palabras[rd.nextInt(palabras.length)];
		char[] pal = palabra.toCharArray();
		return pal;
	}
}
