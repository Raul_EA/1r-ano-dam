package elPenjat;

import java.util.Scanner;

public class Jugador {
	
	public String nom;
	public int ganadas;
	public static Scanner sc = new Scanner(System.in);
	public static void definirJug(Jugador j, Verificador v) {
		System.out.println("--------------------JUGADOR--------------------");
		System.out.println("Introduzca el nombre del jugador:");
		j.nom = sc.nextLine();
		System.out.println("Introduzca el numero de partidas ganadas:");
		j.ganadas = sc.nextInt();
		System.out.println("-----------------------------------------------\n");
		v.defJug=true;
	}
	public static void verJug(Jugador j) {
		System.out.println("--------------------JUGADOR--------------------");
		System.out.println("| Nombre: "+j.nom);
		System.out.println("| Ganadas: "+j.ganadas);
		System.out.println("-----------------------------------------------\n");
	}
	
}
