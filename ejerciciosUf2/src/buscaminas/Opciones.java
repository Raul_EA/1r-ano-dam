package buscaminas;

import java.util.Scanner;

public class Opciones {
	
	static Scanner sc = new Scanner(System.in);
	
	public static int escTab() {
		System.out.println("\nEscoja la dificultad de la partida:");
		System.out.println("1.-Principiante 8x8 casillas 10 minas");
		System.out.println("2.-Intermedio 16x16 casillas 40 minas");
		System.out.println("3.-Experto 16x30 casillas 99 minas");
		System.out.println("4.-Personalizado");
		int a = sc.nextInt();
		while(a>4 || a<1) {
			System.out.println("Valor introducido incorrecto, porfavor vuelva a introducir otro valor");
			System.out.println("\nEscoja la dificultad de la partida:");
			System.out.println("1.-Principiante 8x8 casillas 10 minas");
			System.out.println("2.-Intermedio 16x16 casillas 40 minas");
			System.out.println("3.-Experto 16x30 casillas 99 minas");
			System.out.println("4.-Personalizado");
			a=sc.nextInt();
		}
		if(a==4) {
			System.out.println("Introduce las dimensiones del tablero:");
			Posiciones.per_1=sc.nextInt();		
			Posiciones.per_2=sc.nextInt();
			System.out.println("Introduce el numero de minas:");
			Posiciones.min=sc.nextInt();
			while(Posiciones.per_1*Posiciones.per_2<Posiciones.min) {
				System.out.println("Hay mas minas que casillas en el tablero, porfavor vuelva a introducir el valor");
				Posiciones.min=sc.nextInt();
			}
		}
		return a;
	}
	
	public static void mostrarOpc(int opc) {
		System.out.println("-----------------------------MODO DE JUEGO-------------------------------");
		switch(opc) {
		case 1:
			System.out.println("| Dificultat: Principiante 8x8 casillas 10 minas");
			break;
		case 2:
			System.out.println("| Dificultat: Intermedio 16x16 casillas 40 minas");
			break;
		case 3:
			System.out.println("| Dificultat: Experto");
			break;
		case 4:
			System.out.println("| Dificultat: Personalizado");
			break;
	}
		System.out.println("-------------------------------------------------------------------------\n");
	}
}
