package buscaminas;

import java.util.Scanner;

public class Funciones {
	
	static Scanner sc = new Scanner (System.in);
	static int cas =0;
	
	public static int menu() {
		System.out.println("BUSCAMINAS\n1.-Mostrar ayuda\n2.-Definir jugador\n3.-Opciones\n4.-Jugar partida\n5.-Visualizar\n0.-Salir");
		int a=sc.nextInt();
		while(a>5 || a<=0) {
			System.out.println("Valor introducido incorrecto, porfavor vuelva a introducir otro valor");
			System.out.println("\nBUSCAMINAS\n1.-Mostrar ayuda\n2.-Opciones\n3.-Jugar partida\n4.-Visualizar\n0.-Salir");
			a=sc.nextInt();
		}
		return a;
	}
	public static void partida(Jugadores j, int opc) {
		cas=0;
		char[][] tabSec = Posiciones.crearTab(opc,false);
		char[][] tabMos = Posiciones.crearTab(opc,true);
		boolean[][] tabDes = Posiciones.crearTabBoolean(opc);
		int numC=Posiciones.cal(opc);
		if(juego(tabSec,tabMos,numC,tabDes)==false) { 
			j.gan++;
			System.out.println("Ganaste!!!");
		}
		else
			j.per++;
	}
	private static boolean juego(char[][] tabSec, char[][] tabMos,int num,boolean[][]tabDes) {
		boolean exp=false;
		while(cas<num && !exp) {
			sc.nextLine();
			Posiciones.mostrarTab(tabMos);
			System.out.println("Escoja:\n-Despejar una casilla(D)\n-Marcar una bomba(M)");
			char a=sc.nextLine().toUpperCase().charAt(0);
			while(a!='D' && a!='M') {
				System.out.println("Valor introducido incorrecto, porfavor vuelva a introducir otro valor");
				System.out.println("Escoja:\n-Despejar una casilla(D)\n-Marcar una bomba(M)");
				a=sc.nextLine().toUpperCase().charAt(0);
			}
			if(a=='D') {
				exp=despejarCasilla(tabSec,tabMos,tabDes);
			}else {
				marcarBomb(tabMos);
			}
		}
		Posiciones.mostrarTab(tabSec);
		return exp;
	}
	private static boolean despejarCasilla(char[][] tabSec, char[][] tabMos, boolean[][] tabDes) {
		Posiciones p = new Posiciones();
		System.out.println("Porfavor indique que casilla quiere despejar: (introduce posicion 'x' e 'y')");
		p.x=sc.nextInt();
		p.y=sc.nextInt();
		while(tabMos[p.x][p.y]!='-') {
			System.out.println("Casilla indicada no valida o marcada");
			System.out.println("Porfavor indique que casilla quiere despejar: (introduce posicion 'x' e 'y')");
			p.x=sc.nextInt();
			p.y=sc.nextInt();
		}
		if(tabSec[p.x][p.y]=='0') {
			despejarCasilla0(tabSec,tabMos,tabDes,p);
			return false;
		}else if(tabSec[p.x][p.y]=='*') {
			despejarCasillaBom(tabSec,tabMos,p);
			return true;
		}else {
			despejarCasillaNum(tabSec,tabMos,p);
			return false;
		}
	}
	private static void despejarCasillaBom(char[][] tabSec, char[][] tabMos,Posiciones p) {
		tabMos[p.x][p.y]=tabSec[p.x][p.y];
		Posiciones.mostrarTab(tabMos);
		System.out.println("Perdiste!!!");
	}
	private static void despejarCasillaNum(char[][] tabSec, char[][] tabMos,Posiciones p) {
		tabMos[p.x][p.y]=tabSec[p.x][p.y];
		cas++;
	}
	private static void despejarCasilla0(char[][] tabSec, char[][] tabMos, boolean[][] tabDes,Posiciones p) {
		recursividad(tabSec,tabMos,tabDes,p);
	}
	private static void recursividad(char[][] tabSec, char[][] tabMos, boolean[][] tabDes, Posiciones p) {
		int x=p.x,y=p.y;
		if(p.x >=0 && p.y >=0 && p.x < tabSec.length && p.y < tabSec[0].length) {
			if(tabDes[p.x][p.y]==true) {
				return;
			}
			if (tabSec[p.x][p.y]!='0') {
				tabMos[p.x][p.y]=tabSec[p.x][p.y];
				cas++;
				return;
			}else {
				tabMos[p.x][p.y]=tabSec[p.x][p.y];
				cas++;
				tabDes[p.x][p.y]=true;
				for(int i=x-1;i<=x+1;i++) {
					for(int j=y-1;j<=y+1;j++) {
						p.x=i;p.y=j;
						recursividad(tabSec,tabMos,tabDes,p);
					}
				}	
			}
		}
	}
	public static void marcarBomb(char[][] tabMos) {
		Posiciones p = new Posiciones();
		System.out.println("Porfavor indique que casilla quiere marcar o desmarcar: (introduce posicion 'x' e 'y')");
		p.x=sc.nextInt();
		p.y=sc.nextInt();
		while(tabMos[p.x][p.y]!='?' && tabMos[p.x][p.y]!='-'){
			System.out.println("La casilla indicada ya ha sido despejada");
			System.out.println("Porfavor indique que casilla quiere marcar o desmarcar: (introduce posicion 'x' e 'y')");
			p.x=sc.nextInt();
			p.y=sc.nextInt();
		}
		if(tabMos[p.x][p.y]=='-') {
			tabMos[p.x][p.y]='?';
		}else {
			tabMos[p.x][p.y]='-';	
		}
	}
}
