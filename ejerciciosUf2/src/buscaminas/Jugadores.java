package buscaminas;

import java.util.Scanner;

public class Jugadores {
	
	static Scanner sc = new Scanner (System.in);
	
	public String nom;
	public int gan;
	public int per;

	public static boolean defJug(Jugadores j) {
		System.out.println("--------------------------------JUGADOR----------------------------------");
		System.out.println("Introduzca el nombre del jugador:");
		j.nom = sc.nextLine();
		System.out.println("-------------------------------------------------------------------------\n");
		return true;
	}
	public static void mostrarJug(Jugadores j) {
		System.out.println("--------------------------------JUGADOR----------------------------------");
		System.out.println("| Nombre: "+j.nom);
		System.out.println("| Ganadas: "+j.gan);
		System.out.println("| Perdidas: "+j.per);
		System.out.println("-------------------------------------------------------------------------\n");
	}
}

