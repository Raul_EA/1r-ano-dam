package buscaminas;

import java.util.Random;

public class Posiciones {

	static Random rd = new Random();

	static final int PRIN_C = 8;
	static final int PRIN_M = 10;
	static final int INT_C = 16;
	static final int INT_M = 40;
	static final int EXP_C1 = 16;
	static final int EXP_C2 = 30;
	static final int EXP_M = 99;
	static int per_1;
	static int per_2;
	static int min;
	
	int x;
	int y;
	
	public static void montarTabBoolean(boolean[][]b) {
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				b[i][j]=false;
			}
		}
	}
	public static boolean[][] crearTabBoolean(int opc) {
		boolean[][] b;
		switch (opc) {
		case 1:
			b = new boolean[PRIN_C][PRIN_C];
			montarTabBoolean(b);
			return b;
		case 2:
			b = new boolean[INT_C][INT_C];
			montarTabBoolean(b);
			return b;
		case 3:
			b = new boolean[EXP_C1][EXP_C2];
			montarTabBoolean(b);
			return b;
		case 4:
			b = new boolean[per_1][per_2];
			montarTabBoolean(b);
			return b;
		}
		return null;
	}
	public static int cal (int b) {
		int a;
		switch (b) {
		case 1:
			a=(PRIN_C*PRIN_C)-PRIN_M;
			return a;
		case 2:
			a=(INT_C*INT_C)-INT_M;
			return a;
		case 3:
			a=(EXP_C1*EXP_C2)-EXP_M;
			return a;
		case 4:
			a=(per_1*per_2)-min;
			return a;
		}
		return 0;
	}
	public static char[][] crearTab(int a,boolean c) {
		char[][] b;
		switch (a) {
		case 1:
			b = new char[PRIN_C][PRIN_C];
			if(!c) 
				ponerMin(b, PRIN_M);
			else
				montarTabM(b);
			return b;
		case 2:
			b = new char[INT_C][INT_C];
			if(!c) 
				ponerMin(b, INT_M);
			else
				montarTabM(b);
			return b;
		case 3:
			b = new char[EXP_C1][EXP_C2];
			if(!c) 
				ponerMin(b, EXP_M);
			else
				montarTabM(b);
			return b;
		case 4:
			b = new char[per_1][per_2];
			if(!c) 
				ponerMin(b, min);
			else
				montarTabM(b);
			return b;
		}
		return null;
	}
	public static void montarTabM(char[][]b) {
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				b[i][j]='-';
			}
		}
	}
	public static void ponerMin(char[][] b, int a) {
		int x;
		int y;
		int num = 0;
		boolean fun=false;
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				b[i][j] = '0';
			}
		}
		for (int i = 0; i < a; i++) {
			do {
				x = rd.nextInt(b.length);
				y = rd.nextInt(b[0].length);
			} while (b[x][y] == '*');
			b[x][y] = '*';
		}
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				for (int k = i-1; k <=i+1; k++) {
					for (int l = j-1; l <=j+1; l++) {
						if(k>=0 && k<b.length && l>=0 && l<b[0].length) {
							if (b[i][j] == '0' && b[k][l] == '*' ) {
								num++;
								fun=true;
							}
						}
					}
				}
				if(fun) {
					b[i][j] = devolverCHar(num);
					fun=false;
					num=0;
				}
			}
		}
	}
	public static char devolverCHar(int a) {
		return (char) (a+'0');
	}
	public static void mostrarTab(char[][]b) {
		for (int i = 0; i < b.length; i++) {
			System.out.println();
			for (int j = 0; j < b[0].length; j++) {
				System.out.print(b[i][j]+" ");
			}
		}
		System.out.println();
	}
}
