package recursividad;

import java.util.Scanner;

public class LegoPiramide {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(pir(sc.nextInt()));
		}
		sc.close();
	}
	public static int pir(int a) {
		if(a==2)return 1;
		return (a*a-((a-4)*(a-4)))/4+pir(a-2);
	}

}
