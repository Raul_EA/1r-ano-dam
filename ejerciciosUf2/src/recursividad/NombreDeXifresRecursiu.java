package recursividad;

import java.util.Scanner;

public class NombreDeXifresRecursiu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(xif(sc.nextInt()));
		}
		sc.close();
	}
	
	public static int xif(int a) {
		if(a/10==0)return 1;
		return (1+xif(a/10));
	}

}
