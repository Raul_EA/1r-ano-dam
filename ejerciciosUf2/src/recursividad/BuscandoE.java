package recursividad;

import java.util.Scanner;

public class BuscandoE {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(e(sc.nextInt()));
		}
		sc.close();
	}
	public static double e(int a) {
		if(a==0)return 1;
		return 1/fac(a) + e(a-1);
	}
	public static double fac(int b) {
		if(b==1)return 1;
		return b*fac(b-1);
	}
}
