package recursividad;

import java.util.Scanner;

public class AmbrosioTriangular {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(amb(sc.nextInt()));
		}
		sc.close();
	}
	public static int amb(int a) {
		if(a==1)return 1;
		return pis(a)+amb(a-1);
	}
	public static int pis (int a) {
		if(a==1)return 1;
		return a+pis(a-1);
	}
}
