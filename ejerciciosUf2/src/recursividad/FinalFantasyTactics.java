package recursividad;

import java.util.Scanner;

public class FinalFantasyTactics {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(bit(sc.nextInt(),1));
		}
		sc.close();
	}
	
	public static int bit(int a,int b) {
		if(b>a)return 0;
		return (1+bit(a-b,b+1));
	}

}