package recursividad;

import java.util.Scanner;
import java.lang.Math;

public class OrigenAjedrez {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(dob(sc.nextInt()));
		}
		sc.close();
	}
	public static long dob(int a) {
		if(a==1)return 1;
		return (long) (Math.pow(2,a-1) + dob(a-1));
	}

}
