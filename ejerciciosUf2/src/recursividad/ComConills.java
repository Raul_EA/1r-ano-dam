package recursividad;

import java.util.Scanner;

public class ComConills {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			System.out.println(fib(sc.nextInt()));
		}
		sc.close();
	}
	public static int fib(int a) {
		if(a==0)return 0;
		if(a==1)return 1;
		return fib(a-1)+fib(a-2);
	}

}
