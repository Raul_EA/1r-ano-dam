package tresEnRalla;

import java.util.Random;
import java.util.Scanner;

public class Funciones {
	static Scanner sc = new Scanner(System.in);
	static Random rd = new Random();
	static char[][] mat = new char[3][3];
	static boolean exit=false;
	public static void mosMenu() {
		System.out.println(
				"Tres en ralla:\n1. Mostrar Ajuda\n2. Definir Jugadores\n3. Jugar Partida\n4. Ver Jugadores\n0. Salir");
	}

	public static void ayuda() {
		System.out.println(
				"Gana el primero en poner tres marcas en linia dentro de la cuadricula. Estos deben ser de su mismo tipo, es decir, o tres cruces o tres circulos.\n");
	}

	public static Jugador definir() {
		Jugador j = new Jugador();
		System.out.println("Introduzca el nombre del jugador");
		j.nom = sc.nextLine();
		j.tipus = tipo();
		if (j.tipus == 'M' || j.tipus == 'm') {
			j.nivell = nivel();
		}
		System.out.println("Introduzca el numero de partidas ganadas");
		j.guanyades = sc.nextInt();
		sc.nextLine();
		return j;
	}

	public static int nivel() {
		System.out.println("Seleccione el nivel de la maquina (0-->random)(1-->listo)");
		int a = sc.nextInt();
		if (a != 1 && a != 0) {
			System.out.println("Valor introducido invalido");
			System.out.println();
			nivel();
		}
		return a;
	}

	public static char tipo() {
		System.out.println("Introduzca que tipo de jugador sera (H-->humano)(M-->maquina)");
		char f = sc.next().charAt(0);
		if (f != 'M' && f != 'm' && f != 'h' && f != 'H') {
			System.out.println("Valor introducido invalido");
			System.out.println();
			tipo();
		}
		return f;
	}

	public static void mostrarJ(Jugador j) {
		System.out.println("Nombre: " + j.nom);
		if (j.tipus == 'H' || j.tipus == 'h')
			System.out.println("Tipo: Humano");
		else
			System.out.println("Tipo: Maquina");
		if (j.tipus == 'M' || j.tipus == 'm') {
			if (j.nivell == 0)
				System.out.println("Nivel: Random");
			else
				System.out.println("Nivel: Inteligente");
		}
		System.out.println("Partidas ganadas: " + j.guanyades);
		System.out.println();
	}

	public static void juego(Jugador j1, Jugador j2) {
		// j1 --> O j2 --> X
		int tur = 1;
		boolean fun = true;
		limpiar();
		while (fun == true && tur <= 9 && exit==false) {
			ver();
			if (tur % 2 != 0) {
				System.out.println("Turno del jugador "+j1.nom+":");
				fun = turno(tur, j1);
			} else {
				System.out.println("Turno del jugador "+j2.nom+":");
				fun = turno(tur, j2);
			}
			tur++;
		}
	}

	public static boolean turno(int tur, Jugador j) {
		boolean fun = true;
		Posicion p = new Posicion();
		char sim;
		if (tur % 2 != 0) {
			sim = 'O';
		} else {
			sim = 'X';
		}
		p = posicion(sim, j);
		mat[p.x][p.y] = sim;
		fun = verificar();
		if(fun==false) {
			System.out.println("\nEl jugador "+j.nom+" ha ganado la partida.");
			j.guanyades++;
		}
		return fun;
	}

	public static boolean verificar() {
		boolean fun = true;
		if (fun == true)
			fun = verVerticales();
		if (fun == true)
			fun = verHorizontales();
		if (fun == true)
			fun = verDiagonales();
		return fun;
	}

	public static boolean verVerticales() {
		boolean fun = true;
		for (int i = 0; i < 3; i++) {
			if (mat[0][i] == mat[1][i] && mat[1][i] == mat[2][i] && mat[0][i] != '-' && mat[1][i] != '-'&& mat[2][i]!='-') {
				fun = false;
			}
		}
		return fun;
	}

	public static boolean verHorizontales() {
		boolean fun = true;
		for (int i = 0; i < 3; i++) {
			if (mat[i][0] == mat[i][1] && mat[i][1] == mat[i][2] && mat[i][0] != '-' && mat[i][1] != '-'&& mat[i][2] !='-') {
				fun = false;
			}
		}
		return fun;
	}

	public static boolean verDiagonales() {
		boolean fun = true;
		if (mat[0][0] == mat[1][1] && mat[1][1] == mat[2][2] && mat[0][0] != '-' && mat[1][1] != '-' && mat[2][2]!='-') {
			fun = false;
		}
		if (mat[0][2] == mat[1][1] && mat[1][1] == mat[2][0] && mat[0][2]!='-' && mat[1][1] != '-' && mat[2][0]!='-') {
			fun = false;
		}
		return fun;
	}

	public static Posicion posicion(char sim, Jugador j) {
		Posicion p = new Posicion();
		if (j.tipus == 'H' || j.tipus == 'h') {
			System.out.println("Introduzca las coordenadas 'x' y 'y' de donde quiere colocar su pieza (" + sim + ") o ponga dos veces 8 para dejar la partida");
			int x = sc.nextInt();
			int y = sc.nextInt();
			if(x==8 && y==8) {
				exit=true;
				System.out.println("Saliendo de la partida...");
			}else{
				if (x >= 0 && x <= 2 && y >= 0 && y <= 2) {
					if (mat[x][y] == '-') {
						p.x = x;
						p.y = y;
					} else {
						System.out.println("Esta casilla ya esta ocupada, escoja otra porfavor");
						p = posicion(sim, j);
					}
				} else {
					System.out.println("Esta casilla no existe, escoja otra porfavor");
					p = posicion(sim, j);
				}
			}
		} else {
			if (j.nivell == 0) { // random
				int x = rd.nextInt(3);
				int y = rd.nextInt(3);
				if (mat[x][y] == '-') {
					p.x = x;
					p.y = y;
				} else {
					p = posicion(sim, j);
				}
			} else { // inteligente
				
			}
		}
		return p;
	}
	public static void limpiar() {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				mat[i][j] = '-';
			}
		}
	}

	public static void ver() {
		for (int i = 0; i < mat.length; i++) {
			System.out.println();
			for (int j = 0; j < mat[0].length; j++) {
				System.out.print(mat[i][j] + " ");
			}
		}
		System.out.println("\n");
	}
}
