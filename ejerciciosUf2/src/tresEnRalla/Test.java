package tresEnRalla;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fun = true;
		Jugador j1 = new Jugador();
		Jugador j2 = new Jugador();
		boolean dec = false;
		while (fun) {
			Funciones.mosMenu();
			int a = sc.nextInt();
			switch (a) {
			case 1:
				Funciones.ayuda();
				break;
			case 2:
				System.out.println("Jugador 1:");
				j1=Funciones.definir();
				System.out.println("Jugador 2:");
				j2=Funciones.definir();
				dec=true;
				break;
			case 3:
				if (dec==true) {
					Funciones.juego(j1,j2);
				}else {
					System.out.println("Porfavor primero declare a los jugadores\n");
				}
				break;
			case 4:
				if (dec==true) {
					System.out.println("Jugador 1:");
					Funciones.mostrarJ(j1);
					System.out.println("Jugador 2:");
					Funciones.mostrarJ(j2);
				}else {
					System.out.println("Porfavor primero declare a los jugadores\n");
				}
				break;
			case 0:
				System.out.println("Saliendo...");
				sc.close();
				fun=false;
				break;
			}
		}
	}

}
