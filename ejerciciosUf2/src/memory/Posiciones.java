package memory;

import java.util.Random;
import java.util.Scanner;

public class Posiciones {
	public int x1;
	public int y1;
	public int x2;
	public int y2;
	
	public static Scanner sc = new Scanner(System.in);
	public static Random rd = new Random();
	
	public static void posRet(Posiciones p,char[][]tab,Jugador j,char[][]tabAc) {
		if(j.tip=='H') {
			humano(p,tab);
		}else {
			if(j.niv==0) {
				maq0(p,tab);
			}else {
				maq1(p,tab,tabAc);
			}
		}

	}
	private static void maq1(Posiciones p,char[][]tab,char[][]tabAc) {
		boolean flag=false;
		boolean flag2=false;
		boolean flag3=false;
		for(int i=0;i<Funciones.dim;i++) {
			for(int j=0;j<Funciones.dim;j++) {
				for(int k=0;k<Funciones.dim;k++) {
					for(int l=0;l<Funciones.dim;l++) {
						if(tabAc[i][j] == tabAc[k][l] && tabAc[i][j]!='?' && (i!=k || j!=l)) {
							if(tabAc[i][j]!=tab[i][j]){
								p.x1=i;
								p.y1=j;
								p.x2=k;
								p.y2=l;
								flag=true;
							}
						}
					}
				}
			}
		}
		if(!flag) {
			for(int i=0;i<Funciones.dim && !flag2;i++) {
				for(int j=0;j<Funciones.dim && !flag2;j++) {
					if(tabAc[i][j]=='?') {
						p.x1=i;
						p.y1=j;
						flag2=true;
					}
				}
			}
			for(int i=0;i<Funciones.dim && !flag3;i++) {
				for(int j=0;j<Funciones.dim && !flag3;j++) {
					if(tabAc[i][j]=='?' && (p.x1!=i || p.y1!=j)) {
						p.x2=i;
						p.y2=j;
						flag3=true;
					}
				}
			}
		if(flag3==false) {
			p.x2=rd.nextInt(Funciones.dim);
			p.y2=rd.nextInt(Funciones.dim);
			while(tab[p.x2][p.y2]!='?') {
				p.x2=rd.nextInt(Funciones.dim);
				p.y2=rd.nextInt(Funciones.dim);	
			}
		}
	}
	}
	private static void maq0(Posiciones p,char[][]tab) {
		p.x1=rd.nextInt(Funciones.dim);
		p.y1=rd.nextInt(Funciones.dim);
		while(tab[p.x1][p.y1]!='?') {
			p.x1=rd.nextInt(Funciones.dim);
			p.y1=rd.nextInt(Funciones.dim);	
		}
		p.x2=rd.nextInt(Funciones.dim);
		p.y2=rd.nextInt(Funciones.dim);
		while(tab[p.x2][p.y2]!='?') {
			p.x2=rd.nextInt(Funciones.dim);
			p.y2=rd.nextInt(Funciones.dim);	
		}
	}
	public static void humano(Posiciones p,char[][]tab) {
		System.out.println("Introduce las posiciones X y Y de la primera casilla:");
		p.x1=sc.nextInt();
		p.y1=sc.nextInt();
		while(tab[p.x1][p.y1]!='?') {
			System.out.println("Casilla ya emparejada");
			System.out.println("Introduce las posiciones X y Y de la primera casilla:");
			p.x1=sc.nextInt();
			p.y1=sc.nextInt();	
		}
		System.out.println("Introduce las posiciones X y Y de la segunda casilla:");
		p.x2=sc.nextInt();
		p.y2=sc.nextInt();
		while(tab[p.x2][p.y2]!='?') {
			System.out.println("Casilla ya emparejada");
			System.out.println("Introduce las posiciones X y Y de la segunda casilla:");
			p.x2=sc.nextInt();
			p.y2=sc.nextInt();	
		}
	}
	public static boolean verificar(char[][] tab, Posiciones p, char[][] sol,Jugador j) {
		if(sol[p.x1][p.y1]==sol[p.x2][p.y2]) {
			tab[p.x1][p.y1]=sol[p.x1][p.y1];
			tab[p.x2][p.y2]=sol[p.x2][p.y2];
			j.pun++;
			System.out.println("Acertaste!");
			return true;
		}else {
			System.out.println("Fallaste!");
			return false;
		}
	}
}
