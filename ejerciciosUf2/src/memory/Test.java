package memory;

public class Test {

	public static void main(String[] args) {
		final int a =1;
		final int b =2;
		int op;
		boolean def=false;
		Jugador j1 = new Jugador();
		Jugador j2 = new Jugador();
		do {
			op=Funciones.menu();
			switch (op){
			case 1:	
				Jugador.defJug(j1,a);
				Jugador.defJug(j2,b);
				def=true;
				break;
			case 2:
				if(def==true) {
					Funciones.jugar(j1,j2);
				} else {
					System.err.println("Defina primero los jugadores");
				}
				break;
			case 3:
				if(def==true) {
					Jugador.verJug(j1,a);
					Jugador.verJug(j2,b);
				}else {
					System.err.println("Defina primero los jugadores");
				}
				break;
			case 0:
				System.out.println("Saliendo...");
				break;
			default:
				System.out.println("Valor incorrecto");
				break;
			}
		}while(op!=0);
	}
}
