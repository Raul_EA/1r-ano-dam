package memory;

import java.util.Random;
import java.util.Scanner;

public class Funciones {
	
	public static Random rd = new Random();
	public static Scanner sc = new Scanner(System.in);
	static final int dim = 4;
	
	public static int menu() {	
		System.out.println("MEMORY");
		System.out.println("1-Definir los jugadores");
		System.out.println("2-Jugar partida");
		System.out.println("3-Ver jugadores");
		System.out.println("0-Salir");
		int op=sc.nextInt();
		return op;
	}

	public static void jugar(Jugador j1, Jugador j2) {
		char[][] sol=crearMat();
		j1.pun=0;
		j2.pun=0;
		switch(turnos(sol,j1,j2)){
			case -1:
				j1.gan++;
				j2.per++;
				System.out.println("El ganador es "+j1.nom+" con "+j1.pun+" puntos");
				break;
			case 0:
				System.out.println("Partida empatada, no hay ganador");
				System.out.println("Puntos "+j1.nom+": "+j1.pun);
				System.out.println("Puntos "+j2.nom+": "+j2.pun);
				break;
			case 1:
				j1.per++;
				j2.gan++;
				System.out.println("El ganador es "+j2.nom+" con "+j2.pun+" puntos");
				break;
		}
		
	}

	private static int turnos(char[][] sol,Jugador j1,Jugador j2) {
		char[][] tab = crearTab();
		char[][] tabAc = crearTab();
		Posiciones p = new Posiciones();
		int rell=0;
		boolean jug=true;
		while(rell!=8) {
			if(jug==true) {
				System.out.println("Turno de "+j1.nom);
				mosTab(tab);
				Posiciones.posRet(p,tab,j1,tabAc);
			}else {
				System.out.println("Turno de "+j2.nom);
				mosTab(tab);
				Posiciones.posRet(p,tab,j2,tabAc);
				
			}
			mosTabSin(tab,p,sol);
			if(j1.niv==1 || j2.niv==1)
				actTab(tabAc,p,sol);
			if(jug==true) {
				if(Posiciones.verificar(tab,p,sol,j1)) {
					rell++;
				}else {
					jug=false;
				}
			}else {
				if(Posiciones.verificar(tab,p,sol,j2)) {	
					rell++;
				}else {
					jug=true;
				}
			}
		}
		if(j1.pun>j2.pun) {
			return -1;
		}else if(j1.pun<j2.pun) {
			return 1;
		}else {
			return 0;
		}
	}
	public static void actTab (char[][] tabAc,Posiciones p,char[][] sol) {
		tabAc[p.x1][p.y1]=sol[p.x1][p.y1];
		tabAc[p.x2][p.y2]=sol[p.x2][p.y2];
	}
	private static void mosTabSin(char[][]tab,Posiciones p,char[][]sol) {
		for(int i=0;i<dim;i++) {
			for(int j=0;j<dim;j++) {
				if((p.x1==i && p.y1==j)) 
					System.out.print(sol[i][j]+" ");
				else if((p.x2==i && p.y2==j))
					System.out.print(sol[i][j]+" ");
				else
					System.out.print(tab[i][j]+" ");
			}
			System.out.println();
		}
	}

	private static char[][] crearTab() {
		char[][] sol = new char[dim][dim];
		for(int i=0;i<dim;i++) {
			for(int j=0;j<dim;j++) {
				sol[i][j]='?';
			}
		}
		return sol;
	}

	private static void mosTab(char[][]sol) {
		for(int i=0;i<dim;i++) {
			for(int j=0;j<dim;j++) {
				System.out.print(sol[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("");
	}

	private static char[][] crearMat() {
		char[] let= {'A','A','B','B','C','C','D','D','E','E','F','F','G','G','H','H'};
		char[][] sol = new char[dim][dim];
		int x;
		int y;
		for(int i=0;i<dim;i++) {
			for(int j=0;j<dim;j++) {
				sol[i][j]='-';
			}
		}
		for(int i=0;i<let.length;i++) {
			do {
				x=rd.nextInt(dim);
				y=rd.nextInt(dim);
			}while(sol[x][y]!='-');
			sol[x][y]=let[i];
		}
		return sol;
	}
	
}
