package memory;

import java.util.Scanner;

public class Jugador {
	public static Scanner sc = new Scanner (System.in);
	public String nom;
	public char tip; //'H'--> humà   'M' --> màquina
	public int niv;	// 0 --> bàsic(random) 1--> intel·ligent
	public int gan;
	public int per;
	public int pun;
	
	public static void defJug(Jugador j, int a) {
		System.out.println("--------------------------------JUGADOR "+a+"--------------------------------");
		System.out.println("Introduzca el nombre del jugador:");
		j.nom = sc.nextLine();
		System.out.println("Introduzca el tipo de jugador('H'--> humà   'M' --> màquina):");
		j.tip = sc.nextLine().toUpperCase().charAt(0);
		if(j.tip!='M' && j.tip!='H') {
			do {
				System.out.println("Valor introducido erroneo");
				System.out.println("Introduzca el tipo de jugador('H'--> humà   'M' --> màquina):");
				j.tip = sc.nextLine().toUpperCase().charAt(0);
			}while(j.tip!='M' && j.tip!='H');	
		}
		if(j.tip=='M') {
			System.out.println("Introduzca el nivel de la maquina(0 --> basica 1 --> intel·ligent):");
			j.niv = sc.nextInt();
			sc.nextLine();
			if(j.niv!=0 && j.niv!=1) {
				do {
					System.out.println("Valor introducido erroneo");
					System.out.println("Introduzca el nivel de la maquina(0 --> basica 1 --> intel·ligent):");
					j.niv = sc.nextInt();
					sc.nextLine();
				}while(j.niv!=0 && j.niv!=1);	
			}
		}
		System.out.println("-------------------------------------------------------------------------\n");
	}
	public static void verJug(Jugador j,int a) {
		System.out.println("--------------------------------JUGADOR "+a+"--------------------------------");
		System.out.println("| Nombre: "+j.nom);
		System.out.println("| Tipo: "+j.tip);
		if(j.tip=='M') {
			if(j.niv==0)
				System.out.println("| Nivel : Basico");
			else
				System.out.println("| Nivel : Inteligente");
		}
		System.out.println("| Ganadas: "+j.gan);
		System.out.println("| Perdidas: "+j.per);
		System.out.println("-------------------------------------------------------------------------\n");
	}
}
