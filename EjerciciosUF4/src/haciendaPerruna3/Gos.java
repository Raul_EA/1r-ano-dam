package haciendaPerruna3;

import java.util.List;
import java.util.Random;

public class Gos {

	public static Random rd = new Random();

	private static int comp;

	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private GosEstat estat;
	private List<Tresor> llistaTresors;
	private Tresor tresorActiu;
	private int power;

	// funcion para devolver el numero de perros existentes sin acceder a la
	// variable
	public static int quantitatGossos() {
		return comp;
	}

	// constructor default
	public Gos() {
		comp++;
		sexe = 'M';
		fills = 0;
		nom = "SenseNom";
		edat = 4;
		estat = GosEstat.VIU;
		power=0;
	}

	// constructor que copia
	public Gos(Gos g) {
		comp++;
		this.sexe = g.sexe;
		this.fills = g.fills;
		this.nom = g.nom;
		this.edat = g.edat;
	}

	// contructor con parametros
	public Gos(String c, int d, char a, int b) {
		this();
		sexe = a;
		fills = b;
		nom = c;
		edat = d;
	}

	// metodo que clona otro perro
	public void clonar(Gos g) {
		this.sexe = g.sexe;
		this.fills = g.fills;
		this.nom = g.nom;
		this.edat = g.edat;
	}
	
	public void afegirTresor(Tresor t) {
		
		if(this.tresorActiu!=null) {
			this.llistaTresors.add(tresorActiu);
			this.tresorActiu=t;
		}else {
			this.tresorActiu=t;
		}
			
	}
	
	public Tresor rescatarTresor() {
		Tresor a = this.tresorActiu;
		this.tresorActiu=this.llistaTresors.get(0);
		this.llistaTresors.remove(0);
		return a;
	}
	
	public int powerTresors() {
		int b=0;
		for(Tresor a:this.llistaTresors ) {
			b+=a.valor;
		}
		return b;
	}
	
	public void masValor(){
		this.llistaTresors.add(tresorActiu);
		this.tresorActiu=null;
		this.llistaTresors.sort(null);
		this.tresorActiu=this.llistaTresors.get(0);
		this.llistaTresors.remove(this.llistaTresors.get(0));
	}
	public int gosPower() {
		return this.power+this.powerTresors()+this.tresorActiu.valor;
	}
	public void renovar() {
		if(this.tresorActiu!=null && !this.llistaTresors.isEmpty()) {
			this.llistaTresors.add(tresorActiu);
			this.tresorActiu=this.llistaTresors.get(0);
			this.llistaTresors.remove(0);
		}
	}

	public int valorMenjar() {
		int b=0;
		for(Tresor a:this.llistaTresors) {
			if(a instanceof Comestible)
				b+=a.valor;
		}
		return b;
	}
	// metodo toString
	@Override
	public String toString() {
		String cadena = "Nom: " + nom + "\nEdat: " + edat + "\nSexe: " + sexe + "\nFills: " + fills+"\nPower: "+power;
		if (estat != null)
			cadena += "\nEstat: " + estat;
		else
			cadena += "\nEstado desconocido";
		cadena +="\n\nLlista de tresors:";
		for(Tresor a:this.llistaTresors ) {
			int i=1;
			cadena+="\nTRESOR "+i;
			if(a instanceof Os) {
				Os b=(Os)a;
				cadena+="\nClase: "+b.getClass()+b.toString();
			}else if(a instanceof Joguina) {
				Joguina b=(Joguina)a;
				cadena+="\nClase: "+b.getClass()+b.toString();
			}else if(a instanceof Llaminadura) {
				Llaminadura b=(Llaminadura)a;
				cadena+="\nClase: "+b.getClass()+b.toString();
			}
			i++;
		}
		return cadena;
	}

	// metodo borda de los perros
	public void borda() {
		System.out.println("Guau Guau");
	}

	// metodo alternativo de toString
	public void visualizar() {
		String cadena = "nom: " + nom + "\n" + "edat: " + edat + "\n" + "fills: " + fills + "\n" + "sexe: " + sexe
				+ "\n";
		if (estat != null)
			cadena += "Estat: " + estat + "\n";
		else
			cadena += "Estado desconocido\n";
		System.out.println(cadena);
	}
	public Gos aparellar(Gos parella) throws Exception{
		if(this.sexe=='F'&&parella.sexe=='M'||this.sexe=='M'&&parella.sexe=='F')
			return new Gos();
		else
			throw new SexeIgualException("Eroors, no es poden aparellar");
	}
	// gets de las variables de los perros
	public int getEdat() {
		return edat;
	}

	public int getFills() {
		return fills;
	}

	public String getNom() {
		return nom;
	}

	public char getSexe() {
		return sexe;
	}

	public GosEstat getEstat() {
		return estat;
	}

	public void setFills(int a) {
		fills = a;
	}

	public void setNom(String a) {
		nom = a;
	}

	public void setSexe(char a) {
		sexe = a;
	}

	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}

	public List<Tresor> getLlistaTresors() {
		return llistaTresors;
	}

	public void setLlistaTresors(List<Tresor> llistaTresors) {
		this.llistaTresors = llistaTresors;
	}

	public Tresor getTresorActiu() {
		return tresorActiu;
	}

	public void setTresorActiu(Tresor tresorActiu) {
		this.tresorActiu = tresorActiu;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}
	
}
