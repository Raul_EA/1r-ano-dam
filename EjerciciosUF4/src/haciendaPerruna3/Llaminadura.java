package haciendaPerruna3;

public class Llaminadura extends Tresor implements Comestible{
	
	private String sabor;
	
	public Llaminadura() {
		super(false,false,2,2);
		this.sabor="nuvol";
	}
	
	public Llaminadura(String sabor) {
		this();
		this.sabor=sabor;
	}
	
	public Llaminadura(String sabor,int v,boolean p,int d) {
		super(p,false,v,d);
		this.sabor=sabor;
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nSabor "+sabor;
	}
	
	@Override
	public void enterrar() {
		super.enterrar();
		if(this.valor>0) {
			this.valor--;
		}
	}
	
	@Override
	public void desenterrar() {
		super.desenterrar();
		if(this.valor>0) {
			this.valor--;
		}
	}
	
	public String assaborir() {
		return "Ummmm que bona aquesta llaminadura sabor" + this.sabor;
	}
	
	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	
	
}
