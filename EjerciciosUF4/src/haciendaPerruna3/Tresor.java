package haciendaPerruna3;

public abstract class Tresor implements Comparable<Object>{
	
	protected boolean prioritari;
	protected int valor;
	protected int duracio;
	protected boolean enterrat;
	
	public Tresor() {
		this.prioritari=false;
		this.enterrat=false;
		this.valor=1;
		this.duracio=1;
	}
	
	public Tresor(boolean p,boolean en,int v,int d) {
		this.prioritari=p;
		this.enterrat=en;
		this.valor=v;
		this.duracio=d;
	}
	
	@Override
	public String toString() {
		return "Tresor:\nPrioritari " + prioritari + "\nValor " + valor + "\nDuracio " + duracio + "\nEnterrat "
				+ enterrat;
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj)
			return true;
		if(obj==null)
			return false;
		if(getClass()!=obj.getClass())
			return false;
		if(this.valor==((Tresor)obj).valor) {
			return true;
		}else if(Math.abs(this.valor-((Tresor)obj).valor)<=2){
			if(this.valor>((Tresor)obj).valor && ((Tresor)obj).valor>=1)
				return true;
			if(this.valor<((Tresor)obj).valor && this.valor>=1)
				return true;
		}else if(this instanceof Os && ((Tresor)obj)instanceof Os) {
			if(this.valor>((Os)obj).valor && ((Os)obj).isSuperOs() && !((Os)this).isSuperOs())
				return true;
			if(this.valor<((Os)obj).valor && !((Os)obj).isSuperOs() && ((Os)this).isSuperOs())
				return true;
		}else if(this instanceof Comestible && ((Tresor)obj)instanceof Comestible) {
			if(this.valor>((Tresor)obj).valor && (Tresor)obj instanceof Llaminadura)
				if(((Llaminadura)obj).getSabor()=="nuvol")
					return true;
			if(this.valor<((Tresor)obj).valor && this instanceof Llaminadura)
				if(((Llaminadura)this).getSabor()=="nuvol")
					return true;
		}
		return false;
	}
	
	@Override
	public int compareTo(Object obj){
		Tresor a = (Tresor)obj;
		if(this.valor>a.valor) 
			return 1;
		else if(this.valor<a.valor)
			return -1;
		else if(this.isPrioritari() && !a.isPrioritari())
			return 1;
		else if(!this.isPrioritari() && a.isPrioritari())
			return -1;
		else if(this instanceof Comestible && !(a instanceof Comestible))
			return 1;
		else if(!(this instanceof Comestible) && a instanceof Comestible)
			return 1;
		else return 0;	
	}
	public void enterrar() {
		this.enterrat=true;
	}
	
	public void desenterrar() {
		this.enterrat=false;
	}
	
	public boolean isPrioritari() {
		return prioritari;
	}
	public void setPrioritari(boolean prioritari) {
		this.prioritari = prioritari;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int getDuracio() {
		return duracio;
	}
	public void setDuracio(int duracio) {
		this.duracio = duracio;
	}
	public boolean isEnterrat() {
		return enterrat;
	}
	public void setEnterrat(boolean enterrat) {
		this.enterrat = enterrat;
	}
	
}
