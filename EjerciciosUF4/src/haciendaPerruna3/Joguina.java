package haciendaPerruna3;

public class Joguina extends Tresor implements Intercanviable{
	
	private JoguinaEstat estat;
	
	public Joguina() {
		super(false,false,2,1000);
		this.estat=JoguinaEstat.NORMAL;
	}
	
	public Joguina(JoguinaEstat estat) {
		this();
		this.estat=estat;
		if(estat==JoguinaEstat.NOU) {
			this.valor=6;
			this.prioritari=true;
		}else
			this.valor=6;
	}
	public Joguina(JoguinaEstat estat,int v,boolean p,int d) {
		super(p,false,v,d);
		this.estat=estat;
	}
	@Override
	public boolean interCanviar(Tresor t) {
		if(t instanceof Intercanviable && t.valor>=(this.valor*75)/100)
			return true;
		return false;
	}
	@Override
	public String toString() {
		return super.toString()+"\nEstado "+estat;
	}
	@Override
	public void enterrar() {
		super.enterrar();
		if(this.estat!=JoguinaEstat.NOU && this.valor>0) {
			this.valor--;
		}
	}
	@Override
	public void desenterrar() {
		super.desenterrar();
		if(this.valor>0) {
			this.valor--;
		}
	}
	public JoguinaEstat getEstat() {
		return estat;
	}

	public void setEstat(JoguinaEstat estat) {
		this.estat = estat;
	}
	
}
