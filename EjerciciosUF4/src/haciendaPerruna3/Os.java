package haciendaPerruna3;

public class Os extends Tresor implements Comestible,Intercanviable{
	
	private boolean superOs;
	
	public Os() {
		super();
		this.duracio=4;
		this.superOs=false;
		this.prioritari=false;
		this.valor=4;
	}
	
	public Os(boolean s) {
		this();
		if(s) {
			this.valor=8;
			this.prioritari=true;
			this.duracio=6;
		}
	}
	
	public Os(int d,boolean s,boolean p,int v) {
		this();
		this.duracio=d;
		this.superOs=s;
		this.prioritari=p;
		this.valor=v;
	}
	
	public boolean isSuperOs() {
		return superOs;
	}

	public void setSuperOs(boolean superOs) {
		this.superOs = superOs;
	}
	@Override
	public boolean interCanviar(Tresor t) {
		if(t instanceof Os && ((Os)t).isSuperOs())
			return true;
		return false;
	}
	@Override
	public String toString() {
		return super.toString()+"\nSuper Os "+superOs;
	}
	@Override
	public void enterrar() {
		super.enterrar();
		if(!this.superOs) {
			if(this.duracio>0)
				this.duracio--;
			if(this.valor>0)
				this.valor--;
		}
	}
	@Override
	public void desenterrar() {
		super.desenterrar();
		if(!this.superOs) {
			if(this.valor>0)
				this.valor--;
		}
	}
}
