package haciendaPerruna;

public class Raza {
	
	private String nomRaza;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Raza(String nom, GosMida gt, int t) {
		this(nom,gt);
		this.tempsVida = t;
	}
	public Raza(String nom, GosMida gt) {
		this.nomRaza=nom;
		this.mida = gt;
		tempsVida=10;
		dominant=false;
	}
	@Override
	public String toString() {
		return "Nombre raza: " + nomRaza +"\n"+"Mida: " + mida +"\nTempsVida: " + tempsVida + "\nDominante: " + dominant;
	}
	public String getNomRaza() {
		return nomRaza;
	}
	public void setNomRaza(String nomRaza) {
		this.nomRaza = nomRaza;
	}
	public GosMida getMida() {
		return mida;
	}
	public void setMida(GosMida mida) {
		this.mida = mida;
	}
	public int getTempsVida() {
		return tempsVida;
	}
	public void setTempsVida(int tempsVida) {
		this.tempsVida = tempsVida;
	}
	public boolean isDominant() {
		return dominant;
	}
	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}
	
}
