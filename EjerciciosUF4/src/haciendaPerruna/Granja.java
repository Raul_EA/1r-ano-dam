package haciendaPerruna;

import java.util.ArrayList;
import java.util.Random;

public class Granja {
	
	static Random rd = new Random();
	
	private ArrayList<Gos> gossos;
	private int numGossos;
	private int topGossos;
	
	public Granja() {
		gossos=new ArrayList<>();
		topGossos=100;
		numGossos=0;
	}
	
	public Granja(int top) {
		this();
		if(top<1 && top>100) {
			top=100;
		}
		topGossos=top;
	}

	public int getNumGossos() {
		return numGossos;
	}

	public int getTopGossos() {
		return topGossos;
	}
	
	public int afegir(Gos gos) {
		if(numGossos<topGossos) {
			gossos.add(gos);
			numGossos++;
		}else {
			return -1;
		}
		return numGossos;
	}
	@Override
	public String toString (){
		String cadena="\nMaxim gossos: "+topGossos+"\nNumero de gossos: "+numGossos;
		for(int i=0;i<gossos.size();i++) {
			cadena+="\n\nGOS NUMERO "+(i+1)+":\n"+gossos.get(i);
		}
		return cadena;
	}
	public void visualizar (){
		String cadena="Maxim gossos: "+topGossos+"\nNumero de gossos: "+numGossos;
		for(int i=0;i<gossos.size();i++) {
			cadena+="\n\nGOS NUMERO "+(i+1)+":\n"+gossos.get(i);
		}
		System.out.println(cadena);
	}
	public void visualizarVius (){
		String cadena="Gossos vius:";
		for(int i=0;i<gossos.size();i++) {
			if(gossos.get(i).getEstat()==GosEstat.VIU)
			cadena+="\nGOS NUMERO "+(i+1)+":\n"+gossos.get(i);
		}
		System.out.println(cadena);
	}
	public void generarGranja(int totalGossos) {
		String[] noms = {"Rocky","Toby","Teo","Quino","Leo","Max","Jack","Bruno","Coco","Nilo","Lucas","Zeus","Rey","Chispa","Pancho"};
		for(int i=0;i<totalGossos;i++) {
			afegir(new Gos(noms[rd.nextInt(noms.length)],rd.nextInt(0,10),(rd.nextBoolean()?'M':'F'),rd.nextInt(0,4)));
		}
	}
	public Gos obtenirGos(int a) {
		if(gossos.size()<a) {
			return null;
		}else {
			return gossos.get(a-1);
		}
	}
}
