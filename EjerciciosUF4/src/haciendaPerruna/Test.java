package haciendaPerruna;

import java.util.Random;
import java.util.Scanner;

public class Test {

	public static Scanner sc = new Scanner(System.in);
	public static Random rd = new Random();
	
	public static void main(String[] args) {
		Raza bulldog = new Raza("Bulldog",GosMida.MITJA);
		Gos a = new Gos("Antonio",3,'F',0);
		Gos b = new Gos("Pepe",4,'M',0);
		a.setRaza(bulldog);
		b.setRaza(bulldog);
		Granja Uriel = new Granja();
		Uriel.afegir(b);
		Uriel.afegir(a);
		Uriel.afegir(Uriel.obtenirGos(1).aparellar(Uriel.obtenirGos(2)));
		System.out.println(Uriel);
	}
}
