package haciendaPerruna;

import java.util.Random;

public class Gos {

	public static Random rd = new Random();

	private static int comp;

	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private Raza raza;
	private GosEstat estat;

	// funcion para devolver el numero de perros existentes sin acceder a la
	// variable
	public static int quantitatGossos() {
		return comp;
	}

	// constructor default
	public Gos() {
		comp++;
		sexe = 'M';
		fills = 0;
		nom = "SenseNom";
		edat = 4;
		estat = GosEstat.VIU;
	}

	// constructor con raza
	public Gos(Raza raza) {
		this();
		this.raza = raza;
	}

	// contructor con parametros
	public Gos(String c, int d, char a, int b) {
		this();
		sexe = a;
		fills = b;
		nom = c;
		edat = d;
	}

	// constructor para los hijos
	public Gos(String c, char a, Raza raza) {
		this();
		sexe = a;
		nom = c;
		edat = 0;
		this.raza = raza;
	}

	// constructor que copia
	public Gos(Gos g) {
		comp++;
		this.sexe = g.sexe;
		this.fills = g.fills;
		this.nom = g.nom;
		this.edat = g.edat;
	}

	// metodo que clona otro perro
	public void clonar(Gos g) {
		this.sexe = g.sexe;
		this.fills = g.fills;
		this.nom = g.nom;
		this.edat = g.edat;
	}

	// metodo toString
	@Override
	public String toString() {
		String cadena = "Nom: " + nom + "\nEdat: " + edat + "\nSexe: " + sexe + "\nFills: " + fills;
		if (estat != null)
			cadena += "\nEstat: " + estat;
		else
			cadena += "\nEstado desconocido";
		if (raza != null)
			cadena += "\n" + raza;
		else
			cadena += "\nRaza desconocida";
		return cadena;
	}

	// gets de las variables de los perros
	public int getEdat() {
		return edat;
	}

	public int getFills() {
		return fills;
	}

	public String getNom() {
		return nom;
	}

	public char getSexe() {
		return sexe;
	}

	public Raza getRaza() {
		return raza;
	}

	public GosEstat getEstat() {
		return estat;
	}

	// sets de las variables de los perros
	public void setEdat(int a) {
		if (raza != null) {
			if (a > raza.getTempsVida()) {
				estat = GosEstat.MORT;
			}
		} else {
			if (edat <= 10) {
				estat = GosEstat.MORT;
			}
		}
		edat = a;
	}

	public void setFills(int a) {
		fills = a;
	}

	public void setNom(String a) {
		nom = a;
	}

	public void setSexe(char a) {
		sexe = a;
	}

	public void setRaza(Raza raza) {
		this.raza = raza;
	}

	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}

	// metodo borda de los perros
	public void borda() {
		System.out.println("Guau Guau");
	}

	// metodo alternativo de toString
	public void visualizar() {
		String cadena = "nom: " + nom + "\n" + "edat: " + edat + "\n" + "fills: " + fills + "\n" + "sexe: " + sexe
				+ "\n";
		if (estat != null)
			cadena += "Estat: " + estat + "\n";
		else
			cadena += "Estado desconocido\n";
		if (raza != null)
			cadena += raza + "\n";
		else
			cadena += "Raza desconocida\n";
		System.out.println(cadena);
	}

	public Gos aparellar(Gos g) {
		if (g == null) {
			return null;
		} else {
			if (g.edat >= 2 && g.edat <= 10 && this.edat >= 2 && this.edat <= 10) {
				if (g.sexe != this.sexe) {
					if (g.estat == GosEstat.VIU && this.estat == GosEstat.VIU) {
						if (this.sexe == 'F') {
							if (this.fills <= 3) {
								switch (g.raza.getMida()) {
								case PETIT: {
									if (this.raza.getMida() == GosMida.PETIT) {
										if (g.raza.isDominant() && !this.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', g.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', this.raza);
										}
									}
									if (this.raza.getMida() == GosMida.MITJA) {
										if (g.raza.isDominant() && !this.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', g.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', this.raza);
										}
									}
									if (this.raza.getMida() == GosMida.GRAN) {
										if (g.raza.isDominant() && !this.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', g.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', this.raza);
										}
									}
									break;
								}
								case MITJA: {
									if (this.raza.getMida() == GosMida.MITJA) {
										if (g.raza.isDominant() && !this.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', g.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', this.raza);
										}
									}
									if (this.raza.getMida() == GosMida.GRAN) {
										if (g.raza.isDominant() && !this.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', g.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', this.raza);
										}
									}
									break;
								}
								case GRAN: {
									if (this.raza.getMida() == GosMida.GRAN) {
										if (g.raza.isDominant() && !this.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', g.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + g.nom : "Filla de " + this.nom,
													sex ? 'M' : 'F', this.raza);
										}
									}
									break;
								}
								}
							}
						} else {
							if (g.fills <= 3) {
								switch (this.raza.getMida()) {
								case PETIT: {
									if (g.raza.getMida() == GosMida.PETIT) {
										if (this.raza.isDominant() && !g.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', this.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', g.raza);
										}
									}
									if (g.raza.getMida() == GosMida.MITJA) {
										if (this.raza.isDominant() && !g.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " +this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', this.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', g.raza);
										}
									}
									if (g.raza.getMida() == GosMida.GRAN) {
										if (this.raza.isDominant() && !g.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', this.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', g.raza);
										}
									}
									break;
								}
								case MITJA: {
									if (g.raza.getMida() == GosMida.MITJA) {
										if (this.raza.isDominant() && !g.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', this.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', g.raza);
										}
									}
									if (g.raza.getMida() == GosMida.GRAN) {
										if (this.raza.isDominant() && !g.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', this.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', g.raza);
										}
									}
									break;
								}
								case GRAN: {
									if (g.raza.getMida() == GosMida.GRAN) {
										if (this.raza.isDominant() && !g.raza.isDominant()) {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', this.raza);
										} else {
											boolean sex = rd.nextBoolean();
											this.fills++;
											g.fills++;
											return new Gos(sex ? "Fill de " + this.nom : "Filla de " + g.nom,
													sex ? 'M' : 'F', g.raza);
										}
									}
									break;
								}
								}
							}
						}
					}
				}
			}
			return null;
		}
	}
}
