package haciendaPerruna2;

import java.util.Random;

public abstract class Animal {

	public static Random rd = new Random();

	private static int numAnimals;

	private int edat;
	private String nom;
	private int fills;
	private AnimalSexe sexe;
	private AnimalEstat estat;

	public abstract void so();

	public abstract Animal aparellar(Animal nuvi);

	public abstract Animal saludar(Animal altre);

	public Animal() {
		edat = 0;
		nom = "Sense definir";
		fills = 0;
		sexe = rd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F;
		estat = AnimalEstat.VIU;
		numAnimals++;
	}

	public Animal(int edat, String nom, int fills, AnimalSexe sexe) {
		this();
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
	}

	public Animal(String nom) {
		this();
		this.nom = nom;
	}

	public void clonar(Animal a) {
		this.setEdat(a.getEdat());
		this.setEstat(a.getEstat());
		this.setFills(a.getFills());
		this.setNom(a.getNom());
		this.setSexe(a.getSexe());
	}

	@Override
	public String toString() {
		String cadena = "Nom: " + this.nom + "\n";
		cadena += "Edat: " + this.edat + "\n";
		cadena += "Fills: " + this.fills + "\n";
		cadena += "Sexe: " + this.sexe + "\n";
		cadena += "Estat: " + this.estat;
		return cadena;
	}

	public void visualizar() {
		String cadena = "Nom: " + this.nom + "\n";
		cadena += "Edat: " + this.edat + "\n";
		cadena += "Fills: " + this.fills + "\n";
		cadena += "Sexe: " + this.sexe + "\n";
		cadena += "Estat: " + this.estat;
		System.out.println(cadena);
	}

	public void incEdat() {
		if (this.estat == AnimalEstat.VIU)
			this.edat++;
	}

	public static int getNumAnimals() {
		return numAnimals;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public AnimalSexe getSexe() {
		return sexe;
	}

	public void setSexe(AnimalSexe sexe) {
		this.sexe = sexe;
	}

	public AnimalEstat getEstat() {
		return estat;
	}

	public void setEstat(AnimalEstat estat) {
		this.estat = estat;
	}

}
