package haciendaPerruna2;

import java.util.Random;

public class Gos extends Animal {

	public static Random rd = new Random();

	private Raza raza;
	
	public Raza getRaza() {
		return raza;
	}

	public void setRaza(Raza raza) {
		this.raza = raza;
	}

	public Gos() {
		super();
		raza = null;
	}

	// constructor con raza
	public Gos(Raza raza) {
		this();
		this.raza = raza;
	}

	// contructor con parametros
	public Gos(String c, int d, AnimalSexe a, int b) {
		super(d, c, b, a);
		this.raza = null;
	}

	// metodo toString
	@Override
	public String toString() {
		String cadena = super.toString();
		if (raza != null)
			cadena += "\nRaza: " + raza;
		else
			cadena += "\nRaza desconocida";
		return cadena;
	}

	@Override
	public Animal aparellar(Animal nuvi) {
		if (!(this instanceof Gos) || !(nuvi instanceof Gos))
			return null;
		if (this.getSexe() == nuvi.getSexe() || this.getEstat() == AnimalEstat.MORT
				|| nuvi.getEstat() == AnimalEstat.MORT)
			return null;
		this.setFills(this.getFills() + 1);
		nuvi.setFills(nuvi.getFills() + 1);
		Animal fill = new Gos("Fill de " + this.getNom(), 0, AnimalSexe.M, 0);
		((Gos) fill).setRaza(this.getRaza());
		return fill;
	}

	// metodo borda de los perros
	@Override
	public void so() {
		System.out.println("Guau Guau");
	}

	public void clonar(Gos a) {
		super.clonar(a);
		this.setRaza(a.getRaza());
	}

	@Override
	public Animal saludar(Animal g) {
		Animal mort = null;

		if (g instanceof Ganso) {
			if (((Ganso) g).getTipus() == GansoTipus.AGRESSIU && this.getSexe() == AnimalSexe.F) {
				this.setEstat(AnimalEstat.MORT);
				mort = this;
			}

			if (((Ganso) g).getTipus() == GansoTipus.DOMESTIC) {
				g.setEstat(AnimalEstat.MORT);
				mort = g;
			}

			if (((Ganso) g).getTipus() == GansoTipus.DESCONEGUT && this.getSexe() == AnimalSexe.F) {
				g.setEstat(AnimalEstat.MORT);
				mort = g;
			}
		}

		return mort;
	}

	// metodo alternativo de toString
	@Override
	public void visualizar() {
		String cadena;
		super.visualizar();
		if (raza != null)
			cadena = "\nRaza: " + raza;
		else
			cadena = "\nRaza desconocida";
		System.out.println(cadena);
	}

	@Override
	public void incEdat() {
		super.incEdat();
		if (raza != null)
			if (raza.getTempsVida() <= this.getEdat())
				this.setEstat(AnimalEstat.MORT);
	}
}
