package haciendaPerruna2;

import java.util.ArrayList;
import java.util.Random;

public class Granja {

	static Random rd = new Random();

	private ArrayList<Animal> animals;
	private int numAnimals;
	private int topAnimals;

	public Granja() {
		animals = new ArrayList<>();
		topAnimals = 100;
		numAnimals = 0;
	}

	public Granja(int top) {
		this();
		if (top < 1 && top > 100) {
			top = 100;
		}
		topAnimals = top;
	}

	public int getNumGossos() {
		return numAnimals;
	}

	public int getTopGossos() {
		return topAnimals;
	}

	public int afegir(Animal gos) {
		if (numAnimals < topAnimals) {
			animals.add(gos);
			numAnimals++;
		} else {
			return -1;
		}
		return numAnimals;
	}

	@Override
	public String toString() {
		String cadena = "\nMaxim gossos: " + topAnimals + "\nNumero de gossos: " + numAnimals;
		for (int i = 0; i < animals.size(); i++) {
			cadena += "\n\nGOS NUMERO " + (i + 1) + ":\n" + animals.get(i);
		}
		return cadena;
	}

	public void visualizar() {
		String cadena = "Maxim gossos: " + topAnimals + "\nNumero de gossos: " + numAnimals;
		for (int i = 0; i < animals.size(); i++) {
			cadena += "\n\nGOS NUMERO " + (i + 1) + ":\n" + animals.get(i);
		}
		System.out.println(cadena);
	}

	public void visualizarVius() {
		String cadena = "Gossos vius:";
		for (int i = 0; i < animals.size(); i++) {
			if (animals.get(i).getEstat() == AnimalEstat.VIU)
				cadena += "\nGOS NUMERO " + (i + 1) + ":\n" + animals.get(i);
		}
		System.out.println(cadena);
	}

	public void generarGranja(int totalAnimals) {
		String[] noms = { "Rocky", "Toby", "Teo", "Quino", "Leo", "Max", "Jack", "Bruno", "Coco", "Nilo", "Lucas",
				"Zeus", "Rey", "Chispa", "Pancho", "Eric" };
		for (int i = 0; i < totalAnimals; i++) {
			afegir(rd.nextBoolean()
					? new Gos(noms[rd.nextInt(noms.length)], rd.nextInt(0, 10),
							(rd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F), rd.nextInt(0, 4))
					: new Ganso(noms[rd.nextInt(noms.length)], rd.nextInt(0, 10), GansoTipus.DESCONEGUT,
							(rd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F)));
		}
	}

	public Animal obtenirAnimal(int a) {
		if (animals.size() < a) {
			return null;
		} else {
			return animals.get(a - 1);
		}
	}
}
