package haciendaPerruna2;

public class Ganso extends Animal {

	private static int edatTope;

	private GansoTipus tipus;

	Ganso() {
		super();
		edatTope = 0;
		tipus = GansoTipus.DESCONEGUT;
	}

	Ganso(String nom, int edat, GansoTipus tipus, AnimalSexe sexe) {
		super(edat, nom, 0, sexe);
		this.tipus = tipus;
	}

	Ganso(String n) {
		super(n);
		this.tipus = GansoTipus.DESCONEGUT;
	}

	Ganso(Ganso g) {
		this();
		clonar(g);
	}

	@Override
	public String toString() {
		String cadena = super.toString();
		cadena += "\nTipus: " + this.tipus;
		return cadena;
	}

	@Override
	public Animal aparellar(Animal nuvi) {
		if (!(this instanceof Ganso) || !(nuvi instanceof Ganso))
			return null;
		if (this.getSexe() == nuvi.getSexe() || this.getEstat() == AnimalEstat.MORT
				|| nuvi.getEstat() == AnimalEstat.MORT)
			return null;

		Animal fill = new Ganso();
		((Ganso) fill).setTipus(this.getTipus());
		return fill;
	}

	// metodo borda de los perros
	@Override
	public void so() {
		System.out.println("Grazna grazna");
	}

	public void clonar(Ganso a) {
		super.clonar(a);
		this.setTipus(a.getTipus());
	}

	// metodo alternativo de toString
	@Override
	public void visualizar() {
		super.visualizar();
		String cadena = "\nTipus: " + this.getTipus();
		System.out.println(cadena);
	}

	@Override
	public void incEdat() {
		super.incEdat();
		if (this.getTipus() == GansoTipus.AGRESSIU) {
			if (getEdatTope() - 1 == this.getEdat())
				this.setEstat(AnimalEstat.MORT);
		} else if (this.getTipus() == GansoTipus.DOMESTIC) {
			if (getEdatTope() + 1 == this.getEdat())
				this.setEstat(AnimalEstat.MORT);
		} else if (getEdatTope() == this.getEdat())
			this.setEstat(AnimalEstat.MORT);
	}

	@Override
	public Animal saludar(Animal a) {
		Animal mort = null;

		if (a instanceof Gos) {
			Gos g = (Gos) a;
			if (this.getTipus() == GansoTipus.AGRESSIU && g.getEdat() < 2) {
				g.setEstat(AnimalEstat.MORT);
				mort = g;
			}

			if (this.getTipus() != GansoTipus.AGRESSIU && g.getSexe() == AnimalSexe.F) {
				this.setEstat(AnimalEstat.MORT);
				mort = this;
			}
		}

		return mort;
	}

	public GansoTipus getTipus() {
		return tipus;
	}

	public void setTipus(GansoTipus tipus) {
		this.tipus = tipus;
	}

	public static int getEdatTope() {
		return edatTope;
	}

	public static void setEdatTope(int edatTope) {
		Ganso.edatTope = edatTope;
	}

}
