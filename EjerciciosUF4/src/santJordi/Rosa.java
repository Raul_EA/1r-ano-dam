package santJordi;

public class Rosa extends Regal{
	
	Colores color;
	Calidad calidad;
	
	public Rosa() {
		super();
		color=Colores.ROJO;
		calidad=Calidad.MEDIA;
	}
	
	public Rosa(String color,String calidad) {
		this();
		if(this.comprovarColor(color)!=null)
			this.color=this.comprovarColor(color);
		if(this.comprovarCalidad(calidad)!=null)
			this.calidad=this.comprovarCalidad(calidad);
		preuFinal();
	}
	
	public Rosa(int stock, float precio,String color,String calidad) {
		super(precio,stock);
		
		if(this.comprovarColor(color)!=null)
			this.color=this.comprovarColor(color);
		else 
			this.color=Colores.ROJO;
		
		if(this.comprovarCalidad(calidad)!=null)
			this.calidad=this.comprovarCalidad(calidad);
		else 
			this.calidad=Calidad.MEDIA;
			
		preuFinal();
	}
	
	public void preuFinal() {
		if(this.calidad==Calidad.ALTA)
			this.setPreuDeVenta(preuDeVenta+5);
		else if(this.calidad==Calidad.BAJA)
			this.setPreuDeVenta(preuDeVenta-5);
		
		if(this.color==Colores.BLANCO)
			this.setPreuDeVenta(preuDeVenta+2);
		else if(this.color==Colores.AZUL)
			this.setPreuDeVenta(preuDeVenta+5);
		else if(this.color==Colores.AMARILLO)
			this.setPreuDeVenta(preuDeVenta+2);
		else if(this.color==Colores.LILA)
			this.setPreuDeVenta(preuDeVenta+5);
	}
	
	private Colores comprovarColor(String c) {
		for(Colores i : Colores.values()) {
			if(c.equals(i)) {
				return i;
			}
		}
		return null;
	}
	
	private Calidad comprovarCalidad(String c){
		for(Calidad i : Calidad.values()) {
			if(c.equals(i)) {
				return i;
			}
		}
		return null;
	}
	public Colores getColor() {
		return color;
	}

	public void setColor(Colores color) {
		this.color = color;
	}

	public Calidad getCalidad() {
		return calidad;
	}

	public void setCalidad(Calidad calidad) {
		this.calidad = calidad;
	}
}
