package santJordi;

public class Libro extends Regal{
	
	String titulo;
	Encuadernado enc;
	String genero;
	
	public Libro() {
		super();
		this.titulo="Sin titulo";
		this.enc=Encuadernado.RUSTICA;
		this.genero="No indentificado";
	}
	
	public Libro(int preu,String titulo) {
		this();
		if(this.comprovarPreuBase(preuDeVenta))
			this.preuDeVenta=preu;
		this.titulo=titulo;
	}
	
	public Libro(float preuDeVenta,int stock,String libro,Encuadernado enc, String genero) {
		super(preuDeVenta,stock);
		this.titulo="Sin titulo";
		this.enc=Encuadernado.RUSTICA;
		this.genero="No indentificado";
	}
	
	public void preuFinal(){
		
	}
}
