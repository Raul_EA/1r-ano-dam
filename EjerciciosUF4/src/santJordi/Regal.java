package santJordi;

public abstract class Regal {
	
	static final int PREUBM=10;
	
	float preuDeVenta;
	int stock;
	
	public abstract void preuFinal();
	
	public Regal(){
		preuDeVenta=10;
		stock=100;
	}
	
	public Regal(float preuDeVenta,int stock){
		this();
		if(this.comprovarPreuBase(preuDeVenta))
			this.preuDeVenta=preuDeVenta;
		if(stock>0)
			this.stock=stock;
	}
	
	boolean comprovarPreuBase(float preu) {
		if(preu>0 && preu<PREUBM)
			return true;
		return false;
	}

	public float getPreuDeVenta() {
		return preuDeVenta;
	}
	public void setPreuDeVenta(float preuDeVenta) {
		if(preuDeVenta>=0)
			this.preuDeVenta = preuDeVenta;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		if(stock>=0)
			this.stock = stock;
	}
}
