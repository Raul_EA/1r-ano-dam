package mokepons;

public class TrioDif<T,U,V> {
    private T primer;
    private U segon;
    private V tercer;
    
    public TrioDif(T a, U b, V c) {
    	primer=a;
    	segon=b;
    	tercer=c;
    }
    
    public T getPrimer() {
        return primer;
    }
    public void setPrimer(T primer) {
        this.primer = primer;
    }
    public U getSegon() {
        return segon;
    }
    public void setSegon(U segon) {
        this.segon = segon;
    }
    public V getTercer() {
        return tercer;
    }
    public void setTercer(V tercer) {
        this.tercer = tercer;
    }
    
}
