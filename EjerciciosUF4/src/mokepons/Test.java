package mokepons;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mokepons.objectes.*;

public class Test {

	public static ScannerArreglat sc = new ScannerArreglat();
	public static Random rd = new Random();

	public static void main(String[] args) {
		ObjecteFactory fab = new ObjecteFactory();
		List<Objecte> obj = new ArrayList<Objecte>();
		for (int i = 0; i < 10; i++) {
			int a = rd.nextInt(1, 9);
			switch (a) {
			case 1:
				obj.add(fab.crearObjecte("POCIO"));
				break;
			case 2:
				obj.add(fab.crearObjecte("SUPERPOCIO"));
				break;
			case 3:
				obj.add(fab.crearObjecte("HIPERPOCIO"));
				break;
			case 4:
				obj.add(fab.crearObjecte("REVIURE"));
				break;
			case 5:
				obj.add(fab.crearObjecte("ARMA GRAN"));
				break;
			case 6:
				obj.add(fab.crearObjecte("ARMA PETITA"));
				break;
			case 7:
				obj.add(fab.crearObjecte("ARMADURA GRAN"));
				break;
			case 8:
				obj.add(fab.crearObjecte("ARMADURA PETITA"));
				break;
			}
		}
		System.out.println(obj);
	}

	public static void nomDeTots(List<? extends Mokepon> a) {
		for (Mokepon b : a) {
			System.out.println(b.getNom());
		}
	}
}
