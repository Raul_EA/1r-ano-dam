package mokepons.objectes;

public class ObjecteFactory {
    public Objecte crearObjecte(String tipus)
    {
        if (tipus == null || tipus.isEmpty())
            return null;
        switch (tipus) {
        case "POCIO":
            return new Pocio("Pocio",50);  //les pocions per defecte curen 50 de vida
        case "SUPERPOCIO":
            return new Pocio("Superpocio",100);
        case "HIPERPOCIO":
            return new Pocio("Hiperpocio",200);
        case "REVIURE":
        	return new Reviure("Reviure");
        case "ARMA GRAN":
        	return new Arma("Arma gran",100);
        case "ARMA PETITA":
        	return new Arma("Arma petita",20);
        case "ARMADURA GRAN":
        	return new Armadura("Armadura gran",100);
        case "ARMADURA PETITA":
        	return new Armadura("Armadura petita",20);
        default:
            throw new IllegalArgumentException("Tipus d'objecte desconegut "+tipus);
        }
    }
}
