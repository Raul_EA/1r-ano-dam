package mokepons.objectes;

import mokepons.*;

public class Arma extends Objecte implements Equipament{

	private int atacExtra;
	
	Arma(String nom,int atacExtra) {
		super(nom);
		this.atacExtra=atacExtra;
	}
	public void utilizar(Mokepon a) {
		this.equipar((MokeponCapturat) a);
	};
	public void equipar(MokeponCapturat a) {
		a.setObjecteEquipat(this);
		a.setAtk(a.getAtk()+atacExtra);
	}
	public void desequipar(MokeponCapturat a) {
		a.setObjecteEquipat(null);
		a.setAtk(a.getAtk()-atacExtra);
	}
	public int getAtacExtra() {
		return atacExtra;
	}

	public void setAtacExtra(int atacExtra) {
		this.atacExtra = atacExtra;
	}
	
}
