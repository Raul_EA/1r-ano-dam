package mokepons.objectes;

import mokepons.*;

public class Reviure extends Objecte{

	Reviure(String nom) {
		super(nom);
	}
	
	public void utilizar(Mokepon a) {
		if(a.isDebilitat() && this.getQuantitat()>0) {
			a.setDebilitat(false);
			a.setHp_actual(1);
			this.setQuantitat(getQuantitat()-1);
		}
	}
}
