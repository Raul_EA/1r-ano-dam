package mokepons;

import java.util.ArrayList;

public class ArrayListAcotada<T> extends ArrayList<T>{

	private static final long serialVersionUID = 1L;

	@Override
	public T get(int index) {
		
		if(super.size()<index)
			return super.get(super.size()-1);
		else if (index<0)
			return super.get(0);
		
		return super.get(index);
	}
}
