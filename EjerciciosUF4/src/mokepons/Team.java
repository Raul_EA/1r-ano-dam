package mokepons;

import java.util.ArrayList;

public class Team<T extends Mokepon> extends ArrayList<T>{

	private static final long serialVersionUID = 1L;
	
	@Override
	public boolean add(T a) {
		if(this.size()>=6)
			return false;
		else 
			return super.add(a);
	}
}
 