package mokepons;

public interface Equipament {
	
	public abstract void equipar(MokeponCapturat a);
	public abstract void desequipar(MokeponCapturat a);
	
	public default boolean potEquipar(MokeponCapturat mok) {
		if(mok.getObjecteEquipat()==null || mok.isDebilitat()) {
			return false;
		}else {
			return true;
		}
	}
	public default boolean equipMalPosat(MokeponCapturat mok) {
		if(mok.getObjecte() instanceof Equipament) {
			return true;
		}else {
			return false;
		}
	}
}
