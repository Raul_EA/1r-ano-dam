package ocells;

class Ocell {
	///Atributs
	private char color = ' ';
	private int edat = 0;
	
	//Metodes constructors. Sobrecarrega
	public Ocell() {
		color = ' ';
		edat = 0;
		
	}
	
	public Ocell (int edat, char color) {
		this.edat = edat;
		this.color = color;
	}
	
	///Metodes setter/getter
	public void setEdat (int e) {
		edat = e;
	}
	public int getEdat() {
		return edat;
	}
	public void setColor (char c) {
		color = c;
	}
	public char getColor() {
		return color;
	}
	
	//Metodes de comportament de l'objecte
	public void printEdat () {
		System.out.println(edat);
	}
	public void printColor () {
		switch (color) {
			case 'v': 
				System.out.println("verd"); 
				break;
			case 'a': 
				System.out.println("groc"); 
				break;
			case 'g': 
				System.out.println("gris"); 
				break;
			case 'n': 
				System.out.println("negre"); 
				break;
			case 'b': 
				System.out.println("blanc"); 
				break;
			default: 
				System.out.println("Color no establert"); 
		}
	}
}
