package ocells;

public class Test {

	public static void main(String [] args) {
		Ocell p1, p2;
		System.out.println("Pajaro p1:");
		p1 = new Ocell();
		p1.setEdat(5);
		p1.setColor('a');
		p1.printEdat();
		p1.printColor();
		System.out.println("Pajaro p2:");
		p2 = new Ocell(3, 'v');
		p2.printColor();
		p2.printEdat();
		System.out.println("Color Pajaro p1:");
		p1.setColor(p2.getColor());
		p1.printColor();
	}
}
