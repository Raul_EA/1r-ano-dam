package examenUF2;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio3 {

	static Scanner sc=new Scanner(System.in);
	
	public static void main(String[] args) {
		ArrayList<Integer> vec = new ArrayList<>();
		int op;
		do {
			op=menu();
			switch (op){
			case 1:
				obtenerPuntos(vec);
				break;
			case 2:
				viasualizar(vec);
				break;
			case 3:
				System.out.println(notaFinal(vec));
				break;
			case 4:
				System.out.println("Saliendo...");
				break;
			}
		}while(op!=4);
	}

	private static float notaFinal(ArrayList<Integer> vec) {
		vec.sort(null);
		float a = 0;
		for(int i=0;i<3;i++) {
			a+=vec.get(i+1);
		}
		return a/3;
	}

	private static void viasualizar(ArrayList<Integer> vec) {
		for(int i=0;i<vec.size();i++) {
			System.out.println(vec.get(i));
		}
	}

	private static void obtenerPuntos(ArrayList<Integer> vec) {
		vec.clear();
		for(int i=0;i<5;i++) {
			System.out.println("Introduce el valor numero "+(i+1)+" que quiere introducir en el vector");
			vec.add(sc.nextInt());
		}
	}

	private static int menu() {
		System.out.println("1-Obtener puntuaciones\n2-Visualizar puntuaciones\n3-Obtener nota final\n4-Salir");
		return sc.nextInt();
	}
	
}
