package examenUF2;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		ArrayList<Integer> num = new ArrayList<>();
		System.out.println("Introduzca un numero positivo:");
		recursividad(sc.nextInt(),num);
		System.out.println(num);
		System.out.println(num.size()-1);
		sc.close();
	}

	private static void recursividad(int a, ArrayList<Integer> num) {
		if(a==1) {
			num.add(a);
			return;
		}
		if(a%2==0) {
			num.add(a);
			recursividad(a/2,num);		
		}
		if(a%2==1) {
			num.add(a);
			recursividad((3*a)+1,num);		
		}
	}

}
