package examenUF2;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio1 {

	static Scanner sc = new Scanner(System.in);
	static Random rd = new Random();
	
	public static void main(String[] args) {
		int[][] tabla = null;
		boolean flag=false;
		int op;
		do {
			op=menu();
			switch (op){
			case 1:
				tabla=dimensionar();
				flag=true;
				break;
			case 2:
				if(flag==true)
					jugar(tabla);
				else
					System.out.println("Configura el tablero");
				break;
			case 3:
				System.out.println("Saliendo...");
				break;
			}
		}while(op!=3);
	}
	private static void jugar(int[][] tabla) {
		int a=0;
		inicializarTab(tabla);
		enseñarTab(tabla);
		while(!comprobar(tabla,a)) {
			a++;
			escojerCasilla(tabla);
			enseñarTab(tabla);
		}
	}
	private static void escojerCasilla(int[][] tabla) {
		int x=posicion(true,tabla.length);
		int y=posicion(false,tabla.length);
		for(int i=-1;i<=1;i++) {
			if(x+i>=0 && x+i<tabla.length) {
				if(tabla[x+i][y]==0)tabla[x+i][y]=1;
				else tabla[x+i][y]=0;
			}
		}
		for(int i=-1;i<=1;i++) {
			if(y+i>=0 && y+i<tabla.length) {
				if(tabla[x][y+i]==0)tabla[x][y+i]=1;
				else tabla[x][y+i]=0;
			}
		}
		if(tabla[x][y]==0)tabla[x][y]=1;
		else tabla[x][y]=0;
	}
	private static int posicion(boolean b,int c) {
		if(b) {
			System.out.println("Escoja la fila de la casilla:");
		}else {
			System.out.println("Escoja la columna de la casilla:");
		}
		int a =sc.nextInt();
		while(a<0 || a>=c) {
			System.out.println("Valor introducido incorrecto, porfavor vuelva a introducir otro valor");
			if(b) 
				System.out.println("Escoja la fila de la casilla:");
			else 
				System.out.println("Escoja la columna de la casilla:");
			a =sc.nextInt();	
		}
		return a;
	}
	private static boolean comprobar(int[][] tabla,int a) {
		int num=0;
		for(int i=0;i<tabla.length;i++) {
			for(int j=0;j<tabla.length;j++) {
				if(tabla[i][j]==1)num++;
			}
		}
		if(num==(tabla.length*tabla.length)){
			System.out.println("Ganaste, numero de jugadas "+a);
			return true;
		}
		else
			return false;
		
	}
	private static void enseñarTab(int[][] tabla) {
		for(int i=0;i<tabla.length;i++) {
			for(int j=0;j<tabla.length;j++) {
				System.out.print(tabla[i][j]+" ");
			}
			System.out.println();
		}
	}
	private static void inicializarTab(int[][] tabla) {
		for(int i=0;i<tabla.length;i++) {
			for(int j=0;j<tabla.length;j++) {
				tabla[i][j]=rd.nextInt(2);
			}
		}
	}
	private static int[][] dimensionar() {
		System.out.println("Introduce la dimension del tablero(Solo 1 dado que es cuadrada)");
		int a = sc.nextInt();
		return new int[a][a];
	}
	private static int menu() {
		System.out.println("1-Configurar\n2-Jugar\n3-Salir");
		return sc.nextInt();
	}

}
