package examen1;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt(), y = sc.nextInt();
		sc.nextLine();
		String a = sc.nextLine();
		sc.close();
		switch (a) {
		case "DRETA":
		case "RIGHT":
			x++;
			break;
		case "ESQUERRA":
		case "LEFT":
			x--;
			break;
		case "DALT":
		case "UP":
			y++;
			break;
		case "BAIX":
		case "DOWN":
			y--;
			break;
		case "RESET":
			x = 0;
			y = 0;
			break;
		default:
		}
		if (x < 0 || x > 10 || y < 0 || y > 10) {
			x = 0;
			y = 0;
		}
		System.out.println(x + " " + y);
	}
}
