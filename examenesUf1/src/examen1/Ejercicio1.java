package examen1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int h = sc.nextInt(), m = sc.nextInt(), mt = 0;
		sc.close();
		if (h < 0 || h > 23 || m < 0 || m > 59) {
			System.out.println("INCORRECTE");
		} else {
			mt = (23 - h) * 60 + (60 - m);
			System.out.println(mt);
		}
	}
}
