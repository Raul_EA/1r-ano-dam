package examen1;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int d = sc.nextInt(), m = sc.nextInt();
		sc.close();
		if (m == 4 || m == 5 || (m == 6 && d <= 22) || (m == 3 && d >= 23)) {
			System.out.println("PRIMAVERA");
		} else if (m == 7 || m == 8 || (m == 6 && d >= 23) || (m == 9 && d <= 22)) {
			System.out.println("ESTIU");
		} else if (m == 10 || m == 11 || (m == 9 && d >= 23) || (m == 12 && d <= 22)) {
			System.out.println("TARDOR");
		} else if (m == 1 || m == 2 || (m == 12 && d >= 22) || (m == 3 && d <= 22)) {
			System.out.println("HIVERN");
		}
	}

}
