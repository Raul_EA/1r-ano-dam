package examen3;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		boolean fun = true;
		final int VEC = 2;
		String[] cad = new String[VEC];
		while (fun) {
			System.out.println("Escoja una opcion: \n (1) Leer palabras \n (2) Enlazadas \n (3) Salir \n");
			int a = sc.nextInt();
			sc.nextLine();
			switch (a) {
			case 1: {
				int con = 1;
				for (int i = 0; i < VEC; i++) {
					System.out.println("\nIntroduce una cadena:");
					cad[i] = sc.nextLine();
				}
				for (int i = 1; i < VEC; i++) {
					cad[i - 1] = cad[i - 1].strip();
					cad[i] = cad[i].strip();
					if (cad[i - 1].equals(cad[i]))
						System.out.println("Las dos cadenas son iguales \n");
					else if (cad[i - 1].length() != cad[i].length())
						System.out.println("Las dos cadenas no tiene la misma longitud \n");
					else {
						con++;
					}
				}
				if (con == VEC) {
					System.out.println("Las palabras son:");
					for (int j = 0; j < VEC; j++) {
						cad[j] = cad[j].toLowerCase();
						System.out.println(cad[j]+"\n");
					}
				}
				break;
			}
			case 2: {
				boolean st = false;
				int con = 0;
				int i = 1;
				while (i - 1 < cad[0].length() && !st) {
					if (cad[1].charAt(i - 1) == cad[0].charAt(cad[0].length() - i)) {
						con++;
					} else {
						st = true;
					}
					i++;
				}
				System.out.println("\nSon iguales "+con+" letras \n");
				break;
			}
			case 3:{
				System.out.println("Saliendo...");
				fun=false;
			}
			}
		}
		sc.close();
	}
}
