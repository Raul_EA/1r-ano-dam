package examen3;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int DIE = 10;
		boolean fun = true;
		int[] a = new int[DIE];
		int[] b = new int[DIE];
		System.out.println("Introduzca la altura de los dientes superiores:");
		for (int i = 0; i < DIE; i++) {
			a[i] = sc.nextInt();
		}
		System.out.println("Introduzca la altura de los dientes inferiores:");
		for (int i = 0; i < DIE; i++) {
			b[i] = sc.nextInt();
		}
		sc.close();
		int sum = a[0] + b[0];
		int i = 1;
		while (i < DIE && fun) {
			if (a[i] + b[i] == sum) {
				i++;
			} else {
				fun = false;
			}
		}
		if (fun) {
			System.out.println("\nSI, le encajan todos los dientes");
		} else {
			System.out.println("\nNO, no le encajan todos los dientes");
		}
	}

}
