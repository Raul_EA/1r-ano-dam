package examen3;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		boolean fun = true;
		while(fun) {
			System.out.println("Escoja la opcion:\n (1) Bocata de queso\n (2) Bocata de fuet\n (3) Salir\n");
			int a = sc.nextInt();
			switch(a) {
			case 1:{
				sc.nextLine();
				System.out.println("\nIntroduce a los alumnos:");
				String alu1=sc.nextLine();
				String[] alu2 = alu1.split(";");
				ArrayList<String> alu3= new ArrayList<>();
				for(int i=0;i<alu2.length;i++) {
					alu3.add(alu2[i]);
				}
				System.out.println("\nIntroduce los alumnos que comen bocata a las 10:");
				String boc1 = sc.nextLine();
				String[] boc2 = boc1.split(" ");
				for(int j=0;j<boc2.length;j++) {
				for(int i=0;i<alu3.size();i++) {
					if(alu3.get(i).equals(boc2[j])) {
						alu3.remove(i);
					}
				}
				}
			System.out.println("Los que no se comen el bocata antes de tiempo son:\n\n"+alu3+"\n");
			break;
			}
			case 2:{
				System.out.println("\nIntroduce el numero de alumnos:");
				final int B=sc.nextInt();
				sc.nextLine();
				String[] al=new String [B];
				String[] ing=new String [B];
				ArrayList<String> fin_al = new ArrayList<> ();
				for(int i=0;i<B;i++) {
					System.out.println("\nIntroduce el alumno y su ingrediente:");
					al[i]=sc.next();
					ing[i]=sc.next();
				}
				System.out.println("\nQue ingrediente quieres que busque?");
				String fin_ing = sc.next();
				fin_ing=fin_ing.toLowerCase();
				for(int i=0;i<B;i++) {
					if(ing[i].equals(fin_ing)) {
						fin_al.add(al[i]);
					}
				}
				System.out.println("Los que tienen "+fin_ing+" son:");
				for(int i=0;i<fin_al.size();i++) {
					System.out.println(fin_al.get(i));
				}
				System.out.println("");
				break;
			}
			case 3:{
				System.out.println("Saliendo...");
				fun=false;
				break;
			}
			}
		}
		sc.close();
	}

}
