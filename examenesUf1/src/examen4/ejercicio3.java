package examen4;

import java.util.Random;
import java.util.Scanner;

public class ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		final int M = 5;
		int[][] m = new int [M][M];
		for(int i=0;i<M;i++) {
			for(int j=0;j<M;j++) {
				m[i][j]=rd.nextInt(6,8);
			}
		}
		System.out.println("Tabla:");
		for(int i=0;i<M;i++) {
			System.out.println();
			for(int j=0;j<M;j++) {
				System.out.print(m[i][j]+" ");
			}
		}
		int con=0;
		for(int i=0;i<M-1;i++) {
			for(int j=0;j<M-1;j++) {
				if(m[i][j]==7 && m[i+1][j]==7 && m[i+1][j+1]==7) {
					con++;
				}
			}
		}
		System.out.println("\n"+"\n"+"Veces aparece el logotipo: "+con);
		sc.close();
	}

}
