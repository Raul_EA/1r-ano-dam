package examen4;

import java.util.Random;
import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		System.out.println("Introduce las dimensiones del jardin");
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		char[][] m = new char[F][C];
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				m[i][j]='*';
			}
		}
		m[rd.nextInt(F+1)][rd.nextInt(C+1)]='T';
		System.out.println("Introduce el numero de intentos");
		int in = sc.nextInt();
		boolean fun  =true;
		while(fun && in>0) {
			System.out.println("\nIntroduce la posicion del intento");
			int x=sc.nextInt();
			int y=sc.nextInt();
			if(m[x][y]=='T') {
				fun=false;
				System.out.println("Felicidades, encontraste el tesoro!!!");
			}else {
				in--;
				System.out.println("Intentos: "+in+"\nFallaste!");
				m[x][y]='E';
			}
		}
		for(int i=0;i<F;i++) {
			System.out.println();
			for(int j=0;j<C;j++) {
				System.out.print(m[i][j]+" ");
			}
		}
		sc.close();
	}
}
