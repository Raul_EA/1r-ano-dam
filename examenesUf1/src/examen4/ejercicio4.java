package examen4;

import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el tamaño de la matriz cuadrada:");
		final int M = sc.nextInt();
		int[][] m = new int [M][M];
		System.out.println("Introduce los valores de la matriz:");
		for (int i=0;i<M;i++) {
			for (int j=0;j<M;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		int con=0;
		for (int i=0;i<M;i++) {
			for (int j=0;j<M;j++) {
				if(j==i) {
					if(m[i][j]==i+1) {
						con++;
					}
				}else if(m[i][j]==0){
					con++;
				}
			}
		}
		if(con==M*M) {
			System.out.println("Sí es una matriz creciente");
		}else {
			System.out.println("No es una matriz creciente");
		}
		sc.close();
	}

}
