package examen4;

import java.util.Scanner;

public class ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int f=0;
		int c=0;
		char[][] t = null;
		boolean fun = true;
		while (fun) {
			System.out.println("\nMenu:\n1-Dimensionar\n2-Ver tablero\n3-Vertical\n4-Rectangulo\n5-Limites\n0-Salir");
			int a = sc.nextInt();
			switch (a) {
			case 1:
				System.out.println("Introduce las dimensiones del tablero(Recuerda que tiene que ser entre 4 i 20)"); 
				f=sc.nextInt();
				c=sc.nextInt();
				t= new char[f][c];
				for(int i=0;i<f;i++) {
					for(int j=0;j<c;j++) {
						t[i][j]='.';
					}
				}
				break;
			case 2:
				for(int i=0;i<f;i++) {
					System.out.println();
					for(int j=0;j<c;j++) {
						System.out.print(t[i][j]+" ");
					}
				}
				break;
			case 3:
				System.out.println("Introduce la columna a pintar:");
				int col=sc.nextInt();
				System.out.println("Introduce el inicio de la fila y el final:");
				int in=sc.nextInt();
				int fi=sc.nextInt();
				for(int i=in;i<=fi;i++) {
					t[i][col]='X';
				}
				break;
			case 4:
				int cam=0;
				System.out.println("Introduce una fila y una columna del rectangulo:");
				int x1=sc.nextInt(),y1=sc.nextInt();
				System.out.println("Introduce otra fila y columna del rectangulo:");
				int x2=sc.nextInt(),y2=sc.nextInt();
				if(x1>x2) {
					cam=x1;
					x1=x2;
					x2=cam;
				}
				if(y1>y2) {
					cam=y1;
					y1=y2;
					y2=cam;
				}
				for(int i=x1;i<=x2;i++) {
					for(int j=y1;j<=y2;j++) {
						t[i][j]='X';
					}
				}
				break;
			case 5:
				for(int i=0;i<f;i++) {
					for(int j=0;j<c;j++) {
						if(i==0 || i==f-1 || j==0 || j==f-1) {
							t[i][j]='X';
						}
					}
				}
				System.out.println("Limites pintados");
				break;
			case 0:
				fun=false;
				System.out.println("Saliendo...");
				break;
			}
		}
		sc.close();
	}

}
