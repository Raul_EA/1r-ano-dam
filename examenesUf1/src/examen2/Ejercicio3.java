package examen2;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Hora: ");
		int h = sc.nextInt();
		System.out.println("Minuto: ");
		int m = sc.nextInt();
		System.out.println("Segundo: ");
		int s = sc.nextInt();
		System.out.println();
		System.out.println(h + ":" + m + ":" + s);
		System.out.println();
		System.out.println("Numero de segundos a sumar: ");
		int ns = sc.nextInt();
		sc.close();
		for (int i = 0; i < ns; i++) {
			s++;
			if (s >= 60) {
				s = 0;
				m++;
				if (m >= 60) {
					m = 0;
					h++;
					if (h >= 24) {
						h = 0;
						m = 0;
						s = 0;
					}
				}
			}
			System.out.println(h + ":" + m + ":" + s);
		}
	}

}
