package examen2;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		System.out.println("Introduce la primera nota: ");
		int a=sc.nextInt();
		int t=0,c=1,xup=0;
		float m=0;
		boolean sus=false,ex=false,mit=false;
		//dado que el ejercicio no especifica cuando acabar el bucle he interpretado que cuando se introduce un valor -1 este acabara
		while (a!=-1) {
			m=m+a;
			c++;
			if (a<5) {
				sus=true;
			}else {
				if(a==6) {
					t++;
				} else if(a==10){
					ex=true;
				}
			}
			System.out.println("Introduce la siguiente nota: ");
			a=sc.nextInt();
		}
		sc.close();
		m=m/c;
		if(m>=8) {
			mit=true;
		}
		System.out.println();
		if(sus==true) System.out.println("Carbon");
		else if (t>=3) {
			System.out.println("Incienso");
			xup++;
		}
		if(ex==true) {
			xup++;
			System.out.println("Mirra");
		}
		if(mit==true) {
			System.out.println("Oro");
			xup++;
		}
		System.out.println();
		if(xup==3) System.out.println("Chupito pal ganador");
	}

}
