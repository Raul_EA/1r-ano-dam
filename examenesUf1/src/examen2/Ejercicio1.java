package examen2;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce una temperatura:");
		int b = 0;
		int a = sc.nextInt();
		System.out.println("Introduce la siguiente temperatura:");
		b = a;
		a = sc.nextInt();
		int con = 1;
		while (b >= a) {
			con++;
			System.out.println("Introduce la siguiente temperatura:");
			b = a;
			a = sc.nextInt();
		}
		sc.close();
		System.out.println("Has introducido " + con + " temperaturas antes de acabar.");
	}

}
