package examen2;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		int c = 0;
		boolean sale = false;
		System.out.println("Introduce valor de X:");
		int x = sc.nextInt();
		System.out.println("Introduce valor de Y:");
		int y = sc.nextInt();
		System.out.println("Introduce el numero de ordenes a procesar:");
		int numord = sc.nextInt();
		sc.close();
		for (int i = 0; numord > i; i++) {
			c++;
			int r = rd.nextInt(5);
			System.out.println();
			System.out.println("El valor de la orden es el numero " + r + ".");
			switch (r) {
			case (0):
				break;
			case (1):
				x++;
				break;
			case (2):
				x--;
				break;
			case (3):
				y++;
				break;
			case (4):
				y--;
			}
			System.out.println("La nueva posicion es: (" + x + "," + y + ")");
			if (x < 0 || x > 10 || y < 0 || y > 10) {
				i = numord;
				sale = true;
			}
		}
		if (sale == false) {
			System.out.println();
			System.out.println("PROVA CALAMAR SUPERADA");
		} else {
			System.out.println();
			System.out.println("PROVA CALAMAR NO SUPERADA " + c);
		}
	}

}
