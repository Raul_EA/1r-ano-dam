package codejamFebrero;

import java.util.ArrayList;
import java.util.Scanner;

public class ManInTheMiddle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int q=sc.nextInt();
		for(int i=0;i<q;i++) {
			ArrayList<Integer> z = new ArrayList<>();
			ArrayList<Integer> x = new ArrayList<>();
			int w=sc.nextInt();
			for(int j=0;j<w;j++) {
				x.add(sc.nextInt());
			}
			x.sort(null);
			for(int j=x.size()-1;j>=0;j--) {
				z.add(x.get(j));
			}
			int A=0;
			int B=0;
			for(int j=0;j<w-1;j++) {
				if(A<B) {
					A+=z.get(j);
				}else if(B<A) {
					B+=z.get(j);
				}else {
					A+=z.get(j);
				}
			}
			boolean flag=false;
			if(A<B) {
				A+=z.get(z.size()-1);
			}else if(B<A) {
				B+=z.get(z.size()-1);
				flag=true;
			}else {
				A+=z.get(z.size()-1);
			}
			boolean flag2=false;
			if(A>B) {
				flag2=true;
			}
			if(flag) {
				B-=z.get(z.size()-1);
			}else {
				A-=z.get(z.size()-1);
			}
			boolean flag3=false;
			if(A>B) {
				flag3=true;
			}
			if(flag2==flag3) {
				System.out.println("NO");
			}else {
				System.out.println("SI");
			}
		}		
	}
}