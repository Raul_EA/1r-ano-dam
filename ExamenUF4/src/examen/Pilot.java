package examen;

public abstract class Pilot {
	String nom;
	int posicio;
	Objecte elMeuObjecte;
	
	public abstract void agafarObjecte(Objecte ob);
	public abstract String cridar ();
	
	public Pilot(String nom){
		this.nom=nom;
		this.posicio=0;
		this.elMeuObjecte=null;
	}
	
	public boolean avansar(Circuit c, int increment) {
		this.posicio=this.posicio+increment;
		if(this.posicio>=50) {
			System.out.println(this.cridar());
			return false;
		}else {
			if(c.caselles.get(this.posicio)!=null) {
				this.agafarObjecte(c.caselles.get(this.posicio));
				if(c.caselles.get(this.posicio) instanceof Moneda)
					c.caselles.set(this.posicio, null);
			}
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this==obj)
			return true;
		
		if(obj==null)
			return false;
		
		if(getClass()!=obj.getClass())
			return false;
		
		return false;
 	}
	
	@Override
	public String toString() {
		return "Pilot [nom=" + nom + ", posicio=" + posicio + ", elMeuObjecte=" + elMeuObjecte + "]";
	}
}
