package examen;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Circuit {
	
	public static Random rd = new Random();
	
	public final int NC = 50;
	
	public List<Objecte> caselles;
	
	Circuit(int nObjectes){
		this.caselles=new ArrayList<Objecte>(NC);
		for(int i=0;i<NC;i++) {
			this.caselles.add(null);
		}
		if(nObjectes>20)
			nObjectes=20;
		
		for(int i=0;i<nObjectes;i++) {
			int c = rd.nextInt(0,50);
			if(this.caselles.get(c)==null) {
				int t = rd.nextInt(0,3);
				switch (t) {
				case 0:
					Objecte a = new Moneda(c);
					this.caselles.set(c, a);
					break;
				case 1:
					Objecte b = new Platan();
					this.caselles.set(c, b);
					break;
				case 2:
					Objecte d = new Neumatic();
					this.caselles.set(c, d);
					break;
				}
			}
		}
	}
	
	public void visualizar() {
		for(int i=0;i<NC;i++) {
			String a = (i+1)+ ": "+this.caselles.get(i);
			System.out.println(a);
		}
	}
}
