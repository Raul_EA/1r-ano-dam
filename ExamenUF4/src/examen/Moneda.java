package examen;

public class Moneda extends Objecte{
	
	public int valor;
	
	public Moneda(int quinvalor) {
		super();
		this.valor=quinvalor;
		this.prioritari=true;
		this.tipusObjecte=TipusObjecte.PREMI;
	}

	@Override
	public String toString() {
		return "Moneda [valor=" + valor + "]";
	}
	
}
