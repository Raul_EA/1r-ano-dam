package examen;

public abstract class Objecte implements Comparable<Object>{
	
	protected boolean prioritari;
	protected TipusObjecte tipusObjecte;
	
	public Objecte(){
		this.prioritari=false;
	}
	
	public void llencar() {
		System.out.println("M'han llençat");
	}
	
	@Override
	public int compareTo (Object arg0) {
		Objecte b = (Objecte)arg0;
		if(this instanceof Moneda && !(b instanceof Moneda))
			return -1;
		else if (!(this instanceof Moneda) && b instanceof Moneda)
			return 1;
		else if(this instanceof Neumatic && !(b instanceof Neumatic))
			return -1;
		else if (!(this instanceof Neumatic) && b instanceof Neumatic)
			return 1;
		else if (this instanceof Platan && !(b instanceof Platan))
			return -1;
		else if (!(this instanceof Platan) && b instanceof Platan)
			return 1;
		else return 0;
	}
	
	public boolean isPrioritari() {
		return prioritari;
	}

	public void setPrioritari(boolean prioritari) {
		this.prioritari = prioritari;
	}

	public TipusObjecte getTipusObjecte() {
		return tipusObjecte;
	}

	public void setTipusObjecte(TipusObjecte tipusObjecte) {
		this.tipusObjecte = tipusObjecte;
	}
	
}
