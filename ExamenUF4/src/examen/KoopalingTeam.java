package examen;

public class KoopalingTeam extends Pilot implements Malote {

	public KoopalingTeam(String nom) {
		super(nom);
	}

	@Override
	public void xocar(Pilot p) {
		if (this.equals(p) == true)
			;
		else if (this.elMeuObjecte == null)
			this.posicio = 0;
		else if (p.elMeuObjecte == null)
			p.posicio = 0;
		else {
			if (this.elMeuObjecte instanceof Platan) {
				p.posicio = 0;
				this.elMeuObjecte = null;
			} else if (this.elMeuObjecte instanceof Neumatic) {
				this.elMeuObjecte = null;
			} else if (this.elMeuObjecte instanceof Moneda && p.elMeuObjecte instanceof Moneda) {
				((Moneda) this.elMeuObjecte).valor = ((Moneda) this.elMeuObjecte).valor
						+ ((Moneda) p.elMeuObjecte).valor;
				((Moneda) p.elMeuObjecte).valor = 0;
			}
		}

	}

	@Override
	public void agafarObjecte(Objecte ob) {
		if (this.elMeuObjecte == null)
			this.elMeuObjecte = ob;
		else if (ob instanceof Neumatic && this.elMeuObjecte instanceof Neumatic)
			if (((Neumatic) ob).superNeumatic) {
				this.elMeuObjecte = ob;
			} else if (ob instanceof Neumatic && !(this.elMeuObjecte instanceof Neumatic)) {
				this.elMeuObjecte = ob;
			}
	}

	@Override
	public String cridar() {
		return "SIIIIIIIIIII";
	}

	@Override
	public String toString() {
		return "KoopalingTeam\n" + super.toString();
	}

}
