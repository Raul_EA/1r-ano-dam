package examen;

public class Platan extends Objecte{
	 
	public static int totalPlatans=0;
	 
	public int numPlatan;
	
	public Platan() {
		super();
		totalPlatans++;
		this.numPlatan=totalPlatans;
		this.tipusObjecte=TipusObjecte.ATAC;
	}
	
	@Override
	public void llencar() {
		System.out.println("Toma platanaco");
	}

	@Override
	public String toString() {
		return "Platan [numPlatan=" + numPlatan + "]";
	}
}
