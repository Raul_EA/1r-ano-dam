package examen;

import java.util.Random;

public class Neumatic extends Objecte{
	
	public static Random rd = new Random();

	public boolean superNeumatic;
	
	public Neumatic() {
		super();
		this.superNeumatic=rd.nextBoolean();
		this.tipusObjecte=TipusObjecte.DEFENSA;
	}

	@Override
	public String toString() {
		return "Neumatic [superNeumatic=" + superNeumatic + "]";
	}
	
}
