package examen;

import java.util.List;
import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		Circuit c = new Circuit(20);
		c.visualizar();
		
		MarioTeam m = new  MarioTeam("Josemanuel");
		KoopalingTeam k = new KoopalingTeam("Antonio");
		
		System.out.println(m);
		System.out.println(k);
		
		while(m.avansar(c, 1)==true);
		while(k.avansar(c, 1)==true);
		
		System.out.println(m);
		System.out.println(k);
		
		List<Objecte> z = new ArrayList<Objecte>();
		
		Objecte a = new Moneda(20);
		Objecte b = new Neumatic();
		Objecte d = new Platan();
		Objecte e = new Moneda(20);
		Objecte f = new Neumatic();
		Objecte g = new Platan();
		Objecte h = new Moneda(20);
		Objecte i = new Neumatic();
		Objecte j = new Platan();
		
		z.add(a);
		z.add(b);
		z.add(d);
		z.add(e);
		z.add(f);
		z.add(g);
		z.add(h);
		z.add(i);
		z.add(j);
		
		z.sort(null);
		
		System.out.println("\n"+z);
		
		k.xocar(m);
		System.out.println("\n"+k);
		System.out.println(m);
		
	}

}
