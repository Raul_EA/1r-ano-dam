package f_matrices;

import java.util.Scanner;

public class IndexOfMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int f = sc.nextInt();
		int c = sc.nextInt();
		int[][] m = new int[f][c];
		for (int i=0;i<f;i++) {
			for(int j=0;j<c;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		int a=sc.nextInt();
		sc.close();
		int x=-1;
		int y=-1;
		for (int i=0;i<f;i++) {
			for(int j=0;j<c;j++) {
				if (m[i][j]==a) {
					x=i;
					y=j;
				}
			}
		}
		System.out.println(x+" "+y);
	}
}
