package f_matrices;

import java.util.Scanner;

public class CuadradoAritmetico1 {
	public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int[][] matrix = new int [3][3];
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[i].length; ++j) {
                matrix[i][j] = sc.nextInt();
            }
        }
        sc.close();
        boolean aritmetic = true;
        for (int i = 0; i < 3 && aritmetic; ++i) {
            int d = matrix[i][1]-matrix[i][0];
            if (matrix[i][0] != matrix[i][1]-d || matrix[i][1] != matrix[i][2]-d) aritmetic = false;
        }
        for (int j = 0; j < 3 && aritmetic; ++j) {
            int d = matrix[1][j]-matrix[0][j];
            if (matrix[0][j] != matrix[1][j]-d || matrix[1][j] != matrix[2][j]-d) aritmetic = false;
        }

        if (aritmetic && (matrix[0][0] != 1 && matrix[0][1] != 1 && matrix[0][2] != 1)) System.out.println("si");
        else System.out.println("no");

    }

}

