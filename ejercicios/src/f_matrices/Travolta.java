package f_matrices;

import java.util.Scanner;

public class Travolta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		int[][] m = new int[F][C];
		int a = sc.nextInt();
		int b=0;
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		for(int i=0;i<F;i++) {
			for(int j=0;j<C-3;j++) {
				if(m[i][j]==a && m[i][j+2]==a) {
					b=m[i][j+1];
				}
			}
		}
		if(b!=0) {
			System.out.println(b);
		}else{
			System.out.println("NO");
		}
		sc.close();
	}
}
