package f_matrices;

import java.util.Random;
import java.util.Scanner;

public class BombermanEXAMEN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Random rd = new Random ();
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		int[][] m = new int[F][C];
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				m[i][j]=rd.nextInt(0,10);
			}
		}
		int x = sc.nextInt();
		int y = sc.nextInt();
		sc.close();
		int sum=0;
		for(int i=0;i<F;i++) {
			sum=sum+m[i][y];
			m[i][y]=0;
		}
		for(int i=0;i<C;i++) {
			sum=sum+m[x][i];
			m[x][i]=0;
		}
		System.out.println(sum);
	}
}
