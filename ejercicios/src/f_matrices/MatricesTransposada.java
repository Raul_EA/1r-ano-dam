package f_matrices;

import java.util.Scanner;

public class MatricesTransposada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int f=sc.nextInt();
		int c=sc.nextInt();
		int[][] m = new int[f][c];
		for(int i=0;i<f;i++) {
			for(int j=0;j<c;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		sc.close();
		int[][] m2 = new int [c][f];
		for(int i=0;i<c;i++) {
			for(int j=0;j<f;j++) {
				m2[i][j]=m[j][i];
			}
		}
		for(int i=0;i<c;i++) {
			System.out.println();
			for(int j=0;j<f;j++) {
				System.out.print(m2[i][j]+" ");
			}
		}
	}

}
