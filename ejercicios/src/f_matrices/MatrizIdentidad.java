package f_matrices;

import java.util.Scanner;

public class MatrizIdentidad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int A = sc.nextInt();
		sc.close();
		int[][] m = new int[A][A];
		for(int i=0;i<A;i++) {
			for(int j=0;j<A;j++) {
				if(i==j) {
					m[i][j]=1;
				}else {
					m[i][j]=0;
				}
			}
		}
		for(int i=0;i<A;i++) {
			System.out.println();
			for(int j=0;j<A;j++) {
				System.out.print(m[i][j]+" ");			
			}
		}
	}
}
