package f_matrices;

import java.util.Random;
import java.util.Scanner;

public class BestAthenaEUW_EXAMEN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		final int Q = 7;
		int[][] m = new int[Q][Q];
		for (int i = 0; i < Q; i++) {
			for (int j = 0; j < Q; j++) {
				m[i][j] = rd.nextInt(1, 3);
			}
		}
		int x = sc.nextInt();
		int y = sc.nextInt();
		sc.close();
		int con = 0;
		if (m[x][y] == 1) {
			for (int i = x - 1; i <= x + 1; i++) {
				for (int j = y - 1; j <= y + 1; j++) {
					if (i >= 0 && i < Q && j >= 0 && j < Q)
						if (m[i][j] == 2) {
							con++;
						}
				}
			}
			System.out.println(con);
		} else {
			System.out.println("ERROR");
		}
	}

}
