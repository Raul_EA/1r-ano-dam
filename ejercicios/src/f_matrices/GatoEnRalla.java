package f_matrices;

import java.util.Scanner;

public class GatoEnRalla {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		// Lectura del nombre de casos
		int numCasos = scan.nextInt();
		while (numCasos > 0) {
			numCasos--;

			// Lectura del nombre de files i columnes
			int numFiles = scan.nextInt();
			int numColumnes = scan.nextInt();
			
			// Crear la matriu
			int matriu[][] = new int[numFiles][numColumnes];

			// Llegir les dades
			for (int fila = 0; fila < numFiles; fila++) {
				for (int col = 0; col < numColumnes; col++) {
					matriu[fila][col] = scan.nextInt();
				}
			}
			
			//Cerrar escaner
			scan.close();
			
			// Recorrer la matriu
			int comptador = 0;
			for (int fila = 0; fila < numFiles; fila++) {
				for (int col = 0; col < numColumnes; col++) {
					if (matriu[fila][col] == 1) {
						// He trobat un gat!
						// Busco si n'hi ha 3 en ratlla.
						// Miraré sempre endavant (si hagués format part d'un 3 en ratlla, ja s'hagués
						// eliminat el gat

						// Per comprovar si ja he fet servir aquest gat
						boolean gatEnRatlla = false;

						// Mirar en vertical (vigilant que no em surti dels límits)
						if (fila < numFiles - 2) {
							if (matriu[fila + 1][col] == 1 && matriu[fila + 2][col] == 1) {
								// 3 en ratlla!
								comptador++;
								gatEnRatlla = true;
								// Elimino els gats per tal que no puguin formar part de cap altre 3 en ratlla
								for (int i = 0; i <= 2; i++)
									matriu[fila + i][col] = 0;
							}
						}

						// Mirar en horitzontal (vigilant que no em surti dels límits)
						if (!gatEnRatlla && col < numColumnes - 2) {
							if (matriu[fila][col + 1] == 1 && matriu[fila][col + 2] == 1) {
								// 3 en ratlla!
								comptador++;
								gatEnRatlla = true;
								// Elimino els gats per tal que no puguin formar part de cap altre 3 en ratlla
								for (int i = 0; i <= 2; i++)
									matriu[fila][col + i] = 0;
							}
						}

						// Mirar en diagonal \ (vigilant que no em surti dels límits)
						if (!gatEnRatlla && fila < numFiles - 2 && col < numColumnes - 2) {
							if (matriu[fila + 1][col + 1] == 1 && matriu[fila + 2][col + 2] == 1) {
								// 3 en ratlla!
								comptador++;
								gatEnRatlla = true;
								// Elimino els gats per tal que no puguin formar part de cap altre 3 en ratlla
								for (int i = 0; i <= 2; i++)
									matriu[fila + i][col + i] = 0;
							}
						}

						// Mirar en diagonal / (vigilant que no em surti dels límits)
						if (!gatEnRatlla && fila < numFiles - 2 && col > 1) {
							if (matriu[fila + 1][col - 1] == 1 && matriu[fila + 2][col - 2] == 1) {
								// 3 en ratlla!
								comptador++;
								gatEnRatlla = true;
								// Elimino els gats per tal que no puguin formar part de cap altre 3 en ratlla
								for (int i = 0; i <= 2; i++)
									matriu[fila + i][col - i] = 0;
							}
						}

					}
				}
			}

			// Mostrar el resultats
			System.out.println(comptador);
		}
	}

}
