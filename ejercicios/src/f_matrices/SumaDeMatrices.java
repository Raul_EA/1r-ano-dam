package f_matrices;

import java.util.Scanner;

public class SumaDeMatrices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int A=sc.nextInt();
		int[][] m1 = new int[A][A];
		int[][] m2 = new int[A][A];
		for(int i=0;i<A;i++) {
			for (int j=0;j<A;j++) {
				m1[i][j]=sc.nextInt();
			}
		}
		for(int i=0;i<A;i++) {
			for (int j=0;j<A;j++) {
				m2[i][j]=sc.nextInt();
			}
		}
		sc.close();
		for(int i=0;i<A;i++) {
			for (int j=0;j<A;j++) {
				m2[i][j]=m2[i][j]+m1[i][j];
			}
		}
		for(int i=0;i<A;i++) {
			System.out.println();
			for (int j=0;j<A;j++) {
				System.out.print(m2[i][j]+" ");
			}
		}
	}

}
