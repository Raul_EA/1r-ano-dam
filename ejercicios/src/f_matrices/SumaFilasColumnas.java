package f_matrices;

import java.util.Scanner;

public class SumaFilasColumnas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int f=sc.nextInt();
		int c=sc.nextInt();
		int[][] m = new int[f][c];
		for (int i=0;i<f;i++) {
			for(int j=0;j<c;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		int a = sc.nextInt();
		sc.close();
		int b=0;
		int d=0;
		for(int i=0;i<c;i++) {
			b=b+m[a][i];
		}
		for(int i=0;i<f;i++) {
			d=d+m[i][a];
		}
		System.out.println(b+" "+d);
	}

}
