package f_matrices;

import java.util.Scanner;

public class Bomberman {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		int[][] m = new int[F][C];
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		int x = sc.nextInt();
		int y = sc.nextInt();
		sc.close();
		int sum=0;
		for(int i=0;i<F;i++) {
			sum=sum+m[i][y];
		}
		for(int i=0;i<C;i++) {
			sum=sum+m[x][i];
		}
		System.out.println(sum-m[x][y]);
	}
}
