package f_matrices;

import java.util.Scanner;

public class EleccionsAlConsellEscolar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int A = sc.nextInt();
		sc.close();
		char[][] m = new char[A][A];
		for(int i=0;i<A;i++) {
			for(int j=0;j<A;j++) {
				if(j==0 || j==A-1) {
					m[i][j]='X';
				} else {
					if(i==0 || i==A-1) {
						m[i][j]='X';
					} else {
						if(j==i || j==A-1-i) {
							m[i][j]='X';
						} else {
							m[i][j]='.';
						}
					}
				}
			}
		}
		for(int i=0;i<A;i++) {
			for(int j=0;j<A;j++) {
				System.out.print(m[i][j]);
			}
			System.out.println();
		}
	}

}
