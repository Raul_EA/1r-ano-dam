package f_matrices;

import java.util.Scanner;

public class MaximaMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int casos = sc.nextInt();
		for (int w=0;w<casos;w++) {
			int f = sc.nextInt();
			int c = sc.nextInt();
			int[][] m = new int[f][c];
			for(int i=0;i<f;i++) {
				for (int j=0;j<c;j++) {
					m[i][j]=sc.nextInt();
				}
			}
			int a=0;
			int x=0;
			int y=0;
			for(int i=0;i<f;i++) {
				for (int j=0;j<c;j++) {
					if(m[i][j]>a) {
						a=m[i][j];
						x=i;
						y=j;
					}
				}
			}
			System.out.println((x+1) + " " + (y+1));
		}
		sc.close();
	}

}
