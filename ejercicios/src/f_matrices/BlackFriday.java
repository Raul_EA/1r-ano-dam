package f_matrices;

import java.util.Scanner;

public class BlackFriday {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		int[][] m = new int[F][C];
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		int b=sc.nextInt();
		sc.close();
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				m[i][j]=m[i][j]*b;
			}
		}
		for(int i=0;i<F;i++) {
			System.out.println();
			for(int j=0;j<C;j++) {
				System.out.print(m[i][j]);
			}
		}
	}

}
