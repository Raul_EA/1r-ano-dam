package f_matrices;

import java.util.Scanner;

public class OftalmologoBien {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String a = sc.nextLine();
		sc.close();
		int gv = 0, gh = 0;
		for (int k = 0; k < a.length(); k++) {
			if (a.charAt(k) == 'V') {
				gv++;
			} else if (a.charAt(k) == 'H') {
				gh++;
			}
		}
		if (gv % 2 == 0 && gh % 2 == 0) {
			System.out.println("1 2\n3 4");
		}
		if (gv % 2 == 1 && gh % 2 == 0) {
			System.out.println("2 1\n4 3");
		}
		if (gv % 2 == 0 && gh % 2 == 1) {
			System.out.println("3 4\n1 2");
		}
		if (gv % 2 == 1 && gh % 2 == 1) {
			System.out.println("4 3\n2 1");
		}
	}
}
