package f_matrices;

import java.util.Scanner;

public class EscribeUnaMatriz2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int f = sc.nextInt();
		int c = sc.nextInt();
		int[][] m = new int[f][c];
		for(int i=0;i<f;i++) {
			for(int j=0;j<c;j++) {
				m[i][j]=sc.nextInt();
			}
		}
		int x=sc.nextInt();
		int y=sc.nextInt();
		sc.close();
		for(int i=0;i<f;i++) {
			for(int j=0;j<c;j++) {
				if(m[i][j]==x) {
					m[i][j]=y;
				}
			}
		}
		for(int i=0;i<f;i++) {
			System.out.println();
			for(int j=0;j<c;j++) {
				System.out.print(m[i][j]+" ");
			}
		}
	}

}
