package f_matrices;

import java.util.Scanner;

public class HabiaUnaCebraEnLosBeatles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		final int F1 = sc.nextInt();
		sc.close();
		int[][] m = new int[F][C];
		boolean flag=true;
		for(int i=0;i<F;i++) {
			for(int j=0;j<C;j++) {
				if(F1%2==0) {
					if(flag) m[i][j]=1;
					else m[i][j]=0;
				} else {
					if(flag) m[i][j]=0;
					else m[i][j]=1;
				}
			}
			flag =!flag;
		}
		for(int i=0;i<F;i++) {
			System.out.println();
			for(int j=0;j<C;j++) {
			System.out.print(m[i][j]);
			}
		}
	}

}
