package f_matrices;

import java.util.Scanner;

public class Rectangulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int F = sc.nextInt();
		final int C = sc.nextInt();
		final int F1 = sc.nextInt();
		final int C1 = sc.nextInt();
		final int F2 = sc.nextInt();
		final int C2 = sc.nextInt();
		sc.close();
		char[][] m = new char[F][C]; 
		for (int i=0;i<F;i++) {
			for (int j=0;j<C;j++) {
				if(i>=F1 && i<=F2 && j>=C1 && j<=C2) {
					m[i][j]='X';
				} else {
					m[i][j]='.';
				}
			}
		}
		for (int i=0;i<F;i++) {
			System.out.println();
			for (int j=0;j<C;j++) {
					System.out.print(m[i][j]+" ");
			}
		}
	}

}
