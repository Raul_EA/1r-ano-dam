package f_matrices;

import java.util.Scanner;

public class CinemarcEXAMEN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int F = 12;
		final int C = 15;
		char[][] s = new char[F][C];
		for (int i = 0; i < F; i++) {
			for (int j = 0; j < C; j++) {
				s[i][j] = '_';
			}
		}
		boolean fun = true;
		boolean fun2 = true;
		int x = 0;
		int y = 0;
		while (fun) {
			System.out.println(
					"Menu:\n(1)Vaciar Sala\n(2)Ver Sala\n(3)Reservar butacas\n(4)Consecutivos\n(5)No cabes\n(6)Salir");
			int a = sc.nextInt();
			switch (a) {
			case 1: {
				for (int i = 0; i < F; i++) {
					for (int j = 0; j < C; j++) {
						s[i][j] = '_';
					}
				}
				fun2 = true;
				y = 0;
				x = 0;
				System.out.println("Sala vaciada\n");
				break;
			}
			case 2: {
				for (int i = 0; i < F; i++) {
					for (int j = 0; j < C; j++) {
						System.out.print(s[i][j]);
					}
					System.out.println();
				}
				System.out.println();
				break;
			}
			case 3: {
				int con = 0;
				while (con < 10 && fun2) {
					if (s[x][y] != 'X') {
						s[x][y] = 'X';
						y++;
						con++;
						if (y == C) {
							y = 0;
							x++;

						}
						if (x == F) {
							fun2 = false;
						}
					}else {
						y++;
						if (y == C) {
							y = 0;
							x++;

						}
						if (x == F) {
							fun2 = false;
						}
					}
				}
				if (!fun2) {
					System.out.println("ERROR");
				}else {
					System.out.println("Butacas reservadas");
				}
				break;
			}
			case 4: {
				for (int i = 0; i < F; i++) {
					int con = 0;
					boolean consec = false;
					for (int j = 0; j < C - 1; j++) {
						if (s[i][j] == '_' && s[i][j + 1] == '_' && !consec) {
							con++;
							consec = true;
						}
						if (consec == true && s[i][j] == 'X') {
							consec = false;
						}
					}
					System.out.println("Hay de la fila numero "+i+": "+con+" secuencia de butacas");
				}
				break;
			}
			case 5: {
				boolean fun3 = false;
				System.out.println("Introduce la fila");
				int f = sc.nextInt();
				f=f-1;
				for (int i = 0; i < C && !fun3; i++) {
					if (f == 0 && s[f][i] == '_') {
						s[f][i] = 'X';
						fun3 = true;
					} else if (s[f][i] == '_' && s[f - 1][i] == '_') {
						s[f][i] = 'X';
						fun3 = true;
					}
				}
				if (!fun3) {
					System.out.println("No hay sitio");
				}
				break;
			}
			case 6:{
				fun=false;
				sc.close();
				System.out.println("Saliendo...");
			}
			}
		}
	}

}
