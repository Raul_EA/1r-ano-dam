package f_matrices;

import java.util.Scanner;

public class AreaMatriz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int T =8;
		int[][] m = new int [T][T];
		for(int i=0;i<T;i++) {
			for(int j=0;j<T;j++) {
				m[i][j]=0;
			}
		}
		for(int i=0;i<T;i++) {
			System.out.println();
			for(int j=0;j<T;j++) {
				System.out.print(m[i][j]+" ");
			}
		}
		int x=sc.nextInt();
		int y=sc.nextInt();
		sc.close();
		for(int i=x-1;i<=x+1;i++) {
			for(int j=y-1;j<=y+1;j++) {
				if(i<m.length && i>=0 && j<m[0].length && j>=0)m[i][j]=1;
			}
		}
		m[x][y]=2;
		for(int i=0;i<T;i++) {
			System.out.println();
			for(int j=0;j<T;j++) {
				System.out.print(m[i][j]+" ");
			}
		}
	}

}
