package a_otros;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class Loteria_BusquedaSecuencial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		Random rd = new Random();
		final int BOL= 6;
		final int MIN= 1;
		final int TOP= 49;
		int a = -1;
		int[] b= new int [BOL];
		int[] s= new int [BOL];
		
		while(a!=0) {
			System.out.println("Escoja un de las siguientes opciones:");
			System.out.println("(1) Introducir Combinación");
			System.out.println("(2) Sortear");
			System.out.println("(3) Resultado");
			System.out.println("(0) Salir");
			System.out.println();
			a=sc.nextInt();
			System.out.println();
			switch(a) {
			case 1: 
				System.out.println("Introduzca la combinación:");
				int pos=0;
				while(pos<BOL) {
					int c=sc.nextInt();
					if (c>=MIN && c<=TOP) {
						//Busqueda Secuencial
						boolean en = false;
						int i =0;
						while (i<pos && !en) {
							if(b[i]==c) {
								en=true;
							} else {
								i++;
							}
						}
						if (en) {
							System.out.println("Valor incorrecto");
						} else {
							b[pos]=c;
							pos++;
						}
					} else {
						System.out.println("Valor incorrecto");
					}
				}
				System.out.println();
				System.out.println("Tu combinación es:");
				for(int j=0;j<BOL;j++) {
					System.out.print(b[j]+"["+j+"] ");
				}
				System.out.println();
				System.out.println();
				break;
			case 2:
				pos=0;
				while(pos<BOL) {
					int random = rd.nextInt(MIN, TOP+1);
					boolean en = false;
					int i =0;
					while (i<pos && !en) {
						if(s[i]==random) {
							en=true;
						} else {
							i++;
						}
					}
					if (!en) {
						s[pos]=random;
						pos++;
					}					
				}
				System.out.println("Sorteo hecho");
				System.out.println();
				break;
			case 3:
				int cont=0;
				for (int j =0;j<BOL;j++) {
					boolean en = false;
					int i =0;
					while (j<BOL && !en) {
						if (s[j]==b[i]) {
							en=true;
						} else {
							j++;
						}
					}
					if (en) {
						cont++;
						System.out.println(cont+": "+b[i]);
					}
				}
				System.out.println("Has acertado: "+cont);
				System.out.println();
				Arrays.sort(s);
				for(int i=0;i<BOL;i++) {
					System.out.print(s[i]+" ");
				}
				System.out.println();
				System.out.println();
				break;
			case 0: 
				System.out.println("Siga jugando otro dia!!");
				sc.close();
				break;
			default: 
				System.out.println("Opcion incorrecta");
				System.out.println();
			}
		}
	}
}
