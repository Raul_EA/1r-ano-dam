package a_otros;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		boolean a=true;
		while (a==true) {
			System.out.println("MENU:");
			System.out.println("(1) Decir HOLA");
			System.out.println("(2) Decir ADIOS");
			System.out.println("(3) Cerrar programa");
			int b = sc.nextInt();
			switch (b) {
			case 1:
				System.out.println("HOLA");
				break;
			case 2:
				System.out.println("ADIOS");
				break;
			case 3:
				System.out.println("Apagando...");
				a=false;
				break;
			}
		}
		sc.close();
	}
}
