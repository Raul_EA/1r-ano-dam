package a_otros;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class Bloc3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int a = sc.nextInt();
		LinkedHashMap<String,Integer> map = new LinkedHashMap<>(); 
		for(int i=0;i<a;i++) {
			sc.nextLine();
			String x=sc.next();
			int y=sc.nextInt();
			map.put(x, y);
		}
		System.out.println(map);
		TreeMap<Integer,Integer> map2 = new TreeMap<>();
		for(String s:map.keySet()) {
			if(map2.containsKey(map.get(s))) {
				map2.replace(map.get(s), map2.get(map.get(s))+1);
			} else {
				map2.put(map.get(s), 1);
			}
		}
		System.out.println(map2);
		sc.close();
	}

}
