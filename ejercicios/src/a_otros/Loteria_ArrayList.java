package a_otros;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

public class Loteria_ArrayList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		Random rd = new Random();
		final int BOL= 6;
		final int TOP= 49;
		final int MIN= 1;
		int a = -1;
		ArrayList<Integer> b = new ArrayList<>();
		ArrayList<Integer> s = new ArrayList<>();
		ArrayList<Integer> r = new ArrayList<>();
		while(a!=0) {
			System.out.println("Escoja un de las siguientes opciones:");
			System.out.println("(1) Introducir Combinación");
			System.out.println("(2) Sortear");
			System.out.println("(3) Resultado");
			System.out.println("(0) Salir");
			System.out.println();
			a=sc.nextInt();
			System.out.println();
			switch(a) {
			case 1: 
				System.out.println("Introduzca los numeros de su loteria:");
				int pos=0;
				while(pos<BOL) {
					int c=sc.nextInt();
					if (c>=MIN && c<=TOP) {
						if (b.contains(c)) {
							System.out.println("Valor repetido");
						} else {
							b.add(c);
							pos++;
						}
					} else {
					System.out.println("Valor incorrecto");
					}
				}
				b.sort(null);
				System.out.println();
				System.out.println("Tu combinación es:");
				System.out.println(b);
				System.out.println();
				break;
			case 2:
				pos=0;
				s.clear();
				while(pos<BOL) {
					int random = rd.nextInt(MIN, TOP+1);
					if (!s.contains(random)) {
						s.add(random);
						pos++;
					} 
				}
				s.sort(null);
				System.out.println("Sorteo hecho");
				System.out.println();
				break;
			case 3:
				if (s.size()<BOL) {
					System.out.println("Genere primero el sorteo para comprobar los resultados");
					break;
				}
				if (b.size()<BOL) {
					System.out.println("Introduzca tu combinacion para comprobar los resultados");
					break;
				}
				int c=0;
				r.clear();
				for (int v: b) {
					if (s.contains(v)) {
						c++;
						r.add(v);
					}
				}
				System.out.println("Has acertado: "+c);
				System.out.println(s);
				System.out.println();
				break;
			case 0: 
				sc.close();
				System.out.println("Siga jugando otro dia!!");
				break;
			default: 
				System.out.println("Opcion incorrecta");
				System.out.println();
			}
		}
	}

}
