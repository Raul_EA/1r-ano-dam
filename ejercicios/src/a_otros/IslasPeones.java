package a_otros;

import java.util.Scanner;

public class IslasPeones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int T = 8;
		int[][] m = new int[T][T];

		// crear tablero
		for (int i = 0; i < T; i++) {
			for (int j = 0; j < T; j++) {
				m[i][j] = 0;
			}
		}

		// num peones
		int num = sc.nextInt();
		System.out.println();

		// colocar los peones
		for (int i = 0; i < num; i++) {
			m[sc.nextInt()][sc.nextInt()] = 1;
		}

		// contar islas
		int con = 0;
		boolean fun = false;
		if(num!=0) {
			int y = 0;
			con = 1;
			fun=false;
			for (int i = 0; i < T; i++) {
				for (int j = 0; j < T; j++) {
					if (m[i][j] == 1) {
						if (fun && y <= j - 2) {
							con++;
						}
						fun = true;
						y = j;
					}
				}
			}
		}
		System.out.println(con);

		// mostrar tabla
		for (int i = 0; i < T; i++) {
			System.out.println();
			for (int j = 0; j < T; j++) {
				System.out.print(m[i][j] + " ");
			}
		}
		System.out.println();
		
		// illa solitaria 
		//(no esta acabado dado que he colapsado pero espero que cuente algo)
		con = 0;
		fun = true;
		for (int i = 0; i < T; i++) {
			System.out.println(con);
			if (!fun) {
				if (con == 1) {
					System.out.println("SI");
					fun=true;
				} else {
					fun=true;
					System.out.println("NO");
				}
				con = 0;
			}
			for (int j = 0; j < T; j++) {
				if (m[j][i] == 1) {
					con++;
					for (int k = 0; k < T; k++) {
						if (i == 7) {
							fun=false;
							}else {
								if (m[k][i + 1] == 1) {
									fun = true;
								} else {
									fun = false;
								}
							}
						}
					}
				}
			}
		}
	}
