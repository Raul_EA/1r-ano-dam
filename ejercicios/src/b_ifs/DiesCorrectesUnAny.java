package b_ifs;

import java.util.Scanner;

public class DiesCorrectesUnAny {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int x=sc.nextInt();
		sc.close();
		if (x<1 || x>366) {
			System.out.println("Incorrecte!");
		} else if (x>=1 && x<=365) {
			System.out.println("Correcte per un any no bixest!");
		} else if (x==366) {
			System.out.println("Correcte per un any bixest!");
		}
	}

}
