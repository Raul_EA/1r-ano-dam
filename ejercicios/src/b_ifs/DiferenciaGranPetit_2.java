package b_ifs;

import java.util.Scanner;

public class DiferenciaGranPetit_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int x=sc.nextInt(), y=sc.nextInt(),z=sc.nextInt();
		sc.close();
		if (x>=y && y>=z) {
			System.out.println(x-z);
		} else if (z>=x && x>=y) {
			System.out.println(z-y);
		} else if (y>=z && z>=x) {
			System.out.println(y-x);
		} else if (x>=z && x>=y) {
			System.out.println(x-y);
		} else if (y>=x && x>=z) {
			System.out.println(y-z);
		} else if (z>=y && y>=x) {
			System.out.println(z-x);
		}
	}

}
