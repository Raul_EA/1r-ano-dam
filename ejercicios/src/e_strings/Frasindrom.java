package e_strings;

import java.util.Scanner;

public class Frasindrom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String y = sc.nextLine();
		while (!y.equals(".")) {
			String[] x = y.split(" ");
			int con = 0;
			if (x.length > 1) {
				for (int i = 0; i < x.length / 2; i++) {
					if (x[i].equals(x[x.length - 1 - i])) {
						con++;
					}
				}
				if (x.length / 2 == con) {
					System.out.println("SI");
				} else {
					System.out.println("NO");
				}
			} else {
				System.out.println("SI");
			}
			y = sc.nextLine();
		}
		sc.close();
	}
}
