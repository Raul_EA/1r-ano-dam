package e_strings;

import java.util.Scanner;

public class NumerosBinarios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int q = sc.nextInt();
		for (int i = 0; i < q; i++) {
			int a = sc.nextInt();
			String x = sc.next();
			int b = sc.nextInt();
			int c = 0;
			if (x.equals("+")) {
				c = a + b;
			} else if (x.equals("-")) {
				c = a - b;
			}
			x="";
			while(c!=0 && c!=1) {
				int f = c%2;
				String w = Integer.toString(f);
				x=x + w;
				c=c/2;
			}
			x=x+c;
			StringBuilder z= new StringBuilder (x);
			z.reverse();
			System.out.println(z);
		}
		sc.close();
	}

}
