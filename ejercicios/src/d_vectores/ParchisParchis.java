package d_vectores;

import java.util.Scanner;

public class ParchisParchis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			final int CAS = 4;
			boolean fun = false;
			int[] val = new int[CAS];
			for (int j = 0; j < CAS; j++) {
				val[j] = sc.nextInt();
			}
			int sol = 0;
			for (int j = 0; j < CAS && !fun; j++) {
				sol = sol + val[j];
				if (sol == 8) {
					fun = true;
				} else if (sol >= 8) {
					sol = 8 - (sol - 8);
				}
			}
			System.out.println(sol);
		}
		sc.close();
	}

}
