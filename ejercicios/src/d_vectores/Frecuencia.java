package d_vectores;

import java.util.Scanner;

public class Frecuencia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		final int F = 10;
		int a=sc.nextInt();
		for(int i=0;i<a;i++) {
			final int T = sc.nextInt();
			int[] f = new int[F];
			for(int j=0;j<F;j++) {
				f[j]=0;
			}
			for (int j=0;j<T;j++) {
				f[sc.nextInt()]++;
			}
			for (int j=0;j<F;j++) {
				System.out.println(f[j]);
			}
			System.out.println();
		}
		sc.close();
	}

}
