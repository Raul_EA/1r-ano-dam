package d_vectores;

import java.util.Arrays;
import java.util.Scanner;

public class ElPrimILaGrossa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int TOPE = 100;
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int b = sc.nextInt();
			int[] sol = new int[TOPE];
			int c = sc.nextInt();
			int j = 0;
			while (c != 0 && j <= TOPE) {
				sol[j] = c;
				j++;
				c = sc.nextInt();
			}
			Arrays.sort(sol);
			int con = 0;
			boolean fun = true;
			j = 0;
			while (j < TOPE && fun) {
				if (sol[j] != 0) {
					b = b - sol[j];
					if (b >= 0) {
						con++;
						j++;
					} else {
						fun = false;
					}
				} else {
					j++;
				}
			}
			System.out.println(con);
		}
		sc.close();
	}

}
