package d_vectores;

import java.util.Arrays;
import java.util.Scanner;

public class FormulaEuler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			final int B = sc.nextInt();
			int[] val = new int[B];
			for (int j = 0; j < B; j++) {
				val[j] = sc.nextInt();
			}
			Arrays.sort(val);
			int[] sol = new int[B / 2];
			for (int j = 0; j < B / 2; j++) {
				sol[j] = val[j] + val[B - (j + 1)];
				System.out.print(sol[j] + " ");
			}
		}
		sc.close();
	}

}
