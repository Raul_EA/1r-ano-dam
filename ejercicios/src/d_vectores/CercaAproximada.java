package d_vectores;

import java.util.Scanner;

public class CercaAproximada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int b = sc.nextInt();
			int[] n = new int[b];
			boolean fun = true;
			for (int j = 0; j < b; j++) {
				n[j] = sc.nextInt();
			}
			int c = sc.nextInt();
			for (int j = 0; j < b && fun == true; j++) {
				if ((n[j] == c || n[j] == c - 1 || n[j] == c + 1)) {
					fun = false;
				}
			}
			if (fun == false) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
		}
		sc.close();
	}

}
