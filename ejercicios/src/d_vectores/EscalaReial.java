package d_vectores;

import java.util.Arrays;
import java.util.Scanner;

public class EscalaReial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int CAR = 7;
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int[] car = new int[CAR];
			for (int j = 0; j < CAR; j++) {
				car[j] = sc.nextInt();
			}
			sc.close();
			Arrays.sort(car);
			int con = 1;
			boolean diez = false;
			boolean jack = false;
			boolean reina = false;
			boolean rei = false;
			boolean as = false;
			for (int j = 0; j < CAR; j++) {
				if (car[j] == 10)
					diez = true;
				if (car[j] == 11)
					jack = true;
				if (car[j] == 12)
					reina = true;
				if (car[j] == 13)
					rei = true;
				if (car[j] == 1)
					as = true;
			}
			if (diez && jack && reina && rei && as) {
				System.out.println("ESCALA REIAL");
			} else {
				for (int j = 0; j < CAR; j++) {
					if (con < 5) {
						if (j != 0) {
							if (car[j - 1] == car[j] - 1) {
								con++;
							} else if (car[j - 1] != car[j]) {
								con = 1;
							}
						}
					}
				}
				if (con >= 5)
					System.out.println("ESCALA");
				else
					System.out.println("NO");
			}
		}
	}
}