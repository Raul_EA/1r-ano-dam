package d_vectores;

import java.util.Scanner;

public class Mini2048 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			final int F = sc.nextInt();
			int[] vec = new int[F];
			for (int j = 0; j < F; j++) {
				vec[j] = sc.nextInt();
			}
			char d = sc.next().charAt(0);
			if (d == 'L') {
				for (int j = 0; j < F - 1; j++) {
					if (vec[j] == 0) {
						vec[j] = vec[j + 1];
						vec[j + 1] = 0;
						if (j != 0) {
							if (vec[j] == vec[j - 1]) {
								vec[j - 1] = vec[j - 1] * 2;
								vec[j] = 0;
							}
						}
					} else if (vec[j] == vec[j + 1]) {
						vec[j] = vec[j] * 2;
						vec[j + 1] = 0;
					}
				}
			}
			if (d == 'R') {
				for (int j = vec.length-1; j > 0; j--) {
					if (vec[j] == 0) {
						vec[j] = vec[j - 1];
						vec[j - 1] = 0;
						if (j != vec.length-1) {
							if (vec[j] == vec[j + 1]) {
								vec[j + 1] = vec[j + 1] * 2;
								vec[j] = 0;
							}
						}
					} else if (vec[j] == vec[j - 1]) {
						vec[j] = vec[j] * 2;
						vec[j - 1] = 0;
					}
				}
			}
			for (int j = 0; j < F; j++) {
				System.out.print(vec[j] + " ");
			}
			System.out.println();
		}
	}

}
