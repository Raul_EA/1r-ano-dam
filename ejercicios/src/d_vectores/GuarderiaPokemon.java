package d_vectores;

import java.util.ArrayList;
import java.util.Scanner;

public class GuarderiaPokemon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner si = new Scanner(System.in);
		ArrayList<String> pokemon = new ArrayList<String>();
		String lletra = si.nextLine();
		while (!lletra.equals("D")) {
			if (lletra.equals("A")) {
				String pok = si.nextLine();
				if (!pokemon.contains(pok)) {
					pokemon.add(pok);
				}
			}
			if (lletra.equals("B")) {
				String pok = si.nextLine();
				pokemon.remove(pok);
			}
			if (lletra.equals("C")) {
				pokemon.removeAll(pokemon);
			}
			lletra = si.nextLine();
		}
		si.close();
		pokemon.sort(null);
		System.out.print(pokemon);

	}

}
