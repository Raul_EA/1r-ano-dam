package c_bucles;

import java.util.Scanner;

public class Aparcamiento {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int mida=sc.nextInt();
		while (mida!=0) {
			mida=mida+mida/2+mida%2;
			int num1=sc.nextInt();
			int mejorPosicion=-1;
			int contador=0;
			int mejorMedida=0;
			while (num1!=0) {
				contador++;
				int num2=sc.nextInt();
				if (num2-num1 >= mida) {
					if(mejorPosicion==-1) {
						mejorPosicion=contador;
						mejorMedida=num2-num1;
					} else if (num2-num1<mejorMedida) {
						mejorPosicion=contador;
						mejorMedida=num2-num1;
					}
				}
				num1=sc.nextInt();
			}
			if (mejorPosicion==-1) System.out.println("NO");
			else System.out.println("SI "+mejorPosicion);
			mida=sc.nextInt();
		}
		sc.close();
	}

}
