package c_bucles;

import java.util.Scanner;

public class Exclamaciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		String a=sc.nextLine();
		while(!a.equals("FIN")) {
			int c=0,b=0;
			for(int i=0;i<a.length();i++){
				char d=a.charAt(i);
				if(d=='¡') {
					b++;
				} else if (d=='!') {
					c++;
				}
			}
			if(b==c) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
			a=sc.nextLine();
		}
		sc.close();
	}

}
