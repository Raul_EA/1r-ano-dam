package c_bucles;

import java.util.Scanner;

public class ContarLetrasFrase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		String a=sc.nextLine();
		String n="";
		while(!a.equals("FI")) {
			int c=0;
			for (int j=0;j<a.length();j++) {
				 char x=a.charAt(j);
				 if ((x>=65 && x<=90) || (x>=97 && x<=122)) {
					 c++;
				 }
			}
			a=sc.nextLine();
			n=n+c+" ";
		}
		System.out.println(n);
		sc.close();
	}
}
