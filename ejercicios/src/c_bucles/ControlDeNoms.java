package c_bucles;

import java.util.Scanner;

public class ControlDeNoms {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String p = sc.nextLine();
		int i = p.length() - 1;
		char l = p.charAt(i);
		boolean igual = true;
		while (i > 0) {
			if (l != p.charAt(i - 1)) {
				igual = false;
			}
			i--;
			l = p.charAt(i);
		}
		sc.close();
		if (igual == false) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
	}

}