package c_bucles;

import java.util.Scanner;

public class SumaPositivosINegativos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int a = sc.nextInt();
		int x=0,y=0;
		while (a!=0) {
			if (a>0) x++;
			else if (a<0) y++;
			a=sc.nextInt();
		}
		sc.close();
		if (x==y) System.out.println("IGUALS");
		else if (x>y) System.out.println("POSITIUS");
		else if (x<y) System.out.println("NEGATIUS");
	}

}
