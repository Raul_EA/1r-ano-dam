package c_bucles;

import java.util.Scanner;

public class Cables {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int a=sc.nextInt();
		for (int i=0;i<a;i++) {
			int b=sc.nextInt();
			int h=0,m=0;
			sc.nextLine();
			for (int j=0;j<b;j++) {
				String x = sc.next();
				for (int k=0;k<x.length();k++) {
					char y = x.charAt(k);
					if (y=='H') h++;
					else if (y=='M')m++;
				}
			}
			if (h==m) System.out.println("POSIBLE");
			else System.out.println("IMPOSIBLE");
		}
		sc.close();
	}
}
