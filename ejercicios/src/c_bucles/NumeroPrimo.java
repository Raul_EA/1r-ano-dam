package c_bucles;

import java.util.Scanner;

public class NumeroPrimo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		sc.close();
		boolean fun = true;
		if (a == 0 || a == 1) {
			System.out.println("No es ni primo ni compuesto");
		} else {
			for (int i = 2; i < a / 2 && fun; i++) {
				if (a % i == 0) {
					fun = false;
				}
			}
		}
		if (fun) {
			System.out.println("Es primo");
		} else {
			System.out.println("No es primo");
		}
	}
}
