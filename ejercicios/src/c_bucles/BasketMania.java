package c_bucles;

import java.util.Scanner;

public class BasketMania {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int b = sc.nextInt();
			int L = 0, V = 0;
			for (int j = 0; j < b; j++) {
				char x = sc.next().charAt(0);
				int c = sc.nextInt();
				if (x == 'L') {
					L = L + c;
				} else if (x == 'V') {
					V = V + c;
				}
			}
			if (L > V) {
				System.out.println("L " + L + " " + V);
			} else if (L < V) {
				System.out.println("V " + L + " " + V);
			} else {
				System.out.println("E " + L + " " + V);
			}
		}
		sc.close();
	}

}
