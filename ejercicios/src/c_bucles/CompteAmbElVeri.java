package c_bucles;

import java.util.Scanner;

public class CompteAmbElVeri {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int hp = sc.nextInt(), ar = sc.nextInt(), at = sc.nextInt();
			int c = 0;
			String f = "";
			while (hp > 0) {
				c++;
				hp = hp - ar;
				if (hp <= 0) {
					f = "RAMMUS " + c;
				} else {
					hp = hp - at;
					if (hp <= 0) {
						f = "TWITCH " + c;
					}
				}
			}
			System.out.println(f);
		}
		sc.close();
	}

}
