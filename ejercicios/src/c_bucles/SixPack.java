package c_bucles;

import java.util.Scanner;

public class SixPack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		sc.nextLine();
		for (int i = 0; a > i; i++) {
			String b = sc.nextLine();
			int c = 0, d = 0;
			while (!b.equals("BUFFFFF")) {
				for (int j = 0; j < b.length(); j++) {
					char ch = b.charAt(j);
					if (ch == 'C') {
						c++;
					} else if (ch == 'G') {
						d++;
					}
				}
				b = sc.nextLine();
			}
			if (c > d) {
				System.out.println("Cervesa");
			} else if (c < d) {
				System.out.println("Gimnas");
			} else {
				System.out.println("Empat");
			}
		}
		sc.close();
	}

}
