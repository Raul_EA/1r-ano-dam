package g_diccionarios;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class Surtido2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int A = sc.nextInt();
		for (int i = 0; i < A; i++) {
			int b = sc.nextInt();
			sc.nextLine();
			LinkedHashMap<String, Integer> dic = new LinkedHashMap<>();
			for (int j = 0; j < b; j++) {
				String w = sc.nextLine();
				boolean tr = false;
				for (String s : dic.keySet()) {
					if (!tr) {
						if (s.equals(w)) {
							tr = true;
						}
					}
				}
				if (tr) {
					dic.replace(w, dic.get(w) + 1);
				} else {
					dic.put(w, 1);
				}
			}
			int y = 0;
			boolean fun = true;
			while (fun ) {
				if (dic.size() > 2) {
					LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
					int x = 0;
					String g = "";
					
					// ordenar
					for (int j = 0; j < dic.size(); j++) {
						for (String s : dic.keySet()) {
							if (dic.get(s) > x) {
								x = dic.get(s);
								g = s;
							}
						}
						dic.replace(g, 0);
						map.put(g, x);
						x = 0;
					}

					// borrar y contar
					x=0;
					for (String s : map.keySet()) {
						if (map.get(s) != 0 && x < 3) {
							x++;
							map.replace(s, map.get(s) - 1);
						}
					}
					if (x == 3) {
						y++;
					} else {
						fun=false;
					}
					x=0;
					
					//volver a ordenar
					dic.clear();
					for (int j = 0; j < map.size(); j++) {
						for (String s : map.keySet()) {
							if (map.get(s) > x) {
								x = map.get(s);
								g = s;
							}
						}
						map.replace(g, 0);
						if(x!=0) dic.put(g, x);
						x = 0;
					}
				} else {
					fun=false;
				}
			}
			System.out.println(y);
		}
		sc.close();
	}

}
