package g_diccionarios;

import java.util.HashMap;
import java.util.Scanner;

public class DesiertoSinaí2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int b = sc.nextInt();
			sc.nextLine();
			HashMap<String, Integer> dic = new HashMap<>();
			for (int j = 0; j < b; j++) {
				String c = sc.nextLine();
				String[] d = c.split(" ");
				String h="";
				for(int k=0;k<d.length-1;k++) {
					h=h+" "+d[k];
				}
				h=h.trim();
				int e = Integer.parseInt(d[d.length-1]);
				if(dic.containsKey(h)) {
					dic.replace(h, dic.get(h)+e);
				}else {
					dic.put(h, e);
				}
			}
			int k = 0;
			String w = "";
			for (String j : dic.keySet()) {
				if (dic.get(j) > k) {
					k = dic.get(j);
					w = j;
				}
			}
			System.out.println(w);
		}
		sc.close();
	}
}
