package g_diccionarios;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class Surtido1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		for (int i = 0; i < a; i++) {
			int b = sc.nextInt();
			sc.nextLine();
			LinkedHashMap<String, Integer> dic = new LinkedHashMap<>();
			for (int j = 0; j < b; j++) {
				String w = sc.nextLine();
				boolean tr = false;
				for (String s : dic.keySet()) {
					if (!tr) {
						if (s.equals(w)) {
							tr = true;
						}
					}
				}
				if (tr) {
					dic.replace(w, dic.get(w) + 1);
				} else {
					dic.put(w, 1);
				}
			}
			int x = 2147483647;
			for (String s : dic.keySet()) {
				if (x > dic.get(s)) {
					x = dic.get(s);
				}
			}
			System.out.println(x);
		}
		sc.close();
	}

}
