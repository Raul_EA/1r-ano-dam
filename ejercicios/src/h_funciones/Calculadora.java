package h_funciones;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		boolean fun = true,camb=false;
		int a =0,b=0;
		while(fun) {
			mostrarMenu();
			int x = sc.nextInt();
			switch (x){
				case 1:
					if(!camb) {
						System.out.println("Introduzca los dos valores a operar:");
						a=sc.nextInt();
						b=sc.nextInt();
					}else {
						System.out.println("Introduzca vslor operativo:");
						b=sc.nextInt();
					}
					break;
				case 2:
					a=suma(a,b);
					camb=true;
					break;
				case 3:
					a=resta(a,b);
					camb=true;
					break;
				case 4:
					a=mul(a,b);
					camb=true;
					break;
				case 5:
					if(b==0)
						System.err.println("No se puede divir entre 0");
					else {
						System.out.println("1-Division entera\n2-Modulo");
						int y=sc.nextInt();
						switch(y) {
						case 1:
							a=div(a,b);
							camb=true;
							break;
						case 2:
							a=mod(a,b);
							camb=true;
							break;
						}
					}
					break;
				case 6:
					System.out.println("El resultado es: "+a);
					break;
				case 7:
					camb=false;
					a=0;
					break;
				case 8:
					System.out.println("Saliendo...");
					fun=false;
					break;
			}
		}
	sc.close();
	}
	public static int suma(int n1,int n2) {
		return n1+n2;
	}
	public static int resta(int n1,int n2) {
		return n1-n2;
	}
	public static int mul(int n1,int n2) {
		return n1*n2;
	}
	public static int div(int n1,int n2) {
		return n1/n2;
	}
	public static int mod(int n1,int n2) {
		return n1%n2;
	}
	public static void mostrarMenu() {
		System.out.println("CALCULADORA:\n1-Obtener numeros\n2-Sumar\n3-Restar\n4-Multiplicar\n5-Dividir\n6-Resultado\n7-Valores a 0\n8-Salir");
	}
}
